
== TIME ===========================
start time :: 2016_05_28-12_16_25
end time :: 2016_05_28-19_01_34

== CONFIG ===========================
Experiment Config File : experiment_config.csv

GDM Config File : gdm_parameter_config.csv

Input Data Path : data
Result Folder Path : result_optimization_without_time_norm


== FLAGS ===========================
Normalize Data : 1
Save Visualization : 0

== EXPERIMENT ===========================
GDM Type : 2DTD

Experiment:real - smallnet
Data File:02_eth_100110

map center =  (1.500 x 1.500) m^2
map size   =  (8.000 x 7.000) m^2
source location = [3.20, 1.71, 0.00]) m
gas type : ethanol

== DATA PARTITION ===========================
parse test set selection: contPartTestSetSelection - [5 5 5 0]
parse tv set selection:   contTvSetSelectionFixed - [4 4]

training data index : [1:39582]
validation data index : [39583:52776]
test data index : [52777:65970]


== Intervals : GDM META-PARAMETERS =====================
sigma: [0.10, 0.60], step : 0.05)
c    : [0.05, 0.40], step : 0.05)
beta : [0.00, 0.00], step : 0.00)

== Optimized GDM META-PARAMETERS =====================
Target Time: 1.662001e+03 --> (c = 0.05, sigma = 0.20, beta = 0.00700)

== END =====================================


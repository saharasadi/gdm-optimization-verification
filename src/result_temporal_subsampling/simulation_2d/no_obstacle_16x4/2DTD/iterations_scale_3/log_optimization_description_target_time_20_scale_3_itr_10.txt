
== TIME ===========================
start time :: 2016_06_05-16_27_21
end time :: 2016_06_05-17_26_50

== CONFIG ===========================
Experiment Config File : experiment_config.csv

GDM Config File : gdm_parameter_config.csv

Input Data Path : data
Result Folder Path : result_optimization_without_time_norm


== FLAGS ===========================
Normalize Data : 1
Save Visualization : 0

== EXPERIMENT ===========================
GDM Type : 2DTD

Experiment:simulation_2d - no_obstacle_16x4
Data File:no_obstacle_16x4

map center =  (8.000 x 2.000) m^2
map size   =  (16.000 x 4.500) m^2
source location = [1.00, 2.00, 0.00]) m
gas type : ethanol

== DATA PARTITION ===========================
parse test set selection: contPartTestSetSelection - [5 5 5 0]
parse tv set selection:   contTvSetSelectionFixed - [4 4]

training data index : [1:18703]
validation data index : [18704:24937]
test data index : [24938:31171]


== Intervals : GDM META-PARAMETERS =====================
sigma: [0.10, 0.40], step : 0.05)
c    : [0.05, 0.25], step : 0.05)
beta : [0.00, 0.50], step : 0.02)

== Optimized GDM META-PARAMETERS =====================
Target Time: 20 --> (c = 0.05, sigma = 0.15, beta = 0.22000)

== END =====================================


function [trainIndexSet, valIndexSet, tvIndexSet, testIndexSet] = ...
    selectDataPartition(numOfSamples, strTestSetSelectionId, parsTestSetSelection, ...
    strTvSetSelectionId, parsTvSetSelection, bVisualizeData)

if strcmp(strTestSetSelectionId,'contPartTestSetSelection')
    parsTestSetSelection(3) = round(...
        (numOfSamples * ((parsTestSetSelection(1) - 1)/parsTestSetSelection(2)) + 1));
    parsTestSetSelection(4) = round(...
        (numOfSamples * parsTestSetSelection(1)/parsTestSetSelection(2)));

    testIndexSet = parsTestSetSelection(3):1:parsTestSetSelection(4);
    tvIndexSet = [1:parsTestSetSelection(3)-1 parsTestSetSelection(4)+1:numOfSamples];
end

numOfTvSamples = size(tvIndexSet,2);

% Fixed selection of training and validation data (not for cross-validation)
if strcmp(strTvSetSelectionId,'contTvSetSelectionFixed')
    valSetInd1 = round(...
        (numOfTvSamples * ((parsTvSetSelection(1) - 1)/parsTvSetSelection(2)) + 1));
    valSetInd2 = round(...
        (numOfTvSamples * parsTvSetSelection(1)/parsTvSetSelection(2)));
    valIndexSet = tvIndexSet(valSetInd1:valSetInd2);
    trainIndexSet = tvIndexSet([1:valSetInd1-1 valSetInd2+1:numOfTvSamples]);
end

if bVisualizeData
    h = figure;
    h.Name = 'Data Partitioning';
    plot(testIndexSet, testIndexSet./testIndexSet, 'ro');
    hold on;
    plot(tvIndexSet, 0.75*tvIndexSet./tvIndexSet, 'bo');
    plot(valIndexSet, 0.50*valIndexSet./valIndexSet, 'go');
    plot(trainIndexSet, 0.25*trainIndexSet./trainIndexSet, 'ko');
    hold off;
    legend('test samples','train/val samples','val samples','train samples');
    title('Data Paritions', 'FontSize', 12, 'FontWeight','bold');
    axis([0 numOfSamples 0 1]);
    figure (gcf);
end


end
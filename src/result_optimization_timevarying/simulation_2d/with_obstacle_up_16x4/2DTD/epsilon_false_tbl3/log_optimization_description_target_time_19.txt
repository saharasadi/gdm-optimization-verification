
== TIME ===========================
start time :: 2016_01_08-00_41_02
end time :: 2016_01_08-02_49_29

== CONFIG ===========================
Experiment Config File : experiment_config.csv

GDM Config File : gdm_parameter_config.csv

Input Data Path : data
Result Folder Path : result_optimization_timevarying_without_norm


== FLAGS ===========================
Normalize Data : 1
Save Visualization : 1
Epsilon in Evaluation : 0
== EXPERIMENT ===========================
GDM Type : 2DTD

Experiment:simulation_2d - with_obstacle_up_16x4

map center =  (8.000 x 2.000) m^2
map size   =  (16.000 x 4.500) m^2
source location = [1.00, 2.00, 0.00]) m
gas type : ethanol

== DATA PARTITION ===========================
parse test set selection: contPartTestSetSelection - [5 5 5 0]
parse tv set selection:   contTvSetSelectionFixed - [4 4]

training data index : [1:23385]
validation data index : [23386:31180]
test data index : [31181:38975]


== Intervals : GDM META-PARAMETERS =====================
sigma: [0.15, 0.40], step : 0.05)
c    : [0.05, 0.30], step : 0.05)
beta : [0.00, 0.50], step : 0.02)

== Optimized GDM META-PARAMETERS =====================
Target Time: 19 --> (c = 0.05, sigma = 0.15, beta = 0.06000)

== END =====================================


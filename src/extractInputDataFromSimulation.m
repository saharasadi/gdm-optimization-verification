function [inputData] = extractInputDataFromSimulation(inputFolder, fileName)

% Load input data from file
inputDataRaw = load([inputFolder '/' fileName '.log']);
numOfDataPoints = size(inputDataRaw,1);

% Set Indices to read corresponding columns from inputDataRaw
timeIdx = 1;
xLocIdx = 4;
yLocIdx = 5;
gasIdx = 6;
directionWindIdx = 7;
intensityWindIdx = 8;

inputData = zeros(numOfDataPoints, 6); 

inputData(:,1) = inputDataRaw(:, timeIdx);%/1000; % time stamp
inputData(:,2) = inputDataRaw(:, gasIdx); % gas sensor reading
inputData(:,3) = inputDataRaw(:, xLocIdx); % localization x
inputData(:,4) = inputDataRaw(:, yLocIdx); % localization y
inputData(:,5) = inputDataRaw(:, intensityWindIdx); % flow intensity ??
inputData(:,6) = inputDataRaw(:, directionWindIdx); % flow direction ??

% inputData(1:24935,2) = inputData(1:24935,2) + 0.1047*randn(24935,1);
% 
% s = RandStream('mt19937ar','Seed',1);
% RandStream.setGlobalStream(s);
% indexSubset = randperm(s, size(inputData, 1), round(size(inputData, 1)/4));
% inputData = inputData(indexSubset, :);

% Sort inputData based on the sampling time
inputData = sortrows(inputData, 1);

end

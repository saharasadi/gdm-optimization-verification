%% Description callAnalyzeBasic.m
%   written by: Sahar Asadi, 
%   Last day released: 2017-02-27
%   
% This code, get the file where the grid search results is saved. 
% It reutrns the meta-parameters which optimize GDM for the given metric. 
% It also plots the search space and the metric values for each set of
% meta-parameters.
% 
% input: 
%   gdmType: '2D' (for Kernel DM+V) and/or '2DTD' (for TD Kernel DM+V)
%   experimentLabel: name of the input data file
%   experimentType: the type of the experiment and the folder containing the input data file.
%   time: the timestamp of the last sample in the dataset. 
%       for simulation 2D: '20'
%       for simulation 3D: '100'
%       for real experiments: 'max' refering to the timestamp of the last recorded sample. 
%  plotPath: path to where the output plots have to be saved. 
%  bVisualize: whether or not plot the NLPD values for the values in kernel
%  width, cell size, and time scale.
%
%
%% Initialization =========================================================

close all;
clc;
clear;

bVisualize = false; 

% set input experiment
% [experimentType, experimentLabel] = ['real', 'corridor']
%                                     ['real', 'smallnet'] 
%                                     ['simulation_2d', 'no_obstacle_16x4'] 
%                                     ['simulation_2d', 'with_obstacle_up_16x4'] 
%                                     ['simulation_ros', 'mox_sensor_model']
%                                     ['simulation_ros', 'with_obstacle_two_60x20']
% for simulation ros, you need to add the path manually.
experimentType = 'simulation_2d';
experimentLabel = 'no_obstacle_16x4';

% options for conditionPath: 'constant_rate_y10_z0.1'
%                            'constant_rate_y15_z0.1'
%                            'fast_rate_y10_z0.1'
conditionPath = '';
if strcmp('with_obstacle_two_60x20', experimentLabel)
    conditionPath = 'constant_rate_y15_z0.1' + '_';        
end

if strcmp(experimentType, 'real')
    time = 'max';
elseif strcmp(experimentType, 'simulation_2d')
    time = '20';
else
    time = '100';
end

% set GDM type
epsilon = 'false'; % options are: ['false', 'true '];
gdmTypeStrSet = ['2D  '; '2DTD'];
gdmTypeCellSet = cellstr(gdmTypeStrSet);


% set the prefix to the parent folder where output will be saved and the
% input data from the grid search will be read from.
pathPrefix = 'result_optimization_without_time_norm';
plotPath = [pathPrefix '/' 'plots' '/' experimentLabel];

if ~exist(plotPath, 'dir')
    mkdir(plotPath);
end
pathPrefix = 'result_optimization_without_time_norm';
inputDataPath = [ pathPrefix '/' experimentType '/' experimentLabel '/' ...
        '2DTD' '/' conditionPath 'epsilon_' epsilon];
inputDataFile = [inputDataPath '/' 'log_optimization_description_target_time_' ...
        time '.mat'];

%% Load input data
dataSource = load(inputDataFile);

%% Run on given GDM types
for i=1:size(gdmTypeCellSet,1);
    gdmType = cell2mat(gdmTypeCellSet(i));  
    mainAnalyzeOptimizationBasic(experimentLabel, epsilon, gdmType, time, ...
        dataSource.optimizationSet, plotPath, bVisualize);
end

%close all;
%=========================================================================

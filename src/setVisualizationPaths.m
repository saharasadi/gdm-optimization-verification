function [folder, labelPrefix, ...
    wmFile, cmFile, gdmFile, gdvFile, wmLvFile, cmLvFile, gdmLvFile, ...
    gdvLvFile] = setVisualizationPaths(folder, gdmConfig, gdmType, experimentLabel)

labelPrefix = cell2mat([experimentLabel '_' gdmType '_' gdmConfig]);
% FIG files 
wmFile  = [folder '/' labelPrefix '_WM.fig'];
cmFile  = [folder '/' labelPrefix '_CM.fig'];
gdmFile = [folder '/' labelPrefix '_GDM.fig'];
gdvFile = [folder '/' labelPrefix '_GDV.fig'];

% PNG files (legacy visualisation)
wmLvFile  = [folder '/' labelPrefix '__lv_WM.png'];
cmLvFile  = [folder '/' labelPrefix '__lv_CM.png'];
gdmLvFile = [folder '/' labelPrefix '__lv_GDM.png'];
gdvLvFile = [folder '/' labelPrefix '__lv_GDV.png'];

end
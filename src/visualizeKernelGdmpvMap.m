%% Description: function visualizeKernelGdmpvMap
%
% Called by "doVisualize.m"
%
% This code, reads creates the graphs of the GDM model and saves them in
% a given path.
% Note that you need to change the color bar configs (the range in caxis)
% to change the scales for the colors used for creating graphs.
%==========================================================================

function visualizeKernelGdmpvMap(labelPrefix, gdmType, ...
    kernelWidth, maxKernelEvalRadius, weightVarianceSigma, ...
    kGdmvMap, mapGeometry, ...
    kGdmWeightMin, kGdmWeightMax, kGdmMapMin, kGdmMapMax, kGdmVarMin, kGdmVarMax, ...
    mapFolder, ...
    bShowVisualizations, bSaveVisualizations, ...
    bShowLegacyVisualizations, bSaveLegacyVisualizations, ...
    pngFileSizeW, pngFileSizeH, ...
    maxRange, maxRangeColorMin, maxRangeColorMax, unusedCellsColor)

% (0) Parameters
titleFontSize = 12;

mapLabel = 'Kernel DM+V';
if strcmp(gdmType, '2DTD')
    mapLabel = 'TD Kernel DM+V'; 
end

% (2) Create id strings
runID = labelPrefix;
parID = sprintf('%04.0f_wvs_%06.0f-mkerf_%04.0f',...
        100*mapGeometry(5),...
        1000*weightVarianceSigma,...
        100*maxKernelEvalRadius/kernelWidth);

% (4) Weight map
if bShowVisualizations
    currFig = figure('Name', [mapLabel ' - weight map']);
    surfH = surfc(kGdmvMap(:,:,1),kGdmvMap(:,:,2),kGdmvMap(:,:,3));
    colormap(currFig, jet);
    caxis([0,1]);
    
    grid off;
    title([ regexprep([ mapLabel ' - ' ' weight map (' runID ')'],'_','\\_') sprintf(':\n') ...
        sprintf('\\sigma = %.3fm, \\sigma_{max} = %.3fm, \\sigma_{\\Omega} =%.3fm', ...
        kernelWidth, maxKernelEvalRadius, weightVarianceSigma) ], ...
        'FontSize',titleFontSize, 'FontWeight','bold');
    axis([ ...
        mapGeometry(7), ...
        mapGeometry(3) + mapGeometry(1)/2 * mapGeometry(5), ...
        mapGeometry(8), ...
        mapGeometry(4) + mapGeometry(2)/2 * mapGeometry(6), ...
        kGdmWeightMin-(kGdmWeightMax-kGdmWeightMin)/40, kGdmWeightMax+(kGdmWeightMax-kGdmWeightMin)/40, ... % ".../40" is a hack to avoid ugly white lines appear in the graph (when the min value is simply set to 0)
        kGdmWeightMin-(kGdmWeightMax-kGdmWeightMin)/40, kGdmWeightMax+(kGdmWeightMax-kGdmWeightMin)/40]);
    view(2); % sets the default two-dimensional: az = 0, el = 90
    set(surfH,'EdgeColor','none');
    %set(gca, 'position', [0 0 1 1]);
    set(currFig,'Position',[1,1,pngFileSizeW,pngFileSizeH]);
    set(currFig,'PaperPositionMode','auto');
    if bSaveVisualizations
        % store FIG
        saveas(gcf,[ mapFolder sprintf('/wghtMap__%s__-%s.fig',runID,parID)]);
        %saveas(gcf,wmFile);
        %copyfile(wmFile,[ mapFolder sprintf('/wghtMap__%s__-%s.fig',runID,parID)]);
        
        % store PNG
        print(currFig, ...
            '-dpng', '-r96', ...
            [ mapFolder sprintf('/wghtMap__%s__-%s.png',runID,parID)]);
    end
end

% (5) Confidence map
if bShowVisualizations
    currFig = figure('Name', [mapLabel ' - ' 'confidence map']);
    surfH = surfc(kGdmvMap(:,:,1),kGdmvMap(:,:,2),1 - kGdmvMap(:,:,6));
    grid off;
    colormap(currFig, jet);
    caxis([0,1]);
    title([ regexprep([ mapLabel ' - ' ' confidence map (' runID ')'],'_','\\_') sprintf(':\n') ...
        sprintf('\\sigma = %.3fm, \\sigma_{max} = %.3fm, \\sigma_{\\Omega} =%.3fm', kernelWidth, maxKernelEvalRadius, weightVarianceSigma) ], ...
        'FontSize',titleFontSize, 'FontWeight','bold');
    axis([ ...
        mapGeometry(7), ...
        mapGeometry(3) + mapGeometry(1)/2 * mapGeometry(5), ...
        mapGeometry(8), ...
        mapGeometry(4) + mapGeometry(2)/2 * mapGeometry(6), ...
        -0.1, 1, ... 
        0, 1]);
    view(2); % sets the default two-dimensional: az = 0, el = 90
    
    % store PNG
    set(surfH,'EdgeColor','none');
    %set(gca, 'position', [0 0 1 1]);
    set(currFig,'Position',[1,1,pngFileSizeW,pngFileSizeH]);
    set(currFig,'PaperPositionMode','auto');
    if bSaveVisualizations
        % store FIG
        saveas(gcf, [ mapFolder sprintf('/confMap__%s__-%s.fig',runID,parID)]);
        %saveas(gcf,cmFile);
        %copyfile(cmFile,[ mapFolder sprintf('/confMap__%s__-%s.fig',runID,parID)]);

        print(currFig, ...
            '-dpng', '-r96', ...
            [ mapFolder sprintf('/confMap__%s__-%s.png',runID,parID)]);
    end
end

% (6) Distribution mean
if bShowVisualizations
    currFig = figure('Name', [mapLabel ' - ' ' distribution mean map']);
    surfH = surfc(kGdmvMap(:,:,1),kGdmvMap(:,:,2),kGdmvMap(:,:,4));
    colorbar;
    colormap(currFig, jet);
    %caxis([0,1]);  %to change the range of the color bar change this.
    grid off;
    title([ regexprep([ mapLabel ' - ' ' distribution mean map (' runID ')'],'_','\\_') sprintf(':\n') ...
        sprintf('\\sigma = %.3fm, \\sigma_{max} = %.3fm, \\sigma_{\\Omega} =%.3fm', kernelWidth, maxKernelEvalRadius, weightVarianceSigma) ], ...
        'FontSize',titleFontSize, 'FontWeight','bold');
    axis([ ...
        mapGeometry(7), ...
        mapGeometry(3) + mapGeometry(1)/2 * mapGeometry(5), ...
        mapGeometry(8), ...
        mapGeometry(4) + mapGeometry(2)/2 * mapGeometry(6), ...
        0, 1]);
        %kGdmWeightMin-(kGdmWeightMax-kGdmWeightMin)/40, kGdmWeightMax+(kGdmWeightMax-kGdmWeightMin)/40, ... % ".../40" is a hack to avoid ugly white lines appear in the graph (when the min value is simply set to 0)
        %kGdmWeightMin-(kGdmWeightMax-kGdmWeightMin)/40, kGdmWeightMax+(kGdmWeightMax-kGdmWeightMin)/40]);    view(2); % sets the default two-dimensional: az = 0, el = 90
    view(2); 
    set(surfH,'EdgeColor','none');
    %set(gca, 'position', [0 0 1 1]);
    set(currFig,'Position',[1,1,pngFileSizeW,pngFileSizeH]);
    set(currFig,'PaperPositionMode','auto');
    if bSaveVisualizations
        % store FIG
        saveas(gcf, [ mapFolder sprintf('/meanMap__%s__-%s.fig',runID,parID)]);
        %saveas(gcf,cmFile);
        %copyfile(cmFile,[ mapFolder sprintf('/meanMap__%s__-%s.fig',runID,parID)]);
        % store PNG
        print(currFig, ...
            '-dpng', '-r96', ...
            [ mapFolder sprintf('/meanMap__%s__-%s.png',runID,parID)]);
    end
end

% (7) Distribution variance
if bShowVisualizations
    currFig = figure('Name', [mapLabel ' - ' ' distribution variance map']);
    surfH = surfc(kGdmvMap(:,:,1),kGdmvMap(:,:,2),kGdmvMap(:,:,5));
    colorbar;
    colormap(currFig, jet);
    %caxis([0,0.1]);  %to change the range of the color bar change this.
    title([ regexprep([ mapLabel ' - ' ' distribution variance map (' runID ')'],'_','\\_') sprintf(':\n') ...
        sprintf('\\sigma = %.3fm, \\sigma_{max} = %.3fm, \\sigma_{\\Omega} =%.3fm', kernelWidth, maxKernelEvalRadius, weightVarianceSigma) ], ...
        'FontSize',titleFontSize, 'FontWeight','bold');
    axis([ ...
        mapGeometry(7), ...
        mapGeometry(3) + mapGeometry(1)/2 * mapGeometry(5), ...
        mapGeometry(8), ...
        mapGeometry(4) + mapGeometry(2)/2 * mapGeometry(6), ...
        0, 1]);
        %kGdmWeightMin-(kGdmWeightMax-kGdmWeightMin)/40, kGdmWeightMax+(kGdmWeightMax-kGdmWeightMin)/40, ... % ".../40" is a hack to avoid ugly white lines appear in the graph (when the min value is simply set to 0)
        %kGdmWeightMin-(kGdmWeightMax-kGdmWeightMin)/40, kGdmWeightMax+(kGdmWeightMax-kGdmWeightMin)/40]);    view(2); % sets the default two-dimensional: az = 0, el = 90
    view(2);
    set(surfH,'EdgeColor','none');
    %set(gca, 'position', [0 0 1 1]);
    set(currFig,'Position',[1,1,pngFileSizeW,pngFileSizeH]);
    set(currFig,'PaperPositionMode','auto');    
    if bSaveVisualizations
        % store FIG
        saveas(gcf, [ mapFolder sprintf('/pvarMap__%s__-%s.fig',runID,parID)]);
        %saveas(gcf,cmFile);
        %copyfile(cmFile,[ mapFolder sprintf('/pvarMap__%s__-%s.fig',runID,parID)]);

        % store PNG
        print(currFig, ...
            '-dpng', '-r96', ...
            [ mapFolder sprintf('/pvarMap__%s__-%s.png',runID,parID)]);
    end
end

% (8) Weight map, legacy visualization
if bSaveLegacyVisualizations
    saveGdwmAsPng(kGdmvMap, kGdmWeightMin, kGdmWeightMax, ...
        wmLvFile, ...
        maxRange, maxRangeColorMin, maxRangeColorMax, unusedCellsColor);
    fileInmapFolder = [ mapFolder sprintf('/wghtMap__%s__-%s_lg.png',runID,parID)];
    movefile(wmLvFile,fileInmapFolder);
    if bShowLegacyVisualizations == true
        figure('Name', [mapLabel ' - ' ' weight map, legacy visualization']);
        imshow(fileInmapFolder,'InitialMagnification','fit');
    end
end

% (9) Confidence map, legacy visualization
if bSaveLegacyVisualizations
    saveGdwmcAsPng(kGdmvMap, ...
        cmLvFile, ...
        maxRange, maxRangeColorMin, maxRangeColorMax, unusedCellsColor);
    fileInmapFolder = [ mapFolder sprintf('/confMap__%s__-%s_lg.png',runID,parID)];
    movefile(cmLvFile,fileInmapFolder);
    if bShowLegacyVisualizations == true
        figure('Name', [mapLabel ' - ' ' weight map, legacy visualization']);
        imshow(fileInmapFolder,'InitialMagnification','fit');
    end
end

% (10) Distribution mean map, legacy visualization
if bSaveLegacyVisualizations
    saveGdmAsPng(kGdmvMap, kGdmMapMin, kGdmMapMax, ...
        gdmLvFile, ...
        maxRange, maxRangeColorMin, maxRangeColorMax, unusedCellsColor);
    fileInmapFolder = [ mapFolder sprintf('/meanMap__%s__-%s_lg.png',runID,parID)];
    movefile(gdmLvFile,fileInmapFolder);
    if bShowLegacyVisualizations == true
        figure('Name', [mapLabel ' - ' ' distribution mean map, legacy visualization']);
        imshow(fileInmapFolder,'InitialMagnification','fit');
    end
end

% (11) Distribution variance map, legacy visualization
if bSaveLegacyVisualizations
    saveGdvmAsPng(kGdmvMap, kGdmVarMin, kGdmVarMax, ...
        gdvLvFile, ...
        maxRange, maxRangeColorMin, maxRangeColorMax, unusedCellsColor);
    fileInmapFolder = [ mapFolder sprintf('/pvarMap__%s__-%s_lg.png',runID,parID)];
    movefile(gdvLvFile,fileInmapFolder);
    if bShowLegacyVisualizations == true
        figure('Name', [mapLabel ' - ' ' distribution variance map, legacy visualization']);
        imshow(fileInmapFolder,'InitialMagnification','fit');
    end
end

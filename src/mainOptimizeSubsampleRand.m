function [] = mainOptimizeSubsampleRand(experimentLabel, bEpsilon, bNormalizeTime, ...
    resultFolder, scale, totalItr)

% set the start time - this is used to create a folder in the result path
startTime = clock;
strStartTimeId = [sprintf('%i',startTime(1)) '_' ...
    sprintf('%02i',startTime(2)) '_' sprintf('%02i',startTime(3)) '-' ...
    sprintf('%02i',startTime(4)) '_' sprintf('%02i',startTime(5)) '_' sprintf('%02.0f',startTime(6))];

% initialize variables
gdmTypeSet = ['2D', '2DTD'];
gdmType = '2DTD';
inputFolder = 'data';

experimentConfigFile = 'experiment_config.csv';
gdmConfigFile = 'gdm_parameter_config.csv';

% set optimization function
strOptTarget = 'NLPD'; % [NLPD, avErr] optimise for min NLPD

% set kernel constants
weightVarianceSigmaFactor = 1.0;
maxKernelEvalRadiusFactor = 4.0;

% set flags for conditional operations such as plotting, saving logs, etc.
bVisualizeData = false;
bNormalizeData = true;

bVerbose = false;
bSaveLog = true;
bSaveVisualization = false;

% set epsilon flag and epsilon itself
epsilon = 0.0000001;

% select experiment corresponding to experimentLabel
[experimentType, fileName, startInd, endInd, ...
    mapCenter, mapSize, sourceLocation, gasType] = selectExperiment(experimentLabel);

% extract inputData considering the experimentLabel, and the cutting
% indices.
startTime = 1;
endTime = 20;
inputData = extractInputData(inputFolder, experimentType, ...
    experimentLabel, fileName, startTime, endTime, startInd, endInd, ...
    bNormalizeData, bNormalizeTime, bVisualizeData);

% select partitioning cuts
strTestSetSelectionId = 'contPartTestSetSelection'; % partitions the data into parsTestSetSelection(2) contiguous sets and selects the parsTestSetSelection(1)th of these sets as test set
parsTestSetSelection = [5 5 0 0]; % %(1)th of %(2) parts with indices from %(3) to %(4)
strTvSetSelectionId = 'contTvSetSelectionFixed'; % partitions the data into parsTvSetSelection(2) contiguous sets and selects the parsTestSetSelection(1)th of these sets as validation set, the rest is used for training
parsTvSetSelection = [4 4]; % %(1)th of %(2) parts is the validation set
numOfSamples = size(inputData, 1);
[trainIndexSet, valIndexSet, tvIndexSet, testIndexSet] = ...
    selectDataPartition(numOfSamples, strTestSetSelectionId, parsTestSetSelection, ...
    strTvSetSelectionId, parsTvSetSelection, bVisualizeData);

% tvIndexSet = (find(inputData(:,1) < 19))';
% testIndexSet = (find(inputData(:,1) >= 19))';

[minKernelWidth, maxKernelWidth, minCellSize, maxCellSize, ...
    minTimeScale, maxTimeScale, ...
    stepKernelWidth, stepCellSize, stepTimeScale] = selectMetaParametersIntervals(experimentLabel);

resultFilePath = [resultFolder '/' experimentType '/' experimentLabel '/' gdmType '/' strStartTimeId];

if ~exist(resultFilePath, 'dir')
    mkdir(resultFilePath);
end

if bVisualizeData && bSaveVisualization
    count = 0;
    hfigs = get(0, 'children');
    for m=1:1:length(hfigs)
       if strcmp(hfigs(m).Name, 'Optimization Plot')
           saveas(hfigs(m), [ resultFilePath '/nlpd_plot.fig']);
           print(hfigs(m), '-dpng', '-r96', ...
        [ resultFilePath '/nlpd_plot.png']);
        count = count + 1;
       end
       if strcmp(hfigs(m).Name, 'Data Partitioning')
           saveas(hfigs(m), [ resultFilePath '/paritions_plot.fig']);
           print(hfigs(m), '-dpng', '-r96', ...
        [ resultFilePath '/paritions_plot.png']);
        count = count + 1;
       end
       if strcmp(hfigs(m).Name, 'Visualize Input Data')
           saveas(hfigs(m), [ resultFilePath '/data_plot.fig']);
           print(hfigs(m), '-dpng', '-r96', ...
        [ resultFilePath '/data_plot.png']);
        count = count + 1;
       end
       
       if (count > 2)
           break
       end
    end
end

% list of targetTimes for which we want to evaluate.
targetTimeSet = (unique(inputData(testIndexSet, 1)))';
targetTime = [max(targetTimeSet)];% sum(targetTimeSet)/size(targetTimeSet,2) min(targetTimeSet)];
subsetSize = round(size(tvIndexSet,2)/scale);
s = RandStream('mt19937ar','Seed',0);
part = parsTvSetSelection(1,1);

timeScaleSet = [];
if strcmp(experimentLabel, 'corridor')
    part1 = 0.00000:0.00001:0.000095;
    part2 = 0.0001:0.00005:0.002;
    timeScaleSet = [part1, part2];
elseif strcmp(experimentLabel, 'smallnet')
    part1 = 0.00000:0.00005:0.00095;
    part2 = 0.001:0.0005:0.01;
    timeScaleSet = [part1, part2];
else
    timeScaleSet = minTimeScale:stepTimeScale:maxTimeScale;
end

for itr=1:1:totalItr
    close all;
    tvIndexSubSet = randperm(s, size(tvIndexSet,2), subsetSize);
    partitionSize = round(size(tvIndexSubSet,2)/part);
    
    tvIndexSubSet = sort(tvIndexSubSet);
    trainIndexSubSet = tvIndexSubSet(1, 1:partitionSize * (part -1));
    valIndexSubSet = tvIndexSubSet(1, partitionSize * (part -1)+1:end);
    
    optimizationSet = [];
    optNLPD = 10000;
    optKernelWidth = 0; optCellSize = 0; optTimeScale = 0;

    for kernelWidth = minKernelWidth:stepKernelWidth:maxKernelWidth    
        for cellSize = minCellSize:stepCellSize:maxCellSize
            
            if kernelWidth <= cellSize 
                continue;
            end
            
            
            for timeScale = timeScaleSet
                mapCellSize = [cellSize, cellSize];
                [numOfMapCells, mapCenter, mapCellSize, mapLowerLeft] = getMapGeometry(mapCenter, mapSize, mapCellSize);
                mapGeometry = [numOfMapCells, mapCenter, mapCellSize, mapLowerLeft];

                maxKernelEvalRadius = maxKernelEvalRadiusFactor * kernelWidth;
                weightVarianceSigma = weightVarianceSigmaFactor * 1 / ( sqrt(2.0 * pi) * kernelWidth );
                weightVarianceSteepness = weightVarianceSigma * weightVarianceSigma;
                
                [trainGdmMap, kGdmWeightMin, kGdmWeightMax, kGdmMapMin, kGdmMapMax, totalMean, ...
                    kGdmVarMin, kGdmVarMax, sigma2Tot] = ...
                    createGDM(inputData, trainIndexSubSet, ...
                        mapGeometry, ...
                        kernelWidth, maxKernelEvalRadius, weightVarianceSteepness, ...
                        timeScale, targetTime, ...
                        bVerbose);
                [ trainAvgErr, numOfPredictionsMade, ...
                    trainNLPD, numOfPointsForLikelihoodEval] = evaluateGDM(trainGdmMap, ...
                    mapGeometry, inputData, valIndexSubSet, bEpsilon, epsilon);
                
                if trainNLPD < optNLPD
                    optNLPD = trainNLPD;
                    optKernelWidth = kernelWidth;
                    optCellSize = cellSize;
                    optTimeScale = timeScale;
                end
                
                % evaluate the gdm model over the test set
                [testGdmMap, kGdmWeightMin, kGdmWeightMax, kGdmMapMin, kGdmMapMax, totalMean, ...
                    kGdmVarMin, kGdmVarMax, sigma2Tot] = ...
                    createGDM(inputData, tvIndexSubSet, ...
                        mapGeometry, ...
                        kernelWidth, maxKernelEvalRadius, weightVarianceSteepness, ...
                        timeScale, targetTime, ...
                        bVerbose);
                
                [ testAvgErr, numOfPredictionsMade, ...
                    testNLPD, numOfPointsForLikelihoodEval] = evaluateGDM(testGdmMap, ...
                    mapGeometry, inputData, testIndexSet, bEpsilon, epsilon);
                fprintf('(c = %.2f, sigma = %.2f, beta = %.5f) - targetTime = %i ==> (NLPD_train = %.4f, err = %.4f), (NLPD_test = %.4f, err = %.4f)\n', ...
                    cellSize, kernelWidth, timeScale, targetTime, ...
                    trainNLPD, trainAvgErr, testNLPD, testAvgErr);
                optimizationSet = [optimizationSet; kernelWidth, cellSize, timeScale, ...
                    trainNLPD, trainAvgErr, testNLPD, testAvgErr];
            end
        end
    end

    % set the ending time for logging purpose
    endTime = clock;
    strEndTimeId = [sprintf('%i',endTime(1)) '_' ...
        sprintf('%02i',endTime(2)) '_' sprintf('%02i',endTime(3)) '-' ...
        sprintf('%02i',endTime(4)) '_' sprintf('%02i',endTime(5)) '_' sprintf('%02.0f',endTime(6))];
    
    if bSaveLog
        saveLogsOptimization( optimizationSet, ...
            resultFilePath , inputFolder, resultFolder, ...
            strStartTimeId, strEndTimeId, metric, ...
            trainIndexSet, valIndexSet, testIndexSet, ...
            strTestSetSelectionId, parsTestSetSelection, strTvSetSelectionId, parsTvSetSelection, ...
            gdmType, experimentType, experimentLabel, fileName, gasType, sourceLocation, ...
            mapCenter, mapSize, ...
            optKernelWidth, optCellSize, optTimeScale, ...
            minKernelWidth, minCellSize, minTimeScale, ...
            maxKernelWidth, maxCellSize, maxTimeScale, ...
            stepKernelWidth, stepCellSize, stepTimeScale, ...
            targetTime, ...
            bNormalizeData, bSaveVisualization, bEpsilon, epsilon, itr, scale);
    end
    clear optimizationSet;
end

end
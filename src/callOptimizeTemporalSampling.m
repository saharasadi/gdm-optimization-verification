
clear all;
clc;
clear all;
experimentLabel = 'corridor';

resultFolder = 'result_temporal_subsampling';
bNormalizeTime = false;

bEpsilon = false;
for numberOfScales=1:2
    mainOptimizeTemporalSampling(experimentLabel, bEpsilon, bNormalizeTime, resultFolder, numberOfScales);
end
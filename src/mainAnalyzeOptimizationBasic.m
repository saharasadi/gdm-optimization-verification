%% Description: function mainAnalyzeOptimizationBasic
%
% Called by "callAnalyzeBasic.m"
%
% This code, get the file where the grid search results is saved. 
% It reutrns the meta-parameters which optimize GDM for the given metric. 
% It also plots the search space and the metric values for each set of
% meta-parameters.
% 
% input: 
%   gdmType: '2D' (for Kernel DM+V) and/or '2DTD' (for TD Kernel DM+V)
%   experimentLabel: name of the input data file
%   experimentType: the type of the experiment and the folder containing the input data file.
%   time: the timestamp of the last sample in the dataset. 
%       for simulation 2D: '20'
%       for simulation 3D: '100'
%       for real experiments: 'max' refering to the timestamp of the last recorded sample. 
%   plotPath: path to where the output plots has to be saved. 
%
%
%% Implementaiton: mainAnalyzeOptimizationMetric ==========================

function [] = mainAnalyzeOptimizationBasic(experimentLabel, epsilon, gdmType, t, ...
    dataSource, path, bVisualize)

%% Initialization =========================================================
% if one wants to have constant cellsize
%dataSource = dataSource(dataSource(:,2) == 0.25, :); 

%make sure that the kernel width is not very small.
dataSource = dataSource(dataSource(:,1) > 0.15 , :);

if strcmp(gdmType, '2D')
   dataSource = dataSource(dataSource(:,3) < 0.00000000001, :);
elseif strcmp(gdmType, '2DTD')
   dataSource =  dataSource(dataSource(:,3) > 0.0000000001, :);
end
    
%[values, order] = sort(dataSource(:,1));
%optSet = dataSource(order, :);
optSet = dataSource;

% optSet
% kernelWidth, cellSize, timeScale, trainNLPD, trainAvgErr, testNLPD, testAvgErr

% optSet
% kernelWidth, cellSize, timeScale, trainNLPD, trainAvgErr, testNLPD, testAvgErr
%indSet = find (optSet(:, 4) < 0); %if one wants to filter based on NLPD in
%training
indSet = 1:size(optSet, 1);

kernelWidthSet = optSet(indSet, 1);
cellSizeSet  = optSet(indSet, 2);
timeScaleSet = optSet(indSet, 3);
trainNLPD    = optSet(indSet, 4);
trainAvgErr  = optSet(indSet, 5);
testNLPD     = optSet(indSet, 6);
testAvgErr   = optSet(indSet, 7);

if bVisualize
    %% Plotting ===============================================================
    name =[experimentLabel '_epsilon-' epsilon '_' t '_' gdmType];
    h = figure('Name',name,'Color',[1 1 1], 'Position' , [100 300 1200 800]);
    
    % Create axes
    axes1 = axes('Parent',h,...
        'Position',[0.0748299319727891 0.523316062176166 0.857142857142857 0.383419689119171]);
    hold(axes1,'on');

    % Create multiple lines using matrix input to plot
    plot1 = plot([trainNLPD, testNLPD], 'Parent', axes1, 'MarkerSize', 4, ...
        'Marker', 'o', 'LineWidth',1);
    set(plot1(1),'DisplayName','tv data','MarkerFaceColor',[0 0 1], 'Color',[0 0 1]);
    set(plot1(2),'DisplayName','test data','MarkerFaceColor',[1 0 0], 'Color',[1 0 0]);

    title(['Experiment: ' experimentLabel ', t^* = ' t ...
        ',(80% tv, 20% test - const. val set (25%))'], 'FontWeight','bold','FontSize',18);

    % Create ylabel
    ylabel('NLPD','FontWeight','bold','FontSize',16);
    box(axes1,'on');
    set(axes1,'FontSize',14,'XGrid','on','YGrid','on');
    legend(axes1,'show');

    % Create axes
    axes2 = axes('Parent',h,...
        'Position',[0.0755782312925168 0.065958549222798 0.213405797101449 0.341162790697675]);
    hold(axes2,'on');

    % Create plot
    plot(cellSizeSet,'Parent',axes2,'MarkerFaceColor',[0 0 1],'MarkerSize',4,...
        'Marker','o',...
        'LineWidth',1,...
        'Color',[0 0 1]);

    % Create ylabel
    ylabel('cell size (m)','FontWeight','bold','FontSize',14);
    box(axes2,'on');
    set(axes2,'FontSize',12);
    
    % Create axes
    axes3 = axes('Parent',h,...
        'Position',[0.391749482401651 0.065958549222798 0.213405797101446 0.341162790697675]);
    hold(axes3,'on');

    % Create plot
    plot(kernelWidthSet,'Parent',axes3,'MarkerFaceColor',[0 0 1],'MarkerSize',4,...
        'Marker','o',...
        'LineWidth',1,...
        'Color',[0 0 1]);

    % Create ylabel
    ylabel('kernel width (m)','FontWeight','bold');
    box(axes3,'on');
    set(axes3,'FontSize',12);
    
    % Create axes
    axes4 = axes('Parent',h,...
        'Position',[0.717444543034594 0.065958549222798 0.213405797101449 0.341162790697675]);
    hold(axes4,'on');

    % Create plot
    plot(timeScaleSet,'Parent',axes4,'MarkerFaceColor',[0 0 1],'MarkerSize',4,...
        'Marker','o',...
        'LineWidth',1,...
        'Color',[0 0 1]);

    % Create ylabel
    ylabel('time scale (s^{-1})','FontWeight','bold','FontSize',14);
    box(axes4,'on');
    set(axes4,'FontSize',12);

    %% Save the plots.=========================================================
    n = h.Name;
    saveas(h, sprintf('%s/%s.fig', path, n));
    print(h, '-dpng', '-r96', sprintf('%s/%s.png', path, n));
end



%% Find the index where the NLPD has was optimised on the training set.====
[minValTrain, minIdxTrain] = min(trainNLPD);

%% Print out the results ==================================================
fprintf('%s,%s,%s,%s,%.4f,%.4f,%.2f,%.2f,%.9f\n', ...
    gdmType, experimentLabel, t, epsilon, ... 
    testNLPD(minIdxTrain), trainNLPD(minIdxTrain), ...
    kernelWidthSet(minIdxTrain), cellSizeSet(minIdxTrain), timeScaleSet(minIdxTrain));

end
%==========================================================================

close all;
clc;
clear all;

% set the start time - this is used to create a folder in the result path
startTime = clock;
strStartTimeId = [sprintf('%i',startTime(1)) '_' ...
    sprintf('%02i',startTime(2)) '_' sprintf('%02i',startTime(3)) '-' ...
    sprintf('%02i',startTime(4)) '_' sprintf('%02i',startTime(5)) '_' sprintf('%02.0f',startTime(6))];

% initialize variables
gdmTypeSet = ['2D', '2DTD'];
gdmType = '2DTD';
inputFolder = 'data';
resultFolder = 'result_optimization_cv';
experimentLabel = 'with_obstacle_up_16x4';

experimentConfigFile = 'experiment_config.csv';
gdmConfigFile = 'gdm_parameter_config.csv';

% set optimization function
strOptTarget = 'NLPD'; % [NLPD, avErr] optimise for min NLPD

% set kernel constants
weightVarianceSigmaFactor = 1.0;
maxKernelEvalRadiusFactor = 4.0;

% set flags for conditional operations such as plotting, saving logs, etc.
bVisualizeData = true;
bNormalizeData = true;

bVerbose = false;
bSaveLog = true;
bSaveVisualization = true;

% set epsilon flag and epsilon itsel
bEpsilon = true;
epsilon = 0.0000001;

% select experiment corresponding to experimentLabel
[experimentType, fileName, startInd, endInd, ...
    mapCenter, mapSize, sourceLocation, gasType] = selectExperiment(experimentLabel);

% extract inputData considering the experimentLabel, and the cutting
% indices.
startTime = 1;
endTime = 20;
inputData = extractInputData(inputFolder, experimentType, ...
    experimentLabel, fileName, startTime, endTime, startInd, endInd, ...
    bNormalizeData, bVisualizeData);

% Select intervals for meta-parameter set selection
[minKernelWidth, maxKernelWidth, minCellSize, maxCellSize, ...
    minTimeScale, maxTimeScale, ...
    stepKernelWidth, stepCellSize, stepTimeScale] = selectMetaParametersIntervals(experimentLabel);

% set the result path
resultFilePath = [resultFolder '/' experimentType '/' experimentLabel '/' gdmType '/' strStartTimeId];
if ~exist(resultFilePath, 'dir')
    mkdir(resultFilePath);
end

% select partitioning cuts
strTestSetSelectionId = 'contPartTestSetSelection'; % partitions the data into parsTestSetSelection(2) contiguous sets and selects the parsTestSetSelection(1)th of these sets as test set
parsTestSetSelection = [5 5 0 0]; % %(1)th of %(2) parts with indices from %(3) to %(4)
strTvSetSelectionId = 'contTvSetSelectionFixed'; % partitions the data into parsTvSetSelection(2) contiguous sets and selects the parsTestSetSelection(1)th of these sets as validation set, the rest is used for training
parsTvSetSelection = [4 4]; % %(1)th of %(2) parts is the validation set
numOfSamples = size(inputData, 1);

[trainIndexSet, valIndexSet, tvIndexSet, testIndexSet] = ...
    selectDataPartition(numOfSamples, strTestSetSelectionId, parsTestSetSelection, ...
    strTvSetSelectionId, parsTvSetSelection, bVisualizeData);

numOfValSamples = size(valIndexSet, 2);

% tvIndexSet = (find(inputData(:,1) < 19))';
% testIndexSet = (find(inputData(:,1) >= 19))';

targetTimeSet = (unique(inputData(testIndexSet, 1)))';

if bVisualizeData && bSaveVisualization
    count = 0;
    hfigs = get(0, 'children');
    for m=1:1:length(hfigs)
       if strcmp(hfigs(m).Name, 'Optimization Plot')
           saveas(hfigs(m), [ resultFilePath '/nlpd_plot.fig']);
           print(hfigs(m), '-dpng', '-r96', ...
        [ resultFilePath '/nlpd_plot.png']);
        count = count + 1;
       end
       if strcmp(hfigs(m).Name, 'Data Partitioning')
           saveas(hfigs(m), [ resultFilePath '/paritions_plot.fig']);
           print(hfigs(m), '-dpng', '-r96', ...
        [ resultFilePath '/paritions_plot.png']);
        count = count + 1;
       end
       if strcmp(hfigs(m).Name, 'Visualize Input Data')
           saveas(hfigs(m), [ resultFilePath '/data_plot.fig']);
           print(hfigs(m), '-dpng', '-r96', ...
        [ resultFilePath '/data_plot.png']);
        count = count + 1;
       end
       
       if (count > 2)
           break
       end
    end
end

optNLPD = 10000;
optKernelWidth = 0; optCellSize = 0; optTimeScale = 0;
evaluationSet = [];

% list of targetTimes for which we want to evaluate.
targetTimeList = [max(targetTimeSet) sum(targetTimeSet)/size(targetTimeSet,2) min(targetTimeSet)];

for targetTime=targetTimeList
    evaluationSet = [];
    optNLPD = 10000;
    optKernelWidth = 0; optCellSize = 0; optTimeScale = 0;

    for kernelWidth = minKernelWidth:stepKernelWidth:maxKernelWidth    
        for cellSize = minCellSize:stepCellSize:maxCellSize
            
            if kernelWidth <= cellSize 
                continue;
            end
            
            for timeScale = minTimeScale:stepTimeScale:maxTimeScale
                mapCellSize = [cellSize, cellSize];
                [numOfMapCells, mapCenter, mapCellSize, mapLowerLeft] = getMapGeometry(mapCenter, mapSize, mapCellSize);
                mapGeometry = [numOfMapCells, mapCenter, mapCellSize, mapLowerLeft];

                maxKernelEvalRadius = maxKernelEvalRadiusFactor * kernelWidth;
                weightVarianceSigma = weightVarianceSigmaFactor * 1 / ( sqrt(2.0 * pi) * kernelWidth );
                weightVarianceSteepness = weightVarianceSigma * weightVarianceSigma;
                    
                avgNLPD = 0; 
                avgErr = 0;
                for i=1:parsTvSetSelection(1,1)
                   
                   valIndexSet = numOfValSamples * (i-1) + 1:1:min(numOfValSamples * (i), size(tvIndexSet, 2)); 
                   trainIndexSet = [1:valIndexSet(1,1)-1, valIndexSet(end)+1:tvIndexSet(end)];
                   [trainGdmMap, kGdmWeightMin, kGdmWeightMax, kGdmMapMin, kGdmMapMax, totalMean, ...
                        kGdmVarMin, kGdmVarMax, sigma2Tot] = ...
                        createGDM(inputData, trainIndexSet, ...
                            mapGeometry, ...
                            kernelWidth, maxKernelEvalRadius, weightVarianceSteepness, ...
                            timeScale, targetTime, ...
                            bVerbose);
                   [ trainAvgErr, numOfPredictionsMade, ...
                        trainNLPD, numOfPointsForLikelihoodEval] = evaluateGDM(trainGdmMap, ...
                        mapGeometry, inputData, valIndexSet, bEpsilon, epsilon);
                    fprintf('fold: %i --> NLPD = %.5f, Err = %.5f\n', i, trainNLPD, trainAvgErr);
                   avgNLPD =+ trainNLPD; 
                   avgErr =+ trainAvgErr;
                end
                avgNLPD = avgNLPD / parsTvSetSelection(1,1);
                avgErr = avgErr / parsTvSetSelection(1,1);
                
                if trainNLPD < optNLPD
                    optNLPD = trainNLPD;
                    optKernelWidth = kernelWidth;
                    optCellSize = cellSize;
                    optTimeScale = timeScale;
                end
                
                [testGdmMap, kGdmWeightMin, kGdmWeightMax, kGdmMapMin, kGdmMapMax, totalMean, ...
                    kGdmVarMin, kGdmVarMax, sigma2Tot] = ...
                    createGDM(inputData, tvIndexSet, ...
                        mapGeometry, ...
                        kernelWidth, maxKernelEvalRadius, weightVarianceSteepness, ...
                        timeScale, targetTime, ...
                        bVerbose);
                % evaluate the gdm model over the test set
                [ testAvgErr, numOfPredictionsMade, ...
                    testNLPD, numOfPointsForLikelihoodEval] = evaluateGDM(testGdmMap, ...
                    mapGeometry, inputData, testIndexSet, bEpsilon, epsilon);
                fprintf('(c = %.2f, sigma = %.2f, beta = %.5f) - targetTime = %i ==> (NLPD_train = %.4f, err = %.4f), (NLPD_test = %.4f, err = %.4f)\n', ...
                    cellSize, kernelWidth, timeScale, targetTime, ...
                    avgNLPD, avgErr, testNLPD, testAvgErr);
                evaluationSet = [evaluationSet; kernelWidth, cellSize, timeScale, ...
                    trainNLPD, trainAvgErr, testNLPD, testAvgErr];
            end
        end
    end

    % set the ending time for logging purpose
    endTime = clock;
    strEndTimeId = [sprintf('%i',endTime(1)) '_' ...
        sprintf('%02i',endTime(2)) '_' sprintf('%02i',endTime(3)) '-' ...
        sprintf('%02i',endTime(4)) '_' sprintf('%02i',endTime(5)) '_' sprintf('%02.0f',endTime(6))];
    
    if bSaveLog
        saveLogsOptimization( evaluationSet, ...
            resultFilePath, inputFolder, resultFolder, ...
            strStartTimeId, strEndTimeId, experimentConfigFile, gdmConfigFile, ...
            trainIndexSet, valIndexSet, testIndexSet, ...
            strTestSetSelectionId, parsTestSetSelection, strTvSetSelectionId, parsTvSetSelection, ...
            gdmType, experimentType, experimentLabel, gasType, sourceLocation, ...
            mapCenter, mapSize, ...
            optKernelWidth, optCellSize, optTimeScale, ...
            minKernelWidth, minCellSize, minTimeScale, ...
            maxKernelWidth, maxCellSize, maxTimeScale, ...
            stepKernelWidth, stepCellSize, stepTimeScale, ...
            targetTime, ...
            bNormalizeData, bSaveVisualization, bEpsilon, epsilon);
    end
    clear evluationSet;
end

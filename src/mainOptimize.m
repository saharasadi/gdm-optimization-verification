%% Description: function mainOptimize
%
% Called by "callOptimize.m"
%
% This code, reads experiment setup and the input data for the given
% experimentLabel and does a grid search on the given interval and steps of
% meta-parameters. It returns a table where the corresponding metric values
% of each set of parameters for tv and test sets are stored. To find the
% set that minimizes the metric, run callAnalyzeBasic.m or
% callAnalyzeMetric.m.
% 
% input: 
%   experimentLabel: name of the input data file
%   experimentType: the type of the experiment and the folder containing the input data file.
%   bNormalizeTime, bEpsilone: are old constants that haven't been
%       refactored. There are set to false.
%   resultFolder: Path to store the output.
%   scale: For basic GDM this is set to 1 otherwhite indicates the fraction
%       for subsampling.
%   itr: whether or not we need to iterate.
%
%
%% Implementaiton: mainOptimize ==========================

function [] = mainOptimize(experimentLabel, experimentType, bEpsilon, bNormalizeTime, resultFolder, ...
    scale, itr)

% set the start time - this is used to create a folder in the result path
startTime = clock;
strStartTimeId = [sprintf('%i',startTime(1)) '_' ...
    sprintf('%02i',startTime(2)) '_' sprintf('%02i',startTime(3)) '-' ...
    sprintf('%02i',startTime(4)) '_' sprintf('%02i',startTime(5)) '_' sprintf('%02.0f',startTime(6))];

%% Initialization =========================================================
% select gdm type
gdmTypeStrSet = ['2D  '; '2DTD'];
gdmTypeCellSet = cellstr(gdmTypeStrSet);
gdmType = '2DTD';

% input and output path
inputFolder = 'data';
resultFilePath = [resultFolder '/' experimentType '/' experimentLabel '/' gdmType '/' strStartTimeId];

if ~exist(resultFilePath, 'dir')
    mkdir(resultFilePath);
end

% set optimization function
metric = 'NLPD'; % [NLPD, avErr] optimise for min NLPD

% set kernel constants
weightVarianceSigmaFactor = 1.0;
maxKernelEvalRadiusFactor = 4.0;

% set flags for conditional operations such as plotting, saving logs, etc.
bVisualizeData = true;
bNormalizeData = false;
bVerbose = false;
bSaveLog = true;
bSaveVisualization = false;

% set epsilon (not used anymore) but it is not removed since requires code
% refactoring.
epsilon = 0.0000001;

%% Select experiment setup to experimentLabel =====================
[experimentType, fileName, startInd, endInd, ...
    mapCenter, mapSize, sourceLocation, gasType] = selectExperiment(experimentLabel);

%% Extract input data =====================================================
% setting the indices to cut samples when extracting input data.
% indices. This is only for the simulation experiments.
startTime = 1;
if strcmp(experimentType, 'simulation_2d')
    endTime = 20;
else
    endTime = 100;
end
% extract input data
inputData = extractInputData(inputFolder, experimentType, ...
    experimentLabel, fileName, startTime, endTime, startInd, endInd, ...
    bNormalizeData, bNormalizeTime, bVisualizeData);

%% Select partitioning cuts ===============================================
strTestSetSelectionId = 'contPartTestSetSelection'; % partitions the data into parsTestSetSelection(2) contiguous sets and selects the parsTestSetSelection(1)th of these sets as test set
parsTestSetSelection = [5 5 0 0]; % %(1)th of %(2) parts with indices from %(3) to %(4)
strTvSetSelectionId = 'contTvSetSelectionFixed'; % partitions the data into parsTvSetSelection(2) contiguous sets and selects the parsTestSetSelection(1)th of these sets as validation set, the rest is used for training
parsTvSetSelection = [4 4]; % %(1)th of %(2) parts is the validation set
numOfSamples = size(inputData, 1);
[trainIndexSet, valIndexSet, tvIndexSet, testIndexSet] = ...
    selectDataPartition(numOfSamples, strTestSetSelectionId, parsTestSetSelection, ...
    strTvSetSelectionId, parsTvSetSelection, bVisualizeData);

% Note: for TimeVarying evaluation, the partitioning would be different.
% Foe example
% tvIndexSet = (find(inputData(:,1) < 19))';
% testIndexSet = (find(inputData(:,1) >= 19))';

%% Select list of target times for which we want to evaluate GDM on =======
targetTimeSet = (unique(inputData(testIndexSet, 1)))';
targetTimeList = max(targetTimeSet);% other examples: sum(targetTimeSet)/size(targetTimeSet,2) min(targetTimeSet)];

%% Set the search interval and steps for each meta-parameter ==============
[minKernelWidth, maxKernelWidth, minCellSize, maxCellSize, ...
    minTimeScale, maxTimeScale, ...
    stepKernelWidth, stepCellSize, stepTimeScale] = selectMetaParametersIntervals(experimentLabel);

%% Input data and partition visualization =================================
if bVisualizeData && bSaveVisualization
    count = 0;
    hfigs = get(0, 'children');
    for m=1:1:length(hfigs)
       if strcmp(hfigs(m).Name, 'Optimization Plot')
           saveas(hfigs(m), [ resultFilePath '/nlpd_plot.fig']);
           print(hfigs(m), '-dpng', '-r96', ...
        [ resultFilePath '/nlpd_plot.png']);
        count = count + 1;
       end
       if strcmp(hfigs(m).Name, 'Data Partitioning')
           saveas(hfigs(m), [ resultFilePath '/paritions_plot.fig']);
           print(hfigs(m), '-dpng', '-r96', ...
        [ resultFilePath '/paritions_plot.png']);
        count = count + 1;
       end
       if strcmp(hfigs(m).Name, 'Visualize Input Data')
           saveas(hfigs(m), [ resultFilePath '/data_plot.fig']);
           print(hfigs(m), '-dpng', '-r96', ...
        [ resultFilePath '/data_plot.png']);
        count = count + 1;
       end
       
       if (count > 2)
           break
       end
    end
end

close all;

%% Parameter space search to create GDM for every givent targetTime. ======
for targetTime=targetTimeList
    optimizationSet = [];
    optNLPD = 10000;
    optKernelWidth = 0; optCellSize = 0; optTimeScale = 0;

    for kernelWidth = minKernelWidth:stepKernelWidth:maxKernelWidth    
        for cellSize = minCellSize:stepCellSize:maxCellSize
            
            if kernelWidth <= cellSize 
                continue;
            end
            
            for timeScale = minTimeScale:stepTimeScale:maxTimeScale
                
                % Initialize the geometry
                mapCellSize = [cellSize, cellSize];
                [numOfMapCells, mapCenter, mapCellSize, mapLowerLeft] = getMapGeometry(mapCenter, mapSize, mapCellSize);
                mapGeometry = [numOfMapCells, mapCenter, mapCellSize, mapLowerLeft];

                % Initialize kernel variables
                maxKernelEvalRadius = maxKernelEvalRadiusFactor * kernelWidth;
                weightVarianceSigma = weightVarianceSigmaFactor * 1 / ( sqrt(2.0 * pi) * kernelWidth );
                weightVarianceSteepness = weightVarianceSigma * weightVarianceSigma;
                
                % Create GDM for training set
                [trainGdmMap, kGdmWeightMin, kGdmWeightMax, kGdmMapMin, kGdmMapMax, totalMean, ...
                    kGdmVarMin, kGdmVarMax, sigma2Tot] = ...
                    createGDM(inputData, trainIndexSet, ...
                        mapGeometry, ...
                        kernelWidth, maxKernelEvalRadius, weightVarianceSteepness, ...
                        timeScale, targetTime, ...
                        bVerbose);
                    
                % Evaluate GDM on the validation set  
                [ trainAvgErr, numOfPredictionsMade, ...
                    trainNLPD, numOfPointsForLikelihoodEval] = evaluateGDM(trainGdmMap, ...
                    mapGeometry, inputData, valIndexSet, bEpsilon, epsilon);
                
                if trainNLPD < optNLPD
                    optNLPD = trainNLPD;
                    optKernelWidth = kernelWidth;
                    optCellSize = cellSize;
                    optTimeScale = timeScale;
                end
                
                % Create the model on the training + validation (tv) set
                [testGdmMap, kGdmWeightMin, kGdmWeightMax, kGdmMapMin, kGdmMapMax, totalMean, ...
                    kGdmVarMin, kGdmVarMax, sigma2Tot] = ...
                    createGDM(inputData, tvIndexSet, ...
                        mapGeometry, ...
                        kernelWidth, maxKernelEvalRadius, weightVarianceSteepness, ...
                        timeScale, targetTime, ...
                        bVerbose);
                
                % Evaluate the model on the test set.
                [ testAvgErr, numOfPredictionsMade, ...
                    testNLPD, numOfPointsForLikelihoodEval] = evaluateGDM(testGdmMap, ...
                    mapGeometry, inputData, testIndexSet, bEpsilon, epsilon);
                
                % Print out the results
                fprintf('(c = %.2f, sigma = %.2f, beta = %.5f) - targetTime = %i ==> (NLPD_train = %.4f, err = %.4f), (NLPD_test = %.4f, err = %.4f)\n', ...
                    cellSize, kernelWidth, timeScale, targetTime, ...
                    trainNLPD, trainAvgErr, testNLPD, testAvgErr);
                
                % Save the result in the optimizationSet table
                optimizationSet = [optimizationSet; kernelWidth, cellSize, timeScale, ...
                    trainNLPD, trainAvgErr, testNLPD, testAvgErr];
            end
        end
    end

    % set the ending time for logging purpose
    endTime = clock;
    strEndTimeId = [sprintf('%i',endTime(1)) '_' ...
        sprintf('%02i',endTime(2)) '_' sprintf('%02i',endTime(3)) '-' ...
        sprintf('%02i',endTime(4)) '_' sprintf('%02i',endTime(5)) '_' sprintf('%02.0f',endTime(6))];
    
    % Saves the log in a txt file and save the optimizationSet.
    if bSaveLog
        saveLogsOptimization( optimizationSet, ...
            resultFilePath, inputFolder, resultFolder, ...
            strStartTimeId, strEndTimeId, metric, ...
            trainIndexSet, valIndexSet, testIndexSet, ...
            strTestSetSelectionId, parsTestSetSelection, strTvSetSelectionId, parsTvSetSelection, ...
            gdmType, experimentType, experimentLabel, fileName, gasType, sourceLocation, ...
            mapCenter, mapSize, ...
            optKernelWidth, optCellSize, optTimeScale, ...
            minKernelWidth, minCellSize, minTimeScale, ...
            maxKernelWidth, maxCellSize, maxTimeScale, ...
            stepKernelWidth, stepCellSize, stepTimeScale, ...
            targetTime, ...
            bNormalizeData, bSaveVisualization, ...
            itr, scale);
    end
    
    clear optimizationSet;
end

end
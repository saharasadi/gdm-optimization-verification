function [experimentType, fileName, startInd, endInd, mapCenter, mapSize, ...
    sourceLocation, gasType] = selectExperiment(experimentLabel)

if strcmp(experimentLabel, 'smallnet')
    experimentType = 'real';
    fileName = '02_eth_100110';
    startInd = [];
    endInd = [];
    mapCenter = [ 1.5,  1.5]; 
    mapSize = [8,  7]; 
    sourceLocation = [3.20, 1.71 0];
    gasType = 'ethanol';
end

if strcmp(experimentLabel, 'corridor')
    experimentType = 'real';
    fileName = '2008_10_23a-Corridor_Random_bc_ww_0050_csf_00350';
    startInd = 128;
    endInd = 8546; % full experiment
    mapCenter = [8.0, 3.5]; 
    mapSize = [16.0, 10.0];
    sourceLocation = [6.1, 3.2, 1.6];
    gasType = 'ethanol';
end

if strcmp(experimentLabel, 'no_obstacle_16x4')
    experimentType = 'simulation_2d';
    fileName = 'no_obstacle_16x4';
    startInd = [];
    endInd = []; % full experiment
    mapCenter = [8, 2.0]; 
    mapSize = [16, 4.5];
    sourceLocation = [1.0, 2.0, 0.0];
    gasType = 'ethanol';   
end

if strcmp(experimentLabel, 'with_obstacle_up_16x4')
        experimentType = 'simulation_2d';
    fileName = 'with_obstacle_up_16x4';
    startInd = [];
    endInd = []; % full experiment
    mapCenter = [8, 2.0]; 
    mapSize = [16, 4.5];
    sourceLocation = [1.0, 2.0, 0.0];
    gasType = 'ethanol'; 
end

if strcmp(experimentLabel, 'with_obstacle_two_60x20')
    experimentType = 'simulation_ros';
    fileName = 'release_rate_constant_ethanol_y10_h_25t_1-200';
    startInd = [];
    endInd = []; % full experiment
    mapCenter = [30.0, 10]; 
    mapSize = [58, 20];
    sourceLocation = [10, 5, 0.1];
    gasType = 'ethanol';
end

if strcmp(experimentLabel, 'mox_sensor_model')
    experimentType = 'simulation_ros';
    fileName = 'conductance_TGS2620';
    startInd = [];
    endInd = []; % full experiment
    mapCenter = [30.0, 10]; 
    mapSize = [58, 20];
    sourceLocation = [10, 5, 0.1];
    gasType = 'ethanol';
end

end

%% Description: function mainTimeVarying
%
% Called by "callTimeVarying.m"
%
% This code, reads experiment setup and the input data for the given
% experimentLabel and for each time stamp in the target time (test set),
% computers the metric (NLPD) on the test set. At the end it prints out the
% NLPD values.
%
% Note I: Here the assumption is the model parameters are not learned again 
%   but set using the optimization in callOptimize. if you want to learn
%   meta-parameters for each target time, then please use
%   "callOptimizeTimeVarying.me"
% 
% input: 
%   experimentLabel: name of the input data file
%   experimentType: the type of the experiment and the folder containing the input data file.
%   bNormalizeTime, bEpsilone: are old constants that haven't been
%       refactored. There are set to false.
%   inputFolder: Path to the input data
%   resultFolder: Path to store the output.
%
%% Implementaiton: mainTimeVarying ========================================

function [] = mainTimeVarying(experimentLabel, experimentType, bEpsilon, ...
    bNormalizeTime, inputFolder, resultFolder)

% set the start time - this is used to create a folder in the result path
startTime = clock;
strStartTimeId = [sprintf('%i',startTime(1)) '_' ...
    sprintf('%02i',startTime(2)) '_' sprintf('%02i',startTime(3)) '-' ...
    sprintf('%02i',startTime(4)) '_' sprintf('%02i',startTime(5)) '_' sprintf('%02.0f',startTime(6))];

%% Initialization =========================================================
% initialize GDM type
gdmType = '2DTD'; %2DTD means TD Kernel DM+V.

% Set output foders
resultFilePath = [resultFolder '/' experimentType '/' experimentLabel '/' ...
    gdmType '/' strStartTimeId];

if ~exist(resultFilePath, 'dir')
    mkdir(resultFilePath);
end

% set optimization function
metric = 'NLPD'; % [NLPD, avErr] optimise for min NLPD

% set kernel constants
weightVarianceSigmaFactor = 1.0;
maxKernelEvalRadiusFactor = 4.0;

% set flags for conditional operations such as plotting, saving logs, etc.
bVisualizeData = true;
bNormalizeData = true;
bVerbose = false;
bSaveLog = true;
bSaveVisualization = true;

% set epsilon (not used anymore. It is not removed since needs a broader code
% refactoring)
epsilon = 0.0000001;

%% Select experiment corresponding to experimentLabel======================
[experimentType, fileName, startInd, endInd, ...
    mapCenter, mapSize, sourceLocation, gasType] = selectExperiment(experimentLabel);

%% Extract input data. ====================================================
% setting the indices to cut samples when extracting input data.
% indices. This is only for the simulation experiments.
startTime = 1;
if strcmp(experimentType, 'simulation_2d')
    endTime = 25;
else
    endTime = 100;
end

% extract input
inputData = extractInputData(inputFolder, experimentType, ...
    experimentLabel, fileName, startTime, endTime, startInd, endInd, ...
    bNormalizeData, bNormalizeTime, bVisualizeData);

%% Select meta-parameters. ================================================
% Note that here the assumption is the model is already learned, so for the
% time varying study in this code, new parameters are not optimized as we
% vary the target time and the test set.
[kernelWidth, cellSize, timeScale] = selectMetaParameters(experimentLabel);


%% Select training partition. =============================================
tvTime = 0;
if strcmp(experimentType, 'simulation_2d')
    tvTime = 17;
elseif strcmp(experimentType, 'simulation_ros')
    tvTime = 81;
else
    tvTime = floor(size(inputData(:,1),1)* 0.8);
end
tvIndexSet = (find(inputData(:,1) < tvTime))';

% recoding time of all measurements.
time = inputData(:,1);

%% Iterate to create maps for the test data being from a single time stamp
% in future compared to tv set.
for i=tvTime:1:endTime
    
    % Test set indices (test set includes only samples collected at timestam i).
    testIndexSet = (find(time(:,1) == i))';
    
    % Target time is equal to the timestamp of the test set.
    targetTime = inputData(testIndexSet(1,1), 1);
    
    % Initialize the geometry
    mapCellSize = [cellSize, cellSize];
    [numOfMapCells, mapCenter, mapCellSize, mapLowerLeft] = getMapGeometry(mapCenter, mapSize, mapCellSize);
    mapGeometry = [numOfMapCells, mapCenter, mapCellSize, mapLowerLeft];

    % Initialize Kernel variables.
    maxKernelEvalRadius = maxKernelEvalRadiusFactor * kernelWidth;
    weightVarianceSigma = weightVarianceSigmaFactor * 1 / ( sqrt(2.0 * pi) * kernelWidth );
    weightVarianceSteepness = weightVarianceSigma * weightVarianceSigma;

    % Create GDM on the Training + Validation (tv) set for the given
    % targetTime
    [testGdmMap, kGdmWeightMin, kGdmWeightMax, kGdmMapMin, kGdmMapMax, totalMean, ...
        kGdmVarMin, kGdmVarMax, sigma2Tot] = ...
        createGDM(inputData, tvIndexSet, ...
            mapGeometry, ...
            kernelWidth, maxKernelEvalRadius, weightVarianceSteepness, ...
            timeScale, targetTime, ...
            bVerbose);

    % Evaluate the created model on the test set.     
    [ testAvgErr, numOfPredictionsMade, ...
        testNLPD, numOfPointsForLikelihoodEval] = evaluateGDM(testGdmMap, ...
        mapGeometry, inputData, testIndexSet, bEpsilon, epsilon);
    
    % Print out the output.
    fprintf('%s,%s,%i,%s,%.4f,%.2f,%.2f,%.5f\n', ...
        experimentLabel, gdmType, i, bEpsilon, ... 
        testNLPD, kernelWidth, cellSize, timeScale);

end

end


function [inputData] = extractInputData(inputFolder, experimentType, ...
    experimentLabel, fileName, startTime, endTime, startInd, endInd, ...
    bNormalizeData, bNormalizeTime, bVisualizeData)

inputData = [];
dataFilePath = [inputFolder '/' experimentType '/' experimentLabel];

% call extract input data according to the experiment type and label
if strcmp(experimentType, 'simulation_2d')
    inputData = extractInputDataFromSimulation(dataFilePath, fileName);
else
    if strcmp(experimentType, 'simulation_ros')
        inputData = extractInputDataFromSimulationROS(dataFilePath, fileName);
        marginX = 1;
        maxX= max(inputData(:,3)) - marginX;
        inputData = inputData(inputData(:,3) <= maxX, :);
    else
       if strcmp(experimentType, 'real')
           if strcmp(experimentLabel, 'corridor')
               inputData = extractInputDataFromCorridor(dataFilePath, fileName);
           else
               inputData = extractInputDataFromSmallNet(dataFilePath, fileName);
           end
       end
    end
end
    
if size(inputData) == 0
    fprintf('Error in reading input data!');
    return;
end

%select data within specific date range
if strcmp(experimentType, 'simulation_2d') || strcmp(experimentType, 'simulation_ros')
   inputData = inputData(inputData(:,1) >= startTime, :);
   inputData = inputData(inputData(:,1) <= endTime, :);
end

if bNormalizeTime
    inputData(:,1) = 100 * (inputData(:,1) - min(inputData(:,1))) / (max(inputData(:,1)) - min(inputData(:,1))) + 1;
end

% normalize gas measurements
if bNormalizeData
    colMin = min(inputData(:,2));
    colMax = max(inputData(:,2));
    if (colMin < colMax)
        inputData(:,2) = (inputData(:,2) - colMin)/(colMax - colMin);
    end
end

rawNumOfDataPoints = size(inputData,1);
if(size(startInd,2) == 0)
    startInd = 1;
end
if(size(endInd,2) == 0)
    endInd = rawNumOfDataPoints;
end
inputData = inputData(startInd:endInd,:);

if bVisualizeData
    h = figure;
    h.Name = 'Visualize Input Data';
%   subplot(2,1,1);
    plot(inputData(:,3), inputData(:,4), 'bs');
    xlabel('X (m)'); ylabel('Y (m)');
    grid on;
    title(sprintf('%s - Sampling Locaitons', experimentLabel));
%     
%     subplot(2,1,2);
%     plot3(inputData(:,3), inputData(:,4), inputData(:,2), 'bs');
%     title(sprintf('%s - Data Set', experimentLabel));
end

end
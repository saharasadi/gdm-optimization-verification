function [ avPredictionError, numOfPredictionsMade, ...
    avgNegLogLikelihood, numOfPointsForLikelihoodEval] = evaluateGDM(kGdmvMap, ...
    mapGeometry, inputData, evalSetInd, bEpsilon, epsilon)

maxNpmMessages1 = 2;
maxNpmMessages2 = 2;
maxNpmMessages3 = 2;
npmMessages1 = 0;
npmMessages2 = 0;
npmMessages3 = 0;

%fprintf('Compute average prediction error and total neg. log likelihood at the data points ...\n');
totalPredictionError = 0.0;
totalNegLogLikelihood = 0.0;
numOfPredictionsMade = 0;
numOfPointsForLikelihoodEval = 0;
numOfSensors = 1; %(size(inputData,2) - 1)/3;
%numOfDataPoints = size(evalSetInd,1);

for nDataPoint = evalSetInd    
    for nSensor = 1:1:numOfSensors
        % Read values for the current data point
        r = inputData(nDataPoint, 2 + (nSensor - 1) * 3 + 0);
        x = inputData(nDataPoint, 2 + (nSensor - 1) * 3 + 1);
        y = inputData(nDataPoint, 2 + (nSensor - 1) * 3 + 2);        

        % Determine grid cell corresponding to the measurement
        [ cellX, cellY ] = gridCoordinates(x,y,mapGeometry,true);

        if (cellX ~= Inf) && (cellY ~= Inf) % ignore cells outside the map
            predR = kGdmvMap(cellX,cellY,4);
            predV2 = kGdmvMap(cellX,cellY,5);
            deltaR = r - predR;
            deltaR2 = deltaR * deltaR;
            
            % compute prediction error
            if predR >= 0 % consider only valid predictions
                totalPredictionError = totalPredictionError + deltaR2;                  % --> avPredError
                numOfPredictionsMade = numOfPredictionsMade + 1;
            else
                npmMessages1 = npmMessages1 + 1;
                if npmMessages1 <= maxNpmMessages1
                    fprintf('NO PREDICTION MADE (predR < 0) for data point %i at (%.2f, %.2f) -- predR = %.f\n', nDataPoint, x, y, predR);
                    fprintf('Map Geometry: (%i, %i), center: (%.3f, %.3f), cell size: (%.3f, %.3f), lower left: (%.3f, %.3f)\n', ...
                        mapGeometry(1), mapGeometry(2), mapGeometry(3), mapGeometry(4), ...
                        mapGeometry(5), mapGeometry(6), mapGeometry(7), mapGeometry(8));
                    fprintf('x = %.3f, y = %.3f, r = %.3f\n', x, y, r);
                end
            end
            
            if bEpsilon && predV2 < epsilon
                predV2 = epsilon;
            end
            
            % compute NLPD 
            if (predR >= 0) && (predV2 > 0) % consider only valid predictions
                negLogLikelihood = 0.5 * ( log(2 * pi * predV2) + deltaR2 / (predV2) ); % --> NLPD
                totalNegLogLikelihood = totalNegLogLikelihood + negLogLikelihood;
                numOfPointsForLikelihoodEval = numOfPointsForLikelihoodEval + 1;
            else
                npmMessages2 = npmMessages2 + 1;
                if npmMessages2 <= maxNpmMessages2
                    fprintf('NO PREDICTION MADE (predR < 0 or predV2 < 0) for data point %i at (%.2f, %.2f) -- predR = %.f, predV2 = %.f\n', nDataPoint, x, y, predR, predV2);
                    fprintf('Map Geometry: (%i, %i), center: (%.3f, %.3f), cell size: (%.3f, %.3f), lower left: (%.3f, %.3f)\n', ...
                        mapGeometry(1), mapGeometry(2), mapGeometry(3), mapGeometry(4), ...
                        mapGeometry(5), mapGeometry(6), mapGeometry(7), mapGeometry(8));
                    fprintf('x = %.3f, y = %.3f, r = %.3f\n', x, y, r);
                end
            end
        else
            npmMessages3 = npmMessages3 + 1;
            if npmMessages3 <= maxNpmMessages3
                fprintf('NO PREDICTION MADE for data point %i at (%.2f, %.2f)\n', nDataPoint, x, y);
                fprintf('Map Geometry: (%i, %i), center: (%.3f, %.3f), cell size: (%.3f, %.3f), lower left: (%.3f, %.3f)\n', ...
                    mapGeometry(1), mapGeometry(2), mapGeometry(3), mapGeometry(4), ...
                    mapGeometry(5), mapGeometry(6), mapGeometry(7), mapGeometry(8));
                fprintf('x = %.3f, y = %.3f, r = %.3f\n', x, y, r);
            end
        end
    end
end
%fprintf('Done!\n');

if numOfPredictionsMade ~= 0
    avPredictionError = ...
        totalPredictionError / numOfPredictionsMade;              % --> avPredError
else
    avPredictionError = Inf;
end

if numOfPointsForLikelihoodEval ~= 0
    avgNegLogLikelihood = ...
            totalNegLogLikelihood / numOfPointsForLikelihoodEval; % --> NLPD
else
    avgNegLogLikelihood = Inf;
end

end
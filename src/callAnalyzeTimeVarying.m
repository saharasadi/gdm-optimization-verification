%% Description callAnalyzeTimeVarying.m
%   written by: Sahar Asadi, 
%   Last day released: 2017-02-27
%   
% This code, get the file where the grid search for timevaring optimization is saved. 
% It reutrns the meta-parameters which optimize GDM for the target time. 
% If the bVisualize is set to It also plots the search space and the metric values for each set of
% meta-parameters.
% 
% important variables: 
%   gdmType: '2DTD' (for TD Kernel DM+V).
%   timeSet: you need to specify the target time set for which you want to get the
%       corrsponding NLPD values.
%   experimentType, experimentLabel: specify the dataset to be used.
%   plotPath: path to where the output plots have to be saved. 
%   bVisualize: whether or not plot the NLPD values for the values in kernel
%       width, cell size, and time scale.
%--------------------------------------------------------------------------   

close all;
clc;
clear;

%% Initialization =========================================================
% set input experiment
% [experimentType, experimentLabel] = ['real', 'corridor']
%                                     ['real', 'smallnet'] 
%                                     ['simulation_2d', 'no_obstacle_16x4'] 
%                                     ['simulation_2d', 'with_obstacle_up_16x4'] 
%                                     ['simulation_ros', 'mox_sensor_model']
%                                     ['simulation_ros', 'with_obstacle_two_60x20']
% for simulation ros, you need to add the path manually.
experimentType = 'simulation_2d';
experimentLabel = 'with_obstacle_up_16x4';

% Set gdm type. For time varying it should be 2DTD (meaning TD Kernel DM+V)
gdmType = '2DTD';

% constant value
epsilon = 'false';

% Set input and output path
pathPrefix = 'result_optimization_timevarying';

dataPath = [ pathPrefix '/' experimentType '/' experimentLabel '/' ...
    gdmType '/' 'epsilon_' epsilon];

plotPath = [pathPrefix '/' 'plots' '/' experimentLabel];
if ~exist(plotPath, 'dir')
    mkdir(plotPath);
end

% flag to visualize data
bVisualize = false;

%% Set the list of targetTimes for which we want to evaluate. =============
timeSet = [];
if strcmp(experimentType, 'simulation_2d')
    timeSet = 17:1:20; %, 21, 22, 23, 24, 25];
elseif strcmp(experimentType, 'simulation_ros')
    timeSet = [81, 82, 83, 84, 85, 90, 95, 100]; %, 105, 110, 115, 120, 125]; %, 21, 22, 23, 24, 25];
else
    timeSet = [max(targetTimeSet) sum(targetTimeSet)/size(targetTimeSet,2) min(targetTimeSet)];
end

%% Run mainAnalysisOptimizationBasic for each time in the timeState =======
for time=timeSet
    dataFile = [dataPath '/' 'log_optimization_description_target_time_' ...
        int2str(time) '.mat'];
    dataSource = load(dataFile);
    mainAnalyzeOptimizationBasic(experimentLabel, epsilon, gdmType, int2str(time), ...
        dataSource.optimizationSet, plotPath, bVisualize);
end


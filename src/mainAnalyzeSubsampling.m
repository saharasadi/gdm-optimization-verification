%% Description: function mainAnalyzeSubsampling
%
% Called by "callAnalyzeSubsampling.m"
%
% This code, get the file where the grid search results for each partition of
% the subsampling scale is saved. 
% It reutrns the meta-parameters which optimize GDM on NLPD. 
% If bVisualize flag is set to true, it plots the search space and the metric values for each set of
% meta-parameters.
% 
% input: 
% important input: 
%   gdmType: '2D' 
%   experimentLabel: name of the input data file
%   experimentType: the type of the experiment and the folder containing the input data file.
%   partition: assuming that the scale is i. which partition of the scale.
%   scale: the scales for which we want the analysis on the subsampling 
%       (2 refers to 50% and 3 refers to 33%).
%   bVisualize: A flag for visualizing the NLPD trends in the
%       meta-parameters search space.
%   plotPath: path to where the output plots have to be saved. 
%
%% Implementaiton: mainAnalyzeSubsampling ==========================

function [] = mainAnalyzeSubsampling(experimentLabel, epsilon, ...
    gdmType, partition, scale, ...
    dataSource, path, bVisualize)

%close all;
dataSource = dataSource(dataSource(:,1) > 0.15 , :);
  
%% Initialization ==========================================================
%[values, order] = sort(dataSource(:,1));
%optSet = dataSource(order, :);
optSet = dataSource;

% optSet
% kernelWidth, cellSize, timeScale, trainNLPD, trainAvgErr, testNLPD, testAvgErr
indSet = 1:size(optSet, 1);

kernelWidthSet = optSet(indSet, 1);
cellSizeSet  = optSet(indSet, 2);
timeScaleSet = optSet(indSet, 3);
trainNLPD    = optSet(indSet, 4);
trainAvgErr  = optSet(indSet, 5);
testNLPD     = optSet(indSet, 6);
testAvgErr   = optSet(indSet, 7);

% Visualization ===========================================================
if bVisualize
    name =[experimentLabel '_scale_' scale '_partition_' partition '_epsilon-' epsilon '_'];
    h = figure('Name',name,'Color',[1 1 1], 'Position' , [100 300 1200 800]);
    % Create axes
    axes1 = axes('Parent',h,...
        'Position',[0.0748299319727891 0.523316062176166 0.857142857142857 0.383419689119171]);
    hold(axes1,'on');

    % Create multiple lines using matrix input to plot
    plot1 = plot([trainNLPD, testNLPD],'Parent',axes1,'MarkerSize',4,'Marker','o',...
        'LineWidth',1);
    set(plot1(1),'DisplayName','tv data','MarkerFaceColor',[0 0 1],...
        'Color',[0 0 1]);
    set(plot1(2),'DisplayName','test data','MarkerFaceColor',[1 0 0],...
        'Color',[1 0 0]);

    title(['Experiment: sim\_2d ' experimentLabel '- \epsilon=' epsilon, ...
        ',(80% tv, 20% test - constant val set (25%))'], 'FontWeight','bold','FontSize',18);

    % Create ylabel
    ylabel('NLPD','FontWeight','bold','FontSize',16);

    box(axes1,'on');
    % Set the remaining axes properties
    set(axes1,'FontSize',14,'XGrid','on','YGrid','on');
    % Create legend
    legend(axes1,'show');

    % Create axes
    axes2 = axes('Parent',h,...
        'Position',[0.0755782312925168 0.065958549222798 0.213405797101449 0.341162790697675]);
    hold(axes2,'on');

    % Create plot
    plot(cellSizeSet,'Parent',axes2,'MarkerFaceColor',[0 0 1],'MarkerSize',4,...
        'Marker','o',...
        'LineWidth',1,...
        'Color',[0 0 1]);

    % Create ylabel
    ylabel('cell size (m)','FontWeight','bold','FontSize',14);

    box(axes2,'on');
    % Set the remaining axes properties
    set(axes2,'FontSize',12);
    % Create axes
    axes3 = axes('Parent',h,...
        'Position',[0.391749482401651 0.065958549222798 0.213405797101446 0.341162790697675]);
    hold(axes3,'on');

    % Create plot
    plot(kernelWidthSet,'Parent',axes3,'MarkerFaceColor',[0 0 1],'MarkerSize',4,...
        'Marker','o',...
        'LineWidth',1,...
        'Color',[0 0 1]);

    % Create ylabel
    ylabel('kernel width (m)','FontWeight','bold');

    box(axes3,'on');
    % Set the remaining axes properties
    set(axes3,'FontSize',12);
    % Create axes
    axes4 = axes('Parent',h,...
        'Position',[0.717444543034594 0.065958549222798 0.213405797101449 0.341162790697675]);
    hold(axes4,'on');

    % Create plot
    plot(timeScaleSet,'Parent',axes4,'MarkerFaceColor',[0 0 1],'MarkerSize',4,...
        'Marker','o',...
        'LineWidth',1,...
        'Color',[0 0 1]);

    % Create ylabel
    ylabel('time scale (s^{-1})','FontWeight','bold','FontSize',14);

    box(axes4,'on');
    % Set the remaining axes properties
    set(axes4,'FontSize',12);


    n = h.Name;
    saveas(h, sprintf('%s/%s.fig', path, n));
    print(h, '-dpng', '-r96', sprintf('%s/%s.png', path, n));
end 

%% Selection of the indices of the optimal NLPD values. ================
[minValTrain, minIdxTrain] = min(trainNLPD);

%% Print out the results ===============================================
fprintf('%s,%s,%s,%s,%s,%.4f,%.4f,%.2f,%.2f\n', ...
    gdmType, experimentLabel, scale, partition, epsilon, ... 
    testNLPD(minIdxTrain), trainNLPD(minIdxTrain), ...
    kernelWidthSet(minIdxTrain), cellSizeSet(minIdxTrain));


end
%% Description callVerify.m
%   written by: Sahar Asadi, 
%   Last day released: 2017-02-27
%   
% This code is developed for the purpose of quick sanity checks and
% verifications. It gets an experiement and set of meta-parameters, it
% creates maps and returns the corresponding NLPD values for the training
% and test. This helps to make sure results from optimization are reproducable.
% 
% Note: You can use "selectMetaParameters.m" or set the meta-parameters 
% manually in this code.
%==========================================================================

close all;
clc;
clear;

% set the start time - this is used to create a folder in the result path
startTime = clock;
strStartTimeId = [sprintf('%i',startTime(1)) '_' ...
    sprintf('%02i',startTime(2)) '_' sprintf('%02i',startTime(3)) '-' ...
    sprintf('%02i',startTime(4)) '_' sprintf('%02i',startTime(5)) '_' sprintf('%02.0f',startTime(6))];

%% Initialization =========================================================
% set input experiment
% [experimentType, experimentLabel] = ['real', 'corridor']
%                                     ['real', 'smallnet'] 
%                                     ['simulation_2d', 'no_obstacle_16x4'] 
%                                     ['simulation_2d', 'with_obstacle_up_16x4'] 
%                                     ['simulation_ros', 'mox_sensor_model']
%                                     ['simulation_ros', 'with_obstacle_two_60x20']
% for simulation ros, you need to add the path manually.
experimentType = 'real';
experimentLabel = 'corridor';

% select gdm type
gdmTypeStrSet = ['2D  '; '2DTD'];
gdmTypeCellSet = cellstr(gdmTypeStrSet);
gdmType = '2DTD';

% set optimization function
metric = 'NLPD'; % [NLPD, avErr] optimise for min NLPD

% set kernel constants
weightVarianceSigmaFactor = 1.0;
maxKernelEvalRadiusFactor = 4.0;

% set flags for conditional operations such as plotting, saving logs, etc.
bVisualizeData = true;
bNormalizeData = true;
bNormalizeTime = false;
bVerbose = false;
bSaveLog = true;
bSaveVisualization = true;

% flag for epsilon in evaluation. This has to stay false (not using epsilon
% in calculations)
bEpsilon = false;
epsilon = 0.0000001;

% setting input and output folder
inputFolder = 'data';
resultFolder = 'result';
resultFilePath = [resultFolder '/' experimentType '/' experimentLabel '/' gdmType '/' strStartTimeId];
if ~exist(resultFilePath, 'dir')
    mkdir(resultFilePath);
end

%% Select experiment setup to experimentLabel =====================
[experimentType, fileName, startInd, endInd, ...
    mapCenter, mapSize, sourceLocation, gasType] = selectExperiment(experimentLabel);

%% Extract input data =====================================================
% setting the indices to cut samples when extracting input data.
% indices. This is only for the simulation experiments.
startTime = 1;
if strcmp(experimentType, 'simulation_2d')
    endTime = 20;
else
    endTime = 100;
end

% extract inputData
inputData = extractInputData(inputFolder, experimentType, ...
    experimentLabel, fileName, startTime, endTime, startInd, endInd, ...
    bNormalizeData, bNormalizeTime, bVisualizeData);

%% Select partitioning cuts ===============================================
strTestSetSelectionId = 'contPartTestSetSelection'; % partitions the data into parsTestSetSelection(2) contiguous sets and selects the parsTestSetSelection(1)th of these sets as test set
parsTestSetSelection = [5 5 0 0]; % %(1)th of %(2) parts with indices from %(3) to %(4)
strTvSetSelectionId = 'contTvSetSelectionFixed'; % partitions the data into parsTvSetSelection(2) contiguous sets and selects the parsTestSetSelection(1)th of these sets as validation set, the rest is used for training
parsTvSetSelection = [4 4]; % %(1)th of %(2) parts is the validation set
numOfSamples = size(inputData, 1);
[trainIndexSet, valIndexSet, tvIndexSet, testIndexSet] = ...
    selectDataPartition(numOfSamples, strTestSetSelectionId, parsTestSetSelection, ...
    strTvSetSelectionId, parsTvSetSelection, bVisualizeData);

% Note: for TimeVarying evaluation, the partitioning would be different.
% Foe example
% tvIndexSet = (find(inputData(:,1) < 19))';
% testIndexSet = (find(inputData(:,1) >= 19))';

%% Set the meta-parameters ================================================
% You can either read it from "selectMetaParameters" or initialize them
% manually here.
[kernelWidth, cellSize, timeScale] = selectMetaParameters(experimentLabel);
%kernelWidth = 0.90;
%cellSize = 0.35;
%timeScale = 1.35 * 10^(-3);

if strcmp(gdmType, '2D')
    timeScale = 0.0;
end

%% Select Target Time =====================================================
targetTimeSet = (unique(inputData(testIndexSet, 1)))';
% list of targetTimes for which we want to evaluate.
targetTimeList = max(targetTimeSet); %other examples: sum(targetTimeSet)/size(targetTimeSet,2) min(targetTimeSet)];

%% Creat GDM and Evaluate for different targetTime values. ================
result = [];
for targetTime=targetTimeList  
    % Initializing the geometry
    mapCellSize = [cellSize, cellSize];
    [numOfMapCells, mapCenter, mapCellSize, mapLowerLeft] = getMapGeometry(mapCenter, mapSize, mapCellSize);
    mapGeometry = [numOfMapCells, mapCenter, mapCellSize, mapLowerLeft];

    % Initializing kernel variables
    maxKernelEvalRadius = maxKernelEvalRadiusFactor * kernelWidth;
    weightVarianceSigma = weightVarianceSigmaFactor * 1 / ( sqrt(2.0 * pi) * kernelWidth );
    weightVarianceSteepness = weightVarianceSigma * weightVarianceSigma;
    
    % Create a GDM for the given meta-parameters on the Training + Validation set
    % and for the given targetTime
    [kGdmvMap, kGdmWeightMin, kGdmWeightMax, kGdmMapMin, kGdmMapMax, totalMean, ...
        kGdmVarMin, kGdmVarMax, sigma2Tot] = createGDM(inputData, tvIndexSet, ...
        mapGeometry, ...
        kernelWidth, maxKernelEvalRadius, weightVarianceSteepness, ...
        timeScale, targetTime, ...
        bVerbose);
    
    % Evaluate the gdm model over the test set
    [ avPredictionError, numOfPredictionsMade, ...
        NLPD, numOfPointsForLikelihoodEval] = evaluateGDM(kGdmvMap, ...
        mapGeometry, inputData, testIndexSet, bEpsilon, epsilon);
    fprintf('(c = %.2f, sigma = %.2f, beta = %.5f) - targetTime = %i ==> NLPD = %.4f, err = %.4f\n', ...
        cellSize, kernelWidth, timeScale, targetTime, NLPD, avPredictionError);
    
    % Store the results in a matrix
    result = [result; targetTime NLPD avPredictionError];
end

% set the ending time for logging purpose
endTime = clock;
strEndTimeId = [sprintf('%i',endTime(1)) '_' ...
    sprintf('%02i',endTime(2)) '_' sprintf('%02i',endTime(3)) '-' ...
    sprintf('%02i',endTime(4)) '_' sprintf('%02i',endTime(5)) '_' sprintf('%02.0f',endTime(6))];

%% Visualize Data =========================================================
if bVisualizeData && bSaveVisualization
    count = 0;
    hfigs = get(0, 'children');
    for m=1:1:length(hfigs)
       if strcmp(hfigs(m).Name, 'Optimization Plot')
           saveas(hfigs(m), [ resultFilePath '/nlpd_plot.fig']);
           print(hfigs(m), '-dpng', '-r96', ...
        [ resultFilePath '/nlpd_plot.png']);
        count = count + 1;
       end
       if strcmp(hfigs(m).Name, 'Data Partitioning')
           saveas(hfigs(m), [ resultFilePath '/paritions_plot.fig']);
           print(hfigs(m), '-dpng', '-r96', ...
        [ resultFilePath '/paritions_plot.png']);
        count = count + 1;
       end
       if strcmp(hfigs(m).Name, 'Visualize Input Data')
           saveas(hfigs(m), [ resultFilePath '/data_plot.fig']);
           print(hfigs(m), '-dpng', '-r96', ...
        [ resultFilePath '/data_plot.png']);
        count = count + 1;
       end
       
       if (count > 2)
           break
       end
    end
end

%% Save the logs in a file ================================================
if bSaveLog
    saveLogs(result, resultFilePath, inputFolder, resultFolder, ...
        strStartTimeId, strEndTimeId, metric, ...
        trainIndexSet, valIndexSet, testIndexSet, ...
        gdmType, experimentType, experimentLabel, gasType, sourceLocation, ...
        mapCenter, mapSize, mapCellSize, ...
        kernelWidth, cellSize, timeScale, ...
        strTestSetSelectionId, parsTestSetSelection, strTvSetSelectionId, parsTvSetSelection, ...
        bNormalizeData, bSaveVisualization, bEpsilon, epsilon);
end


close all;
clc;
clear;

% set input experiment
% [experimentType, experimentLabel] = ['real', 'corridor']
%                                     ['real', 'smallnet'] 
%                                     ['simulation_2d', 'no_obstacle_16x4'] 
%                                     ['simulation_2d', 'with_obstacle_up_16x4'] 
%                                     ['simulation_ros', 'mox_sensor_model']
%                                     ['simulation_ros', 'with_obstacle_two_60x20']
% for simulation ros, you need to add the path manually.
experimentType = 'simulation_ros';
experimentLabel = 'with_obstacle_two_60x20';

resultFolder = 'result_time_varying';
inputFolder = 'data';

% flags for the evaluation. They should not be changed.
bNormalizeTime = false;
bEpsilon = false;

% Call the mainTimeVarying
mainTimeVarying(experimentLabel, experimentType, ...
    bEpsilon, bNormalizeTime, inputFolder, resultFolder);

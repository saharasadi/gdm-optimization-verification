function [ ] = saveLogs( data, logFolder, inputFilePath, resultFolder, ...
    strStartTimeId, strEndTimeId, metric, ...
    trainIndexSet, valIndexSet, testIndexSet, ...
    gdmType, experimentType, experimentLabel, gasType, sourceLocation, ...
    mapCenter, mapSize, mapCellSize, ...
    kernelWidth, cellSize, timeScale, ...
    strTestSetSelectionId, parsTestSetSelection, strTvSetSelectionId, parsTvSetSelection, ...
    bNormalizeData, bSaveVisualization, bEpsilon, epsilon)

% THIS FUNCTION PROVIDES LOGS ABOUT THE PERFORMED EXPERIMENT
   
% log: time
content = sprintf('\n== TIME ===========================\n');

content = [content 'start time :: ' strStartTimeId sprintf('\n')];
content = [content 'end time :: ' strEndTimeId sprintf('\n')];

% log: configs
content = [content sprintf('\n== Metric ===========================\n')];
content = [content 'Metric: ' metric];

% log: input output path
content = [content 'Input Data Path : ' inputFilePath sprintf('\n')];
content = [content 'Result Folder Path : ' resultFolder sprintf('\n\n')];

% log: flags
content = [content sprintf('\n== FLAGS ===========================\n')];
content = [content 'Normalize Data : ' sprintf('%i\n', bNormalizeData)];
content = [content 'Save Visualization : ' sprintf('%i\n', bSaveVisualization)];
epsilonStr = '';
if (bEpsilon)
    epsilonStr = sprintf(' --> %.8f\n', epsilon);
end
content = [content 'Epsilon in Evaluation : ' sprintf('%i', bEpsilon) epsilonStr];

% log: experiment type
content = [content sprintf('\n== EXPERIMENT ===========================\n')];
content = [content 'GDM Type : ' gdmType sprintf('\n\n')];
content = [content 'Experiment:' experimentType ' - ' experimentLabel sprintf('\n\n')];

% log : Geometry
[ numOfMapCells, mapCenter, mapCellSize, mapLowerLeft ] = ...
   getMapGeometry(mapCenter, mapSize, mapCellSize);

content = [content 'map center =  ' sprintf('(%.3f',mapCenter(1)) ' x ' sprintf('%.3f',mapCenter(2)) sprintf(') m^2\n')];
content = [content 'map size   =  ' sprintf('(%.3f',mapSize(1))   ' x ' sprintf('%.3f',mapSize(2)) sprintf(') m^2\n')];
content = [content 'cell size  =  ' sprintf('(%.3f',mapCellSize(1)) ' x ' sprintf('%.3f',mapCellSize(2)) ') m^2' sprintf(' (-> %i x %i cells)\n',numOfMapCells(1),numOfMapCells(2))];
content = [content 'map ll     = ' sprintf('(%.3f',mapLowerLeft(1)) ', ' sprintf('%.3f',mapLowerLeft(2)) sprintf(') m\n')];
content = [content 'map ur     = ' sprintf('(%.3f',mapCenter(1)+mapSize(1)/2) ', ' sprintf('%.3f',mapCenter(2)+mapSize(2)/2) sprintf(') m\n\n')];
content = [content 'source location = ' sprintf('[%.2f, %.2f, %.2f]', sourceLocation(1), sourceLocation(2), sourceLocation(3)) sprintf(') m\n')];
content = [content 'gas type : ' gasType sprintf('\n\n')];

% log: data partitioning
content = [content '== DATA PARTITION ===========================' sprintf('\n')];

content = [content 'parse test set selection: ' strTestSetSelectionId ' - ' ...
    sprintf('[%i %i %i %i]\n', parsTestSetSelection(1,1), parsTestSetSelection(1,2), ...
    parsTestSetSelection(1,2), parsTestSetSelection(1,4))];
content = [content 'parse tv set selection:   ' strTvSetSelectionId ' - ' ...
    sprintf('[%i %i]\n\n', parsTvSetSelection(1,1), parsTvSetSelection(1,2))];

content = [content 'training data index : ' sprintf('[%i:%i]', ...
   trainIndexSet(1,1), trainIndexSet(1,size(trainIndexSet,2))) sprintf('\n')];
content = [content 'validation data index : ' sprintf('[%i:%i]', ...
   valIndexSet(1,1), valIndexSet(1,size(valIndexSet,2))) sprintf('\n')];
content = [content 'test data index : ' ...
   sprintf('[%i:%i]', testIndexSet(1,1), testIndexSet(1, size(testIndexSet,2))) sprintf('\n\n')];

content = [content sprintf('\n== GDM META-PARAMETERS =====================\n')];
content = [content sprintf('(c = %.2f, sigma = %.2f, beta = %.5f)', ...
   cellSize, kernelWidth, timeScale) sprintf('\n')]; 

content = [content sprintf('\n== EVALUATION RESULT =======================\n')];   

for i=1:1:size(data, 1)
   targetTime = data(i, 1);
   NLPD = data(i, 2);
   avPredictionError = data(i, 3);
   content = [content sprintf('(c = %.2f, sigma = %.2f, beta = %.5f) - targetTime = %i ==> NLPD = %.4f, err = %.4f\n', ...
        cellSize, kernelWidth, timeScale, targetTime, NLPD, avPredictionError) sprintf('\n')];
end

content = [content sprintf('\n== END =====================================\n')];

%% SAVE IN THE FILE
logFile = 'log_run_description.txt';
logFileHandler = fopen([logFolder '/' logFile],'w');
fprintf(logFileHandler, '%s\n', content);
fclose(logFileHandler);

end


%% Description: function mainVisualize
%
% Called by "callVisualize.m"
%
% This code, reads the meta-parameters for the given gdmType and the
% experiment label and calls "doVisualize" to create the predictive mean
% and variance as well as condifence maps. 
%
% partitions for training are set to: fixed partitions; 
% The first 80% as training + validation (of which the first 75% are training set and the
% last 25% of validation set) and the last 20% as test set.
% 
% input:   
%   gdmType: '2D' (for Kernel DM+V) and/or '2DTD' (for TD Kernel DM+V)
%   experimentLabel: name of the input data file
%   experimentType: the type of the experiment and the folder containing the input data file.
%   inputDataPath: path to where dataset measurements are stored.
%   outputPath: path to where the output plots have to be saved. 
%==========================================================================

%% mainVisualize function
function [] = mainVisualize(experimentLabel, gdmType, inputFolder, outputPath)

% set kernel constants
weightVarianceSigmaFactor = 1.0;
maxKernelEvalRadiusFactor = 4.0;

% set flags for conditional operations such as plotting, saving logs, etc.
bVisualizeData = false;
bNormalizeData = true;
bNormalizeTime = false;
bVerbose = false;
bSaveLog = true;
bSaveVisualization = false;

% select experiment corresponding to the input experimentLabel
[experimentType, fileName, startInd, endInd, ...
    mapCenter, mapSize, sourceLocation, gasType] = selectExperiment(experimentLabel);

% extract inputData considering the experimentLabel, and the cutting
% indices.
startTime = 1;
endTime = 100;
if strcmp('no_obstacle_16x4', experimentLabel) || strcmp('with_obstacle_up_16x4', experimentLabel)
    endTime = 20;
end

inputData = extractInputData(inputFolder, experimentType, ...
    experimentLabel, fileName, startTime, endTime, startInd, endInd, ...
    bNormalizeData, bNormalizeTime, bVisualizeData);

% select partitioning cuts
strTestSetSelectionId = 'contPartTestSetSelection'; % partitions the data into parsTestSetSelection(2) contiguous sets and selects the parsTestSetSelection(1)th of these sets as test set
parsTestSetSelection = [5 5 0 0]; % %(1)th of %(2) parts with indices from %(3) to %(4)
strTvSetSelectionId = 'contTvSetSelectionFixed'; % partitions the data into parsTvSetSelection(2) contiguous sets and selects the parsTestSetSelection(1)th of these sets as validation set, the rest is used for training
parsTvSetSelection = [4 4]; % %(1)th of %(2) parts is the validation set
numOfSamples = size(inputData, 1);
[trainIndexSet, valIndexSet, tvIndexSet, testIndexSet] = ...
    selectDataPartition(numOfSamples, strTestSetSelectionId, parsTestSetSelection, ...
    strTvSetSelectionId, parsTvSetSelection, bVisualizeData);

targetTimeSet = (unique(inputData(testIndexSet, 1)))';

% get the met-parameters to create GDM models
[kernelWidth, cellSize, timeScale] = selectMetaParameters(experimentLabel);

if strcmp(gdmType, '2D')
    timeScale = 0.0;
end

% list of targetTimes for which we want to evaluate.
targetTime = max(targetTimeSet);  %other examples: sum(targetTimeSet)/size(targetTimeSet,2) min(targetTimeSet)];

% configure geometries given the cellsize
mapCellSize = [cellSize, cellSize];
[numOfMapCells, mapCenter, mapCellSize, mapLowerLeft] = getMapGeometry(mapCenter, mapSize, mapCellSize);
mapGeometry = [numOfMapCells, mapCenter, mapCellSize, mapLowerLeft];

% congifure kernel parameters given the kernel width
maxKernelEvalRadius = maxKernelEvalRadiusFactor * kernelWidth;
weightVarianceSigma = weightVarianceSigmaFactor * 1 / ( sqrt(2.0 * pi) * kernelWidth );
weightVarianceSteepness = weightVarianceSigma * weightVarianceSigma;

% prepare the labels for printing and saving logs.
gdmConfig = sprintf('c%.2f-s%.2f-t%.4f', cellSize, kernelWidth, timeScale);
labelPrefix  = [experimentLabel '_' gdmType '_' gdmConfig];
if strcmp(gdmType, '2DTD')
   labelPrefix = [labelPrefix  sprintf('_targetTime-%i', targetTime)]; 
end

% create a map for the given meta-parameters and targetTime on the
% "training + validation set"
[kGdmvMap, kGdmWeightMin, kGdmWeightMax, kGdmMapMin, kGdmMapMax, totalMean, ...
    kGdmVarMin, kGdmVarMax, sigma2Tot] = createGDM(inputData, tvIndexSet, ...
    mapGeometry, ...
    kernelWidth, maxKernelEvalRadius, weightVarianceSteepness, ...
    timeScale, targetTime, ...
    bVerbose);

mapType = 'tv';
mapPath = [outputPath '/' 'map' '/' mapType];
if (~exist(mapPath, 'dir'))
    mkdir(mapPath)
end

% create a map for the given meta-parameters and targetTime on the "test"
% set
[kGdmvMap, kGdmWeightMin, kGdmWeightMax, kGdmMapMin, kGdmMapMax, totalMean, ...
    kGdmVarMin, kGdmVarMax, sigma2Tot] = createGDM(inputData, testIndexSet, ...
    mapGeometry, ...
    kernelWidth, maxKernelEvalRadius, weightVarianceSteepness, ...
    timeScale, targetTime, ...
    bVerbose);

mapType = 'test';
mapPath = [outputPath '/' 'map' '/' mapType];
if (~exist(mapPath, 'dir'))
    mkdir(mapPath)
end

% call doVisualize to visualize the maps
doVisualize(kGdmvMap, labelPrefix, gdmType, ...
    mapSize, mapPath, ... 
    kernelWidth, maxKernelEvalRadius, weightVarianceSigma, mapGeometry, ...
    kGdmWeightMin, kGdmWeightMax, kGdmMapMin, kGdmMapMax, kGdmVarMin, kGdmVarMax, ...
    totalMean, sigma2Tot);

end
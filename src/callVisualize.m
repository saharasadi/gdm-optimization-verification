%% Description
%   written by: Sahar Asadi, 
%   Last day released: 2017-02-27
%   
% This code, creates the 2D maps of predictive mean and variance maps, confidence map, 
% and the weighted sum of measurements. To create these maps, the meta-parameter
% values set in "selectMetaParameters.m" are used.
% You can define to create a model using basic Kernel DM+V (2D) or TD
% Kernel DM+V (2DTD).
% To create the visualization, this code calls "mainVisualize".
% 
% input: 
%   
%   gdmType: '2D' (for Kernel DM+V) and/or '2DTD' (for TD Kernel DM+V)
%   experimentLabel: name of the input data file
%   experimentType: the type of the experiment and the folder containing the input data file.
%   inputDataPath: path to where dataset measurements are stored.
%   outputPath: path to where the output plots have to be saved. 
%==========================================================================

%% Initialization
clc;
close all;
clear;

% set input experiment
% experimentLabel = 'corridor', 'smallnet',  
%                   'no_obstacle_16x4', 'with_obstacle_up_16x4'] 
%                   'mox_sensor_model',  'with_obstacle_two_60x20'
% for 'with_obstacle_two_60x20' you need to add the path manually if you want to change datasset to 
% the ones with change in conditions such as gas source location, and release rate.
experimentType = 'simulation_2d';
experimentLabel = 'no_obstacle_16x4';

% select for which type of GDM we want to create visualization: kernel DM+V
% (2D) and/or TD Kernel DM+V (2DTD)
gdmType = '2DTD';

dataPath = 'data';
outPath = ['output' '/' experimentType '/' experimentLabel];

%% call mainVisualize for different gdmType
mainVisualize(experimentLabel, gdmType, dataPath, outPath);
%===========================================================================

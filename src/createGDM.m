function [kGdmvMap, kGdmWeightMin, kGdmWeightMax, kGdmMapMin, kGdmMapMax, totalMean, ...
    kGdmVarMin, kGdmVarMax, sigma2Tot] = createGDM(inputData, trainingSetIndices, ...
    mapGeometry, ...
    kernelWidth, maxKernelEvalRadius, weightVarianceSteepness, ...
    timeScale, targetTime, ...
    bVerbose)
% This function is an implementation of the Kernel GDM+V algorithm, which
% can be used for (but is not limited to) gas distribution mapping. 
%
% The kernelGDMpV function returns the following variables
% - kGdmvMap
%   A NxMx6 dimensional matrix that represents the computed map with N, M
%   being the map size in x- and y-direction. The 6 dimensions represent:
%   iCellX,iCellY,1 -> x
%   iCellX,iCellY,2 -> y
%   iCellX,iCellY,3 -> integrated weights  (\Omega^{(k)})
%   iCellX,iCellY,4 -> mean estimates      (r^{(k)})
%   iCellX,iCellY,5 -> variance estimates  (v^{(k)})
%   iCellX,iCellY,6 -> inverse confidence  (\beta^{(k)} = 1 - \alpha^{(k)})

%--------------------------------------------------------------------------
% (0) Compute constants (to improve efficiency)
gaussianFactor = 1 / ( sqrt(2.0 * pi) * kernelWidth); % faster than calling an external function  (approx. twice as fast!)
gaussianExpFactor = - 1 / (2.0 * kernelWidth * kernelWidth);

%--------------------------------------------------------------------------
% (1) Init
% init gas distribution map
mapCellsX = mapGeometry(1,1); % here it is assumed that mapCellsX is an integer, otherwise use "sign(mapGeometry(1,1))*ceil(abs(mapGeometry(1,1)));"
mapCellsY = mapGeometry(1,2); % here it is assumed that mapCellsY is an integer, otherwise use "sign(mapGeometry(1,2))*ceil(abs(mapGeometry(1,2)));"
kGdmvMap = zeros(mapCellsX,mapCellsY,6);

% set (x,y) values in the map
%mapCenterX = mapGeometry(1,3);
%mapCenterY = mapGeometry(1,4);
mapCellSizeX = mapGeometry(1,5); 
mapCellSizeY = mapGeometry(1,6);
mapLowerLeftX = mapGeometry(1,7);
mapLowerLeftY = mapGeometry(1,8);
[kGdmvMap(:,:,1), kGdmvMap(:,:,2)] = ndgrid(...
    mapLowerLeftX+0.5*mapCellSizeX:mapCellSizeX:mapLowerLeftX+mapCellsX*mapCellSizeX, ...
    mapLowerLeftY+0.5*mapCellSizeY:mapCellSizeY:mapLowerLeftY+mapCellsY*mapCellSizeY);
% use 'ndgrid' instead of 'meshgrid' (where one would have to refer to
% cells as (m, n) instead of (n, m) without additional transpose operations) 

% determine number of sensors to be used
numOfSensors = 1; %floor((size(inputData,2) - 1)/3); % works for inputData structure (tEN r1 x1 y1 r2 x2 y2 ... rN xN yN P1 P2) with up to two additional entries P1, P2
showDataPointNumInterval = 10000 * numOfSensors;

% #########################
% For time weight test
% #########################
time = inputData(:,1);
rr(:,1) = inputData(:, 2);
xx(:,1) = inputData(:, 3);
yy(:,1) = inputData(:, 4);        


delta(:,1) = targetTime - time(:,1);
%delta(:,1) = (delta(:,1) - min(delta)) / (max(delta) - min(delta)) * 100;
lambda = 1;
wt(:,1) = lambda * exp(-1 * timeScale * delta(:,1));
% wt = zeros(size(delta,1),1);
% if (timeScale < 0.00000001)
%     wt(:,1) = 1;
% else
%     wt(:,1) = 1./(timeScale * delta(:,1));
% end
%--------------------------------------------------------------------------
% (2) Integration of Weights and Weighted Readings over all Data Points
totalMean = 0.0;
nCount = 0;
totalwt = 0.0;

if bVerbose == true
    fprintf('\nIntegration of weights and weighted readings ...\n');
end
for nCurrDataPoint = trainingSetIndices    
    if bVerbose == true
        if mod(nCount,showDataPointNumInterval) == 0
            fprintf('Data points processed: %i, current sample: %i ...\n', nCount, nCurrDataPoint-1);
        end
        nCount = nCount + numOfSensors;
    end
    for nSensor = 1:1:numOfSensors
        % Read values for the current data point
        
        r = rr(nCurrDataPoint, 1);
        totalwt = totalwt + wt(nCurrDataPoint,1);
        totalMean = totalMean + r * wt(nCurrDataPoint,1);
        x = xx(nCurrDataPoint, 1);
        y = yy(nCurrDataPoint, 1);   
        
        %nDataPoints = nDataPoints + 1;
        %fprintf('Map value %i of sensor %i at (%5.2f,%5.2f) with value %.3f\n',nCurrDataPoint, nSensor, x, y, r);
        
        % Determine area that contains all cells which may be affected by the currently processed sensor reading
        [ minCoordsX, minCoordsY ] = gridCoordinates(x - maxKernelEvalRadius,y - maxKernelEvalRadius,mapGeometry,false);
        [ maxCoordsX, maxCoordsY ] = gridCoordinates(x + maxKernelEvalRadius,y + maxKernelEvalRadius,mapGeometry,false);
        %fprintf('Consider area from (%i,%i) to (%i,%i)\n',minCoordsX,minCoordsY,maxCoordsX,maxCoordsY);

        gridposTot1 = kGdmvMap(minCoordsX:maxCoordsX,minCoordsY:maxCoordsY,1);
        gridposTot2 = kGdmvMap(minCoordsX:maxCoordsX,minCoordsY:maxCoordsY,2);

        distCellCenter2MeasLocation = (gridposTot1 - x).^2 + (gridposTot2 - y).^2; 
        weight = wt(nCurrDataPoint, 1) * gaussianFactor * exp(gaussianExpFactor * distCellCenter2MeasLocation);
        kGdmvMap(minCoordsX:maxCoordsX,minCoordsY:maxCoordsY,3) = kGdmvMap(minCoordsX:maxCoordsX,minCoordsY:maxCoordsY,3) + weight;
        kGdmvMap(minCoordsX:maxCoordsX,minCoordsY:maxCoordsY,4) = kGdmvMap(minCoordsX:maxCoordsX,minCoordsY:maxCoordsY,4) + r * weight;

    end
end
nDataPoints = size(trainingSetIndices,2) * numOfSensors;
totalMean = totalMean / totalwt;
% sigma2Tot = totalR2 - totalMean * totalMean; % PREVIOUSLY USED - replaced by an estimate of sigma2Tot from the variance contributions
%fprintf('Total mean = %.4d\n',totalMean);
if bVerbose == true
    fprintf('Done!\n');
end
% Result from this step:
% -> kGdmvMap(iX,iY,3) (= \Omega^{(k)})
% -> kGdmvMap(iX,iY,4) (= R^{(k)})
% -> totalMean (= \bar{r})
% -> totalR2 (= \bar{r^2})
% -> sigma2Tot (= \sigma^2_{tot}) % PREVIOUSLY USED - replaced by an estimate of sigma2Tot from the variance contributions


%--------------------------------------------------------------------------
% (3) Computation of the Mean Estimates and the Inverse Confidence Map
if bVerbose == true
    fprintf('\nComputation of the inverse confidence map and the mean estimates ...\n');
end
kGdmWeightMin = Inf;
kGdmWeightMax = -Inf;
kGdmMapMin = Inf;
kGdmMapMax = -Inf;
for iX = 1:1:mapCellsX
    for iY = 1:1:mapCellsY
        totalWeight = kGdmvMap(iX,iY,3);
        totalWeightedReading = kGdmvMap(iX,iY,4);
        invConfidence = exp(-(totalWeight * totalWeight) / weightVarianceSteepness);
        kGdmvMap(iX,iY,6) = invConfidence;
        if totalWeight > 0
            kGdmvMap(iX,iY,4) = (1 - invConfidence) * (totalWeightedReading / totalWeight) + invConfidence * totalMean;
        else
            kGdmvMap(iX,iY,4) = totalMean;
        end

        % remember min/max values of total weight and weighted readings
        if kGdmvMap(iX,iY,3) > kGdmWeightMax
            kGdmWeightMax = kGdmvMap(iX,iY,3);
        end
        if kGdmvMap(iX,iY,3) < kGdmWeightMin
            kGdmWeightMin = kGdmvMap(iX,iY,3);
        end
        if kGdmvMap(iX,iY,4) > kGdmMapMax
            kGdmMapMax = kGdmvMap(iX,iY,4);
        end
        if kGdmvMap(iX,iY,4) < kGdmMapMin
            kGdmMapMin = kGdmvMap(iX,iY,4);
        end
    end
end
if bVerbose == true
    fprintf('Done!\n');
end
% Result from this step:
% -> kGdmvMap(iX,iY,6) (= \beta^{(k)})
% -> kGdmvMap(iX,iY,4) (= r^{(k)})


%--------------------------------------------------------------------------
% (4) Integration of Weighted Variance Contributions
totalVarContr = 0.0;
numOfVarContr = 0;
if bVerbose == true
   nCount = 0;
   fprintf('\nIntegration of the weighted variance contributions ...\n');
end
for nCurrDataPoint = trainingSetIndices    
    if bVerbose == true
        if mod(nCount,showDataPointNumInterval) == 0
            fprintf('Data points processed: %04i, current sample: %i ...\n', nCount, nCurrDataPoint-1);
        end
        nCount = nCount + numOfSensors;
    end
    for nSensor = 1:1:numOfSensors
        
        % Get values for the current data point
        x = xx(nCurrDataPoint, 1);
        y = yy(nCurrDataPoint, 1);
        r = rr(nCurrDataPoint, 1);
        
        % determine corresponding predicted value
        [ cellX, cellY ] = gridCoordinates(x,y,mapGeometry,false); % returns always a map cell if last argument is false 
        predR = kGdmvMap(cellX,cellY,4);
        deltaR = r - predR;
        totalVarContr = totalVarContr + deltaR * deltaR * wt(nCurrDataPoint, 1);
        numOfVarContr = numOfVarContr + 1;

        % Determine area that contains all cells which may be affected by the currently processed sensor reading
        [ minCoordsX, minCoordsY ] = gridCoordinates(x - maxKernelEvalRadius,y - maxKernelEvalRadius,mapGeometry,false);
        [ maxCoordsX, maxCoordsY ] = gridCoordinates(x + maxKernelEvalRadius,y + maxKernelEvalRadius,mapGeometry,false);
        %fprintf('Consider area from (%i,%i) to (%i,%i)\n',minCoordsX,minCoordsY,maxCoordsX,maxCoordsY);
 
        gridposTot1 = kGdmvMap(minCoordsX:maxCoordsX,minCoordsY:maxCoordsY,1);
        gridposTot2 = kGdmvMap(minCoordsX:maxCoordsX,minCoordsY:maxCoordsY,2);
        
        distCellCenter2MeasLocation = (gridposTot1 - x).^2 + (gridposTot2 - y).^2; 
        weight = wt(nCurrDataPoint, 1) * gaussianFactor * exp(gaussianExpFactor * distCellCenter2MeasLocation);
        kGdmvMap(minCoordsX:maxCoordsX,minCoordsY:maxCoordsY,5) = kGdmvMap(minCoordsX:maxCoordsX,minCoordsY:maxCoordsY,5) + deltaR^2 * weight;
    end
end
if bVerbose == true
    fprintf('Done!\n');
end
%sigma2Tot =  sqrt(totalVarContr)/numOfVarContr;
sigma2Tot =  sqrt(totalVarContr)/totalwt;
% Result from this step:
% -> kGdmvMap(iX,iY,4) (= V^{(k)})
% -> sigma2Tot (= \sigma^2_{tot})


%--------------------------------------------------------------------------
% (5) Computation of the Variance Estimates
if bVerbose == true
    fprintf('\nComputation of the variance estimates ...\n');
end
kGdmVarMin = Inf;
kGdmVarMax = -Inf;
for iX = 1:1:mapCellsX
    for iY = 1:1:mapCellsY
        totalWeight = kGdmvMap(iX,iY,3);
        totalWeightedVar = kGdmvMap(iX,iY,5);
        invConfidence = kGdmvMap(iX,iY,6);
        if totalWeight > 0
            kGdmvMap(iX,iY,5) = (1 - invConfidence) * (totalWeightedVar / totalWeight) + invConfidence * sigma2Tot;
        else
            kGdmvMap(iX,iY,5) = sigma2Tot;
        end

        % remember min/max values of total weight and weighted readings
        if kGdmvMap(iX,iY,5) > kGdmVarMax
            kGdmVarMax = kGdmvMap(iX,iY,5);
        end
        if kGdmvMap(iX,iY,5) < kGdmVarMin
            kGdmVarMin = kGdmvMap(iX,iY,5);
        end
    end
end
if bVerbose == true
    fprintf('Done!\n');
end
% Result from this step:
% -> kGdmvMap(iX,iY,5) (= v^{(k)})
end
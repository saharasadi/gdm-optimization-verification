%% Description callOptimize.m
%   written by: Sahar Asadi, 
%   Last day released: 2017-02-27
%   
% This code, for a given experiment (dataset), searches in the interval of
% meta-parameters, creates GDM models for each set of meta-parameters, and
% evaluates the model by calculating NLPD. the results will be saved in a
% table. 

% Note I: To find the meta-parameter set that optimizes the target function
% (NLPD in this case), you need to run "callAnalyzeBasic" or
% "callAnalyzeMetric".
% 
% Note II: To visualize the GDM models for a meta-parameter set, you have
% to put these meta-parameters in "selectMetaParameters" and then run
% "callVisualize".
%
% Note III: You can choose to run basic optimization for all meta-parameters or 
% to run subsampling or optimization with constant cell size. For this
% purpose you need to set the flags.
%
% input: 
%   gdmType: '2D' (for Kernel DM+V) and/or '2DTD' (for TD Kernel DM+V)
%   experimentLabel: name of the input data file
%   experimentType: the type of the experiment and the folder containing the input data file.
%   time: the timestamp of the last sample in the dataset. 
%       for simulation 2D: '20'
%       for simulation 3D: '100'
%       for real experiments: 'max' refering to the timestamp of the last recorded sample. 
%  plotPath: path to where the output plots have to be saved. 
%
%
%% Initialization =========================================================

close all;
clc;
clear;

% set input experiment
% [experimentType, experimentLabel] = ['real', 'corridor']
%                                     ['real', 'smallnet'] 
%                                     ['simulation_2d', 'no_obstacle_16x4'] 
%                                     ['simulation_2d', 'with_obstacle_up_16x4'] 
%                                     ['simulation_ros', 'mox_sensor_model']
%                                     ['simulation_ros', 'with_obstacle_two_60x20']
% for simulation ros, you need to add the path manually.
experimentType = 'real';
experimentLabel = 'corridor';

% flags for time and evaluation, these are constant and should not be
% changed.
bNormalizeTime = false;
bEpsilon = false;

% flag to decide if we want to run optimization for constant cell size.
bConstantCellSize = false;

% flag to decide if we want to run optimization for random subsampling with
% scale X.
bRandSubsampling = false;

% flag for basic optimization (to build Kernel DM+V and TD Kernel DM+V)
bBasic = true;

% To run optimization on all meta-parameters
if bBasic
    close all;
    clc;
    scale = 1;
    totalItr = 1;
    resultFolder = 'result_optimization_without_time_norm';
    mainOptimize(experimentLabel, experimentType, ...
        bEpsilon, bNormalizeTime, resultFolder, ...
        scale, totalItr);
end

% To run optimization with constant cell size
if bConstantCellSize
    close all;
    clc;
    resultFolder = 'result_optimization_cell_constant';
    cellSizeConstant = 0.05;
    mainOptimizeWithConstantCellSize(experimentLabel, bEpsilon, bNormalizeTime, ...
        resultFolder, ...
        cellSizeConstant);
end

% To run temportal subsampling in a given scale. 
if bRandSubsampling
    close all;
    clc;
    %scale options 2 (as 50% of samples) and 3 (as 33% of samples).
    scale = 3;
    % totalItr decides on how many times running the optimization. This is
    % for the purpose of confidence interval calcualtion.
    totalItr = 10;
    resultFolder = 'result_temporal_subsampling';
    mainOptimizeSubsampleRand(experimentLabel, bEpsilon, bNormalizeTime, resultFolder, ...
        scale, totalItr);
end

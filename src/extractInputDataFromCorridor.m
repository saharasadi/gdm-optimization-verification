function [inputData] = extractInputDataFromCorridor(dataFilePath, fileName)

numOfSensorsUsed = 1;
sensorsUsed = 9; 
sensorsUsedDescr = 'TGS2620, Nose-A'; 

% Load input data from file
inputDataRaw = load([dataFilePath '/' fileName '.log']);
numOfDataPoints = size(inputDataRaw,1);

numOfSensorsUsed = size(sensorsUsed,2);

%tXY = inputDataRaw(:,1) % (in ms!)
%---
%x_odo = inputDataRaw(:,2);
%y_odo = inputDataRaw(:,3);
%j_odo = inputDataRaw(:,4);
%---
%x = inputDataRaw(:,5);
%y = inputDataRaw(:,6);
%j = inputDataRaw(:,7);
%------------
% Anemometer
%------------
%windU   = inputDataRaw(:,8);
%windV   = inputDataRaw(:,9);
%windW   = inputDataRaw(:,10);
%windUV  = inputDataRaw(:,11); % = wind 2D speed
%windAz  = inputDataRaw(:,12); % = azimuth 2D
%temp    = inputDataRaw(:,13); % wind temperature (anemometer)
%err     = inputDataRaw(:,14); % error check (anemometer)
%xAn     = inputDataRaw(:,15); % x-coordinate of the anemometer
%yAn     = inputDataRaw(:,16); % y-coordinate of the anemometer
%--------------------
% Status Information
%--------------------
%state = inputDataRaw(:,17); % robot state (0: init localization (start), 1: fetch_goal, 2: drive, 3: stopped (measure), 4: end
%-----------------------
% Nose-A, Height: 10 cm
%-----------------------
%tENA  = inputDataRaw(:,18);  % measurement time Nose-A (in ms! - at the moment equals tXY)
%chA1  = inputDataRaw(:,19);  % Sensor 1  <-> TGS 4??? (CO), Nose-A
%chA2  = inputDataRaw(:,20);  % Sensor 2  <-> TGS 2600_1, Nose-A
%chA3  = inputDataRaw(:,21);  % Sensor 3  <-> TGS 2620, Nose-A
%chA4  = inputDataRaw(:,22);  % Sensor 4  <-> TGS 2611, Nose-A
%chA5  = inputDataRaw(:,23);  % Sensor 5  <-> TGS 2602, Nose-A
%chA6  = inputDataRaw(:,24);  % Sensor 6  <-> TGS 2600_2, Nose-A
%tempA = inputDataRaw(:,25);  % temperature, Nose-A
%humA  = inputDataRaw(:,26);  % humidity, Nose-A
%xA    = inputDataRaw(:,27);  % x-coordinate of Nose-A
%yA    = inputDataRaw(:,28);  % y-coordinate of Nose-A
%-----------------------
% Nose-B, Height: 60 cm
%-----------------------
%tENB = inputDataRaw(:,29); % measurement time Nose-B (in ms! - at the moment equals tXY)
%chB1 = inputDataRaw(:,30); % Sensor 7  <-> TGS 2620_1, Nose-B
%chB2 = inputDataRaw(:,31); % Sensor 8  <-> TGS 2620_2, Nose-B
%chB3 = inputDataRaw(:,32); % Sensor 9  <-> TGS 2620_3, Nose-B
%chB4 = inputDataRaw(:,33); % Sensor 10 <-> TGS 2620_4, Nose-B
%chB5 = inputDataRaw(:,34); % Sensor 11 <-> currently unused
%chB6 = inputDataRaw(:,35); % Sensor 12 <-> currently unused
%chB7 = inputDataRaw(:,36); % Sensor 13 <-> currently unused
%chB8 = inputDataRaw(:,37); % Sensor 14 <-> currently unused
%xB   = inputDataRaw(:,38);  % x-coordinate of Nose-B
%yB   = inputDataRaw(:,39);  % y-coordinate of Nose-B
%------------------------
% Nose-C, Height: 110 cm
%------------------------
%tENC  = inputDataRaw(:,40); % measurement time Nose-C (in ms! - at the moment equals tXY)
%chC1  = inputDataRaw(:,41); % Sensor 15 <-> TGS 4??? (CO)
%chC2  = inputDataRaw(:,42); % Sensor 16 <-> TGS 2600_1, Nose-C
%chC3  = inputDataRaw(:,43); % Sensor 17 <-> TGS 2620, Nose-C
%chC4  = inputDataRaw(:,44); % Sensor 18 <-> TGS 2611, Nose-C
%chC5  = inputDataRaw(:,45); % Sensor 19 <-> TGS 2602, Nose-C
%chC6  = inputDataRaw(:,46); % Sensor 20 <-> TGS 2600_2, Nose-C
%tempC = inputDataRaw(:,47); % temperature, Nose-C
%humC  = inputDataRaw(:,48); % humidity, Nose-C
%xC    = inputDataRaw(:,49); % x-coordinate of Nose-C
%yC    = inputDataRaw(:,50); % y-coordinate of Nose-C
%------------------------

% (2) Load calibration file
%calibration = load(calibrationFile);

% (3) Assign columns: raw data structure -> internal data structure
inputData = zeros(numOfDataPoints, numOfSensorsUsed * 3 + 3); 
% 3 values per sensor (c, x, y) or pseudo sensor plus 
% - measurement time
% - error state from anemometer
% - robot state

% (3.1) Add measurement times (assume synchronous measurements)
inputData(:,1) = inputDataRaw(:,1)/1000; % time stamp
% (3.2) Add selected sensors
nextInputDataCol = 2;
nextSensorNum = 0;
%--------
% Nose-A
%--------
% Sensor 1  <-> TGS4161 (CO), Nose-A
nextSensorNum = nextSensorNum + 1;
if (size(find(sensorsUsed == nextSensorNum),2) > 0)
    inputData(:,nextInputDataCol) = inputDataRaw(:,19); % gas sensor reading
    nextInputDataCol = nextInputDataCol + 1;
    inputData(:,nextInputDataCol) = inputDataRaw(:,27); % localization x
    nextInputDataCol = nextInputDataCol + 1;
    inputData(:,nextInputDataCol) = inputDataRaw(:,28); % localization y
    nextInputDataCol = nextInputDataCol + 1;
end
% Sensor 2 <-> TGS2600_1, Nose-A
nextSensorNum = nextSensorNum + 1;
if (size(find(sensorsUsed == nextSensorNum),2) > 0)
    inputData(:,nextInputDataCol) = inputDataRaw(:,20); % gas sensor reading
    nextInputDataCol = nextInputDataCol + 1;
    inputData(:,nextInputDataCol) = inputDataRaw(:,27); % localization x
    nextInputDataCol = nextInputDataCol + 1;
    inputData(:,nextInputDataCol) = inputDataRaw(:,28); % localization y
    nextInputDataCol = nextInputDataCol + 1;
end
% Sensor 3 <-> TGS 2620, Nose-A
nextSensorNum = nextSensorNum + 1;
if (size(find(sensorsUsed == nextSensorNum),2) > 0)
    inputData(:,nextInputDataCol) = inputDataRaw(:,21); % gas sensor reading
    nextInputDataCol = nextInputDataCol + 1;
    inputData(:,nextInputDataCol) = inputDataRaw(:,27); % localization x
    nextInputDataCol = nextInputDataCol + 1;
    inputData(:,nextInputDataCol) = inputDataRaw(:,28); % localization y
    nextInputDataCol = nextInputDataCol + 1;
end
% Sensor 4 <-> TGS 2611, Nose-A
nextSensorNum = nextSensorNum + 1;
if (size(find(sensorsUsed == nextSensorNum),2) > 0)
    inputData(:,nextInputDataCol) = inputDataRaw(:,22); % gas sensor reading
    nextInputDataCol = nextInputDataCol + 1;
    inputData(:,nextInputDataCol) = inputDataRaw(:,27); % localization x
    nextInputDataCol = nextInputDataCol + 1;
    inputData(:,nextInputDataCol) = inputDataRaw(:,28); % localization y
    nextInputDataCol = nextInputDataCol + 1;
end
% Sensor 5 <-> TGS 2602, Nose-A
nextSensorNum = nextSensorNum + 1;
if (size(find(sensorsUsed == nextSensorNum),2) > 0)
    inputData(:,nextInputDataCol) = inputDataRaw(:,23); % gas sensor reading
    nextInputDataCol = nextInputDataCol + 1;
    inputData(:,nextInputDataCol) = inputDataRaw(:,27); % localization x
    nextInputDataCol = nextInputDataCol + 1;
    inputData(:,nextInputDataCol) = inputDataRaw(:,28); % localization y
    nextInputDataCol = nextInputDataCol + 1;
end
% Sensor 6 <-> TGS 2600_2, Nose-A
nextSensorNum = nextSensorNum + 1;
if (size(find(sensorsUsed == nextSensorNum),2) > 0)
    inputData(:,nextInputDataCol) = inputDataRaw(:,24); % gas sensor reading
    nextInputDataCol = nextInputDataCol + 1;
    inputData(:,nextInputDataCol) = inputDataRaw(:,27); % localization x
    nextInputDataCol = nextInputDataCol + 1;
    inputData(:,nextInputDataCol) = inputDataRaw(:,28); % localization y
    nextInputDataCol = nextInputDataCol + 1;
end
%--------
% Nose-B
%--------
% Sensor 7 <-> TGS 2620_1, Nose-B
nextSensorNum = nextSensorNum + 1;
if (size(find(sensorsUsed == nextSensorNum),2) > 0)
    inputData(:,nextInputDataCol) = inputDataRaw(:,30); % gas sensor reading
    nextInputDataCol = nextInputDataCol + 1;
    inputData(:,nextInputDataCol) = inputDataRaw(:,38); % localization x
    nextInputDataCol = nextInputDataCol + 1;
    inputData(:,nextInputDataCol) = inputDataRaw(:,39); % localization y
    nextInputDataCol = nextInputDataCol + 1;
end
% Sensor 8 <-> TGS 2620_2, Nose-B
nextSensorNum = nextSensorNum + 1;
if (size(find(sensorsUsed == nextSensorNum),2) > 0)
    inputData(:,nextInputDataCol) = inputDataRaw(:,31); % gas sensor reading
    nextInputDataCol = nextInputDataCol + 1;
    inputData(:,nextInputDataCol) = inputDataRaw(:,38); % localization x
    nextInputDataCol = nextInputDataCol + 1;
    inputData(:,nextInputDataCol) = inputDataRaw(:,39); % localization y
    nextInputDataCol = nextInputDataCol + 1;
end
% Sensor 9 <-> TGS 2620_3, Nose-B
nextSensorNum = nextSensorNum + 1;
if (size(find(sensorsUsed == nextSensorNum),2) > 0)
    inputData(:,nextInputDataCol) = inputDataRaw(:,32); % gas sensor reading
    nextInputDataCol = nextInputDataCol + 1;
    inputData(:,nextInputDataCol) = inputDataRaw(:,38); % localization x
    nextInputDataCol = nextInputDataCol + 1;
    inputData(:,nextInputDataCol) = inputDataRaw(:,39); % localization y
    nextInputDataCol = nextInputDataCol + 1;
end
% Sensor 10 <-> TGS 2620_4, Nose-B
nextSensorNum = nextSensorNum + 1;
if (size(find(sensorsUsed == nextSensorNum),2) > 0)
    inputData(:,nextInputDataCol) = inputDataRaw(:,33); % gas sensor reading
    nextInputDataCol = nextInputDataCol + 1;
    inputData(:,nextInputDataCol) = inputDataRaw(:,38); % localization x
    nextInputDataCol = nextInputDataCol + 1;
    inputData(:,nextInputDataCol) = inputDataRaw(:,39); % localization y
    nextInputDataCol = nextInputDataCol + 1;
end
% Sensor 11 <-> currently unused, Nose-B
nextSensorNum = nextSensorNum + 1;
if (size(find(sensorsUsed == nextSensorNum),2) > 0)
    inputData(:,nextInputDataCol) = inputDataRaw(:,34); % gas sensor reading
    nextInputDataCol = nextInputDataCol + 1;
    inputData(:,nextInputDataCol) = inputDataRaw(:,38); % localization x
    nextInputDataCol = nextInputDataCol + 1;
    inputData(:,nextInputDataCol) = inputDataRaw(:,39); % localization y
    nextInputDataCol = nextInputDataCol + 1;
end
% Sensor 12 <-> currently unused, Nose-B
nextSensorNum = nextSensorNum + 1;
if (size(find(sensorsUsed == nextSensorNum),2) > 0)
    inputData(:,nextInputDataCol) = inputDataRaw(:,35); % gas sensor reading
    nextInputDataCol = nextInputDataCol + 1;
    inputData(:,nextInputDataCol) = inputDataRaw(:,38); % localization x
    nextInputDataCol = nextInputDataCol + 1;
    inputData(:,nextInputDataCol) = inputDataRaw(:,39); % localization y
    nextInputDataCol = nextInputDataCol + 1;
end
% Sensor 13 <-> currently unused, Nose-B
nextSensorNum = nextSensorNum + 1;
if (size(find(sensorsUsed == nextSensorNum),2) > 0)
    inputData(:,nextInputDataCol) = inputDataRaw(:,36); % gas sensor reading
    nextInputDataCol = nextInputDataCol + 1;
    inputData(:,nextInputDataCol) = inputDataRaw(:,38); % localization x
    nextInputDataCol = nextInputDataCol + 1;
    inputData(:,nextInputDataCol) = inputDataRaw(:,39); % localization y
    nextInputDataCol = nextInputDataCol + 1;
end
% Sensor 14 <-> currently unused, Nose-B
nextSensorNum = nextSensorNum + 1;
if (size(find(sensorsUsed == nextSensorNum),2) > 0)
    inputData(:,nextInputDataCol) = inputDataRaw(:,37); % gas sensor reading
    nextInputDataCol = nextInputDataCol + 1;
    inputData(:,nextInputDataCol) = inputDataRaw(:,38); % localization x
    nextInputDataCol = nextInputDataCol + 1;
    inputData(:,nextInputDataCol) = inputDataRaw(:,39); % localization y
    nextInputDataCol = nextInputDataCol + 1;
end
%--------
% Nose-C
%--------
% Sensor 15 <-> TGS 4161 (CO), Nose-C
nextSensorNum = nextSensorNum + 1;
if (size(find(sensorsUsed == nextSensorNum),2) > 0)
    inputData(:,nextInputDataCol) = inputDataRaw(:,41); % gas sensor reading
    nextInputDataCol = nextInputDataCol + 1;
    inputData(:,nextInputDataCol) = inputDataRaw(:,49); % localization x
    nextInputDataCol = nextInputDataCol + 1;
    inputData(:,nextInputDataCol) = inputDataRaw(:,50); % localization y
    nextInputDataCol = nextInputDataCol + 1;
end
% Sensor 16 <-> TGS 2600_1, Nose-C
nextSensorNum = nextSensorNum + 1;
if (size(find(sensorsUsed == nextSensorNum),2) > 0)
    inputData(:,nextInputDataCol) = inputDataRaw(:,42); % gas sensor reading
    nextInputDataCol = nextInputDataCol + 1;
    inputData(:,nextInputDataCol) = inputDataRaw(:,49); % localization x
    nextInputDataCol = nextInputDataCol + 1;
    inputData(:,nextInputDataCol) = inputDataRaw(:,50); % localization y
    nextInputDataCol = nextInputDataCol + 1;
end
% Sensor 17 <-> TGS 2620, Nose-C
nextSensorNum = nextSensorNum + 1;
if (size(find(sensorsUsed == nextSensorNum),2) > 0)
    inputData(:,nextInputDataCol) = inputDataRaw(:,43); % gas sensor reading
    nextInputDataCol = nextInputDataCol + 1;
    inputData(:,nextInputDataCol) = inputDataRaw(:,49); % localization x
    nextInputDataCol = nextInputDataCol + 1;
    inputData(:,nextInputDataCol) = inputDataRaw(:,50); % localization y
    nextInputDataCol = nextInputDataCol + 1;
end
% Sensor 18 <-> TGS 2611, Nose-C
nextSensorNum = nextSensorNum + 1;
if (size(find(sensorsUsed == nextSensorNum),2) > 0)
    inputData(:,nextInputDataCol) = inputDataRaw(:,44); % gas sensor reading
    nextInputDataCol = nextInputDataCol + 1;
    inputData(:,nextInputDataCol) = inputDataRaw(:,49); % localization x
    nextInputDataCol = nextInputDataCol + 1;
    inputData(:,nextInputDataCol) = inputDataRaw(:,50); % localization y
    nextInputDataCol = nextInputDataCol + 1;
end
% Sensor 19 <-> TGS 2602, Nose-C
nextSensorNum = nextSensorNum + 1;
if (size(find(sensorsUsed == nextSensorNum),2) > 0)
    inputData(:,nextInputDataCol) = inputDataRaw(:,45); % gas sensor reading
    nextInputDataCol = nextInputDataCol + 1;
    inputData(:,nextInputDataCol) = inputDataRaw(:,49); % localization x
    nextInputDataCol = nextInputDataCol + 1;
    inputData(:,nextInputDataCol) = inputDataRaw(:,50); % localization y
    nextInputDataCol = nextInputDataCol + 1;
end
% Sensor 20 <-> TGS 2600_2, Nose-C
nextSensorNum = nextSensorNum + 1;
if (size(find(sensorsUsed == nextSensorNum),2) > 0)
    inputData(:,nextInputDataCol) = inputDataRaw(:,46); % gas sensor reading
    nextInputDataCol = nextInputDataCol + 1;
    inputData(:,nextInputDataCol) = inputDataRaw(:,49); % localization x
    nextInputDataCol = nextInputDataCol + 1;
    inputData(:,nextInputDataCol) = inputDataRaw(:,50); % localization y
    nextInputDataCol = nextInputDataCol + 1;
end

% (3.3) Add pseudo sensors for temperature, humidity and wind 
% "Sensor" 21 <-> wind-U
nextSensorNum = nextSensorNum + 1;
if (size(find(sensorsUsed == nextSensorNum),2) > 0)
    inputData(:,nextInputDataCol) = inputDataRaw(:,8);  % wind-U
    nextInputDataCol = nextInputDataCol + 1;
    inputData(:,nextInputDataCol) = inputDataRaw(:,15); % localization x
    nextInputDataCol = nextInputDataCol + 1;
    inputData(:,nextInputDataCol) = inputDataRaw(:,16); % localization y
    nextInputDataCol = nextInputDataCol + 1;
end
% "Sensor" 22 <-> wind-V
nextSensorNum = nextSensorNum + 1;
if (size(find(sensorsUsed == nextSensorNum),2) > 0)
    inputData(:,nextInputDataCol) = inputDataRaw(:,9);  % wind-V
    nextInputDataCol = nextInputDataCol + 1;
    inputData(:,nextInputDataCol) = inputDataRaw(:,15); % localization x
    nextInputDataCol = nextInputDataCol + 1;
    inputData(:,nextInputDataCol) = inputDataRaw(:,16); % localization y
    nextInputDataCol = nextInputDataCol + 1;
end
% "Sensor" 23 <-> wind-W
nextSensorNum = nextSensorNum + 1;
if (size(find(sensorsUsed == nextSensorNum),2) > 0)
    inputData(:,nextInputDataCol) = inputDataRaw(:,10); % wind-W
    nextInputDataCol = nextInputDataCol + 1;
    inputData(:,nextInputDataCol) = inputDataRaw(:,15); % localization x
    nextInputDataCol = nextInputDataCol + 1;
    inputData(:,nextInputDataCol) = inputDataRaw(:,16); % localization y
    nextInputDataCol = nextInputDataCol + 1;
end
% "Sensor" 24 <-> wind-UV
nextSensorNum = nextSensorNum + 1;
if (size(find(sensorsUsed == nextSensorNum),2) > 0)
    inputData(:,nextInputDataCol) = inputDataRaw(:,11); % wind-W
    nextInputDataCol = nextInputDataCol + 1;
    inputData(:,nextInputDataCol) = inputDataRaw(:,15); % localization x
    nextInputDataCol = nextInputDataCol + 1;
    inputData(:,nextInputDataCol) = inputDataRaw(:,16); % localization y
    nextInputDataCol = nextInputDataCol + 1;
end
% "Sensor" 25 <-> wind-UVW
nextSensorNum = nextSensorNum + 1;
if (size(find(sensorsUsed == nextSensorNum),2) > 0)
    inputData(:,nextInputDataCol) = sqrt( inputDataRaw(:,10).^2 + inputDataRaw(:,10).^2 + inputDataRaw(:,10).^2); % sqrt(windU^2+windV^2+windW^2)
    nextInputDataCol = nextInputDataCol + 1;
    inputData(:,nextInputDataCol) = inputDataRaw(:,15); % localization x
    nextInputDataCol = nextInputDataCol + 1;
    inputData(:,nextInputDataCol) = inputDataRaw(:,16); % localization y
    nextInputDataCol = nextInputDataCol + 1;
end
% "Sensor" 26 <-> Temperature (Anemometer)
nextSensorNum = nextSensorNum + 1;
if (size(find(sensorsUsed == nextSensorNum),2) > 0)
    inputData(:,nextInputDataCol) = inputDataRaw(:,13); % tempAn
    nextInputDataCol = nextInputDataCol + 1;
    inputData(:,nextInputDataCol) = inputDataRaw(:,15); % localization x
    nextInputDataCol = nextInputDataCol + 1;
    inputData(:,nextInputDataCol) = inputDataRaw(:,16); % localization y
    nextInputDataCol = nextInputDataCol + 1;
end
% "Sensor" 27 <-> Temperature (Nose-A)
nextSensorNum = nextSensorNum + 1;
if (size(find(sensorsUsed == nextSensorNum),2) > 0)
    inputData(:,nextInputDataCol) = inputDataRaw(:,25); % tempA
    nextInputDataCol = nextInputDataCol + 1;
    inputData(:,nextInputDataCol) = inputDataRaw(:,27); % localization x
    nextInputDataCol = nextInputDataCol + 1;
    inputData(:,nextInputDataCol) = inputDataRaw(:,28); % localization y
    nextInputDataCol = nextInputDataCol + 1;
end
% "Sensor" 28 <-> Humidity (Nose-A)
nextSensorNum = nextSensorNum + 1;
if (size(find(sensorsUsed == nextSensorNum),2) > 0)
    inputData(:,nextInputDataCol) = inputDataRaw(:,26); % humA
    nextInputDataCol = nextInputDataCol + 1;
    inputData(:,nextInputDataCol) = inputDataRaw(:,27); % localization x
    nextInputDataCol = nextInputDataCol + 1;
    inputData(:,nextInputDataCol) = inputDataRaw(:,28); % localization y
    nextInputDataCol = nextInputDataCol + 1;
end
% "Sensor" 29 <-> Temperature (Nose-C)
nextSensorNum = nextSensorNum + 1;
if (size(find(sensorsUsed == nextSensorNum),2) > 0)
    inputData(:,nextInputDataCol) = inputDataRaw(:,47); % tempC
    nextInputDataCol = nextInputDataCol + 1;
    inputData(:,nextInputDataCol) = inputDataRaw(:,49); % localization x
    nextInputDataCol = nextInputDataCol + 1;
    inputData(:,nextInputDataCol) = inputDataRaw(:,50); % localization y
    nextInputDataCol = nextInputDataCol + 1;
end
% "Sensor" 30 <-> Humidity (Nose-C)
nextSensorNum = nextSensorNum + 1;
if (size(find(sensorsUsed == nextSensorNum),2) > 0)
    inputData(:,nextInputDataCol) = inputDataRaw(:,47); % humC
    nextInputDataCol = nextInputDataCol + 1;
    inputData(:,nextInputDataCol) = inputDataRaw(:,49); % localization x
    nextInputDataCol = nextInputDataCol + 1;
    inputData(:,nextInputDataCol) = inputDataRaw(:,50); % localization y
    nextInputDataCol = nextInputDataCol + 1;
end

% (3.4) Add status data
inputData(:,nextInputDataCol) = inputDataRaw(:,14);  % error check (anemometer)
nextInputDataCol = nextInputDataCol + 1;
inputData(:,nextInputDataCol) = inputDataRaw(:,17);  % robot status
%nextInputDataCol = nextInputDataCol + 1;

% % (3.5) Add Wind Speed and Direction
% inputData(:,nextInputDataCol) = inputDataRaw(:,11);  % 2D Wind Speed
% nextInputDataCol = nextInputDataCol + 1;
% inputData(:,nextInputDataCol) = inputDataRaw(:,12);  % 2D Wind orientation


end

clear all;
clc;
clear all;
experimentLabel = 'corridor';

resultFolder = 'result_temporal_subsampling';
bNormalizeTime = false;
numberOfScales = 3;
bEpsilon = false;

% initialize variables
gdmType = '2D';
inputFolder = 'data';

experimentConfigFile = 'experiment_config.csv';
gdmConfigFile = 'gdm_parameter_config.csv';

% set optimization function
strOptTarget = 'NLPD'; % [NLPD, avErr] optimise for min NLPD

% set kernel constants
weightVarianceSigmaFactor = 1.0;
maxKernelEvalRadiusFactor = 4.0;

% set flags for conditional operations such as plotting, saving logs, etc.
bVisualizeData = true;
bNormalizeData = true;

bVerbose = false;
bSaveLog = true;
bSaveVisualization = true;

% set epsilon flag and epsilon itself
epsilon = 0.0000001;

% select experiment corresponding to experimentLabel
[experimentType, fileName, startInd, endInd, ...
    mapCenter, mapSize, sourceLocation, gasType] = selectExperiment(experimentLabel);

% extract inputData considering the experimentLabel, and the cutting
% indices.
startTime = 1;
endTime = 100;
inputData = extractInputData(inputFolder, experimentType, ...
    experimentLabel, fileName, startTime, endTime, startInd, endInd, ...
    bNormalizeData, bNormalizeTime, bVisualizeData);

% select partitioning cuts
strTestSetSelectionId = 'contPartTestSetSelection'; % partitions the data into parsTestSetSelection(2) contiguous sets and selects the parsTestSetSelection(1)th of these sets as test set
parsTestSetSelection = [5 5 0 0]; % %(1)th of %(2) parts with indices from %(3) to %(4)
strTvSetSelectionId = 'contTvSetSelectionFixed'; % partitions the data into parsTvSetSelection(2) contiguous sets and selects the parsTestSetSelection(1)th of these sets as validation set, the rest is used for training
parsTvSetSelection = [4 4]; % %(1)th of %(2) parts is the validation set
numOfSamples = size(inputData, 1);
[trainIndexSet, valIndexSet, tvIndexSet, testIndexSet] = ...
    selectDataPartition(numOfSamples, strTestSetSelectionId, parsTestSetSelection, ...
    strTvSetSelectionId, parsTvSetSelection, bVisualizeData);

targetTimeSet = (unique(inputData(testIndexSet, 1)))';

[minKernelWidth, maxKernelWidth, minCellSize, maxCellSize, ...
    minTimeScale, maxTimeScale, ...
    stepKernelWidth, stepCellSize, stepTimeScale] = selectMetaParametersIntervals(experimentLabel);


close all;

optNLPD = 10000;
optKernelWidth = 0; optCellSize = 0; optTimeScale = 0;
evaluationSet = [];

% list of targetTimes for which we want to evaluate.
targetTime = max(inputData(testIndexSet,1));

subsetSize = round(size(tvIndexSet,2)/numberOfScales);
s = RandStream('mt19937ar','Seed',0);
part = parsTvSetSelection(1,1);


s = RandStream('mt19937ar','Seed',0);
result = [];

for itr = 1:20
    %for i=1:1:numberOfScales

       %testSubsetSize = round(size(testIndexSet,2) * 1);
       testIndexSubSet = testIndexSet; %(randperm(s, size(testIndexSet,2), testSubsetSize));

%        tvIndexSubSet = [((i-1)*subsetSize + 1):(i*subsetSize)];
%        %fprintf('%i : [%i , %i]\n', i, tvIndexSubSet(1,1), tvIndexSubSet(1,end));
%        part = parsTvSetSelection(1,1);
%        partitionSize = round(size(tvIndexSubSet,2)/part);
%        trainIndexSubSet = tvIndexSubSet(1, 1:partitionSize * (part -1));
%        valIndexSubSet = tvIndexSubSet(1, partitionSize * (part -1)+1:end);

       tvIndexSubSet = tvIndexSet(randperm(s, size(tvIndexSet,2), subsetSize));
       partitionSize = round(size(tvIndexSubSet,2)/part);

       tvIndexSubSet = sort(tvIndexSubSet);
       trainIndexSubSet = tvIndexSubSet(1, 1:partitionSize * (part -1)); 
       valIndexSubSet = tvIndexSubSet(1, partitionSize * (part -1)+1:end);

       kernelWidth = 0.20; cellSize = 0.10; timeScale = 0;
        i = 1;
%        if i == 1
%             cellSize = 0.10;
%             kernelWidth = 0.50;
%         elseif i == 2
%             cellSize = 0.10;
%             kernelWidth = 0.45;
%         else
%             cellSize = 0.10;
%             kernelWidth = 0.45;
%        end

        mapCellSize = [cellSize, cellSize];
        [numOfMapCells, mapCenter, mapCellSize, mapLowerLeft] = getMapGeometry(mapCenter, mapSize, mapCellSize);
        mapGeometry = [numOfMapCells, mapCenter, mapCellSize, mapLowerLeft];

        maxKernelEvalRadius = maxKernelEvalRadiusFactor * kernelWidth;
        weightVarianceSigma = weightVarianceSigmaFactor * 1 / ( sqrt(2.0 * pi) * kernelWidth );
        weightVarianceSteepness = weightVarianceSigma * weightVarianceSigma;

        % evaluate the gdm model over the test set
        [testGdmMap, kGdmWeightMin, kGdmWeightMax, kGdmMapMin, kGdmMapMax, totalMean, ...
            kGdmVarMin, kGdmVarMax, sigma2Tot] = ...
            createGDM(inputData, tvIndexSubSet, ...
                mapGeometry, ...
                kernelWidth, maxKernelEvalRadius, weightVarianceSteepness, ...
                timeScale, targetTime, ...
                bVerbose);

        [ testAvgErr, numOfPredictionsMade, ...
            testNLPD, numOfPointsForLikelihoodEval] = evaluateGDM(testGdmMap, ...
            mapGeometry, inputData, testIndexSubSet, bEpsilon, epsilon);
        fprintf('%s,%s,%s,%.2f,%.2f,%.4f,%.4f\n', ...
            num2str(itr), num2str(numberOfScales), num2str(i), ...
            cellSize, kernelWidth, ...
            testNLPD, testAvgErr);
        result = [result; itr, numberOfScales, i, ...
            cellSize, kernelWidth, ...
            testNLPD, testAvgErr];


    %    logFile = sprintf('log_optimization_subsampling_scale-%i_partition-%i', numberOfScales, i);
    %    save([resultFilePath '/' logFile '.mat'], 'optimizationSet');
    %    clear optimizationSet;

    %end    
end

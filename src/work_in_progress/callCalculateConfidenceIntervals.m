close all
clear all
clc

experimentLabel = 'corridor';
experimentType = 'real';

%GDM type '2D' refers to suing Kernel DM+V. For TD Kernel DM+V, one can use '2DTD'. 
%For subsampling we mainly use '2D'.
gdmType = '2D';

%for temopral subsampling: 2 refers to using 50% and 3 refers to using 33%
%of samples.
scaleSet = [2,3]; 

%number of times we want to run the optimization.
numberOfIterations = 10;

%prefix to select path for input and output
pathPrefix = 'result_temporal_subsampling';
inputPath = [pathPrefix '/' experimentType '/' experimentLabel '/' ...
    '2D'];

x = [];
for numberOfScales=2:3
    for itr = 1:numberOfIterations
        inputFile = ['log_optimization_subsampling_scale-' ...
            num2str(numberOfScales) '_itr-' num2str(itr) '.mat'];
        inputData = load([inputPath '/' 'scale_' num2str(numberOfScales) ... 
            '_with-all_epsilon_false_all_rand' '/' ...
            inputFile]);
        dataSource = inputData.optimizationSet;
        
        if strcmp(gdmType, '2DTD')
            dataSource = dataSource(dataSource(:,3) > 0.0000000000001, : );
        else
            dataSource = dataSource(dataSource(:,3) < 0.0000000000001, : );
        end
        
        %to avoid very small kernel widths compared to cell sizes
        dataSource = dataSource(dataSource(:,1) - dataSource(:,2) > 0.151 , :);

        %[values, order] = sort(dataSource(:,4));
        %optSet = dataSource(order, :);
        optSet = dataSource;
        
        % optSet
        % kernelWidth, cellSize, timeScale, trainNLPD, trainAvgErr, testNLPD, testAvgErr
        indSet = 1:size(optSet, 1);
        kernelWidthSet = optSet(indSet, 1);
        cellSizeSet  = optSet(indSet, 2);
        timeScaleSet = optSet(indSet, 3);
        trainNLPD    = optSet(indSet, 4);
        trainAvgErr  = optSet(indSet, 5);
        testNLPD     = optSet(indSet, 6);
        testAvgErr   = optSet(indSet, 7);
        
        [minValTest, minIdxTest] = min(testNLPD);
        [minValTrain, minIdxTrain] = min(trainNLPD);
        
        x = [x testNLPD(minIdxTrain)];      
    end
       
    SEM = std(x)/sqrt(length(x));               % Standard Error
    ts = tinv([0.025  0.975],length(x)-1);      % T-Score
    CI = mean(x) + ts*SEM;                      % Confidence Intervals
    fprintf('%s, %s, Scale = %.2f, Num Iterations: %.2f\n', ...
        experimentLabel, gdmType, numberOfScales, numberOfIterations);
    fprintf('%.4f with 95 CI = [%.4f,%.4f]\n', ...
        mean(x), CI(1,1), CI(1,2)) ;   
end


clc
clear all
close all

experimentLabel = 'smallnet';
numberOfScales = 2;
numberOfIterations = 10;
gdmType = '2D';

res = load(['output/CI/' experimentLabel '_' num2str(numberOfScales) '_80.mat']);
input = res.result;

for i = 1:numberOfScales
    x = input(input(:,3) == i , 6);
    x = x + 0.15;
    SEM = std(x)/sqrt(length(x));               % Standard Error
    ts = tinv([0.025  0.975],length(x)-1);      % T-Score
    CI = mean(x) + ts*SEM;                      % Confidence Intervals
    fprintf('%s, %s, Scale = %s, folder, %s, Num Iterations: %s\n', ...
        experimentLabel, gdmType, num2str(numberOfScales), num2str(i), num2str(numberOfIterations));
    fprintf('%.4f with 95 CI = [%.4f,%.4f]\n', ...
        mean(x), CI(1,1), CI(1,2)) ;   
end

close all;
clc;
clear all;

% set the start time - this is used to create a folder in the result path
startTime = clock;
strStartTimeId = [sprintf('%i',startTime(1)) '_' ...
    sprintf('%02i',startTime(2)) '_' sprintf('%02i',startTime(3)) '-' ...
    sprintf('%02i',startTime(4)) '_' sprintf('%02i',startTime(5)) '_' sprintf('%02.0f',startTime(6))];

% initialize variables
gdmTypeSet = ['2D', '2DTD'];
gdmType = '2D';

inputFolder = 'data';
resultFolder = 'result';
experimentLabel = 'corridor';

% set optimization function
metric = 'NLPD'; % [NLPD, avErr] optimise for min NLPD

% set kernel constants
weightVarianceSigmaFactor = 1.0;
maxKernelEvalRadiusFactor = 4.0;

% set flags for conditional operations such as plotting, saving logs, etc.
bVisualizeData = true;
bNormalizeData = true;
bNormalizeTime = false;

bVerbose = false;
bSaveLog = true;
bSaveVisualization = true;

% set epsilon flag and epsilon itsel
bEpsilon = false;
epsilon = 0.0000001;

% select experiment corresponding to experimentLabel
[experimentType, fileName, startInd, endInd, ...
    mapCenter, mapSize, sourceLocation, gasType] = selectExperiment(experimentLabel);

% extract inputData considering the experimentLabel, and the cutting
% indices.
startTime = 1;
endTime = 100;
inputData = extractInputData(inputFolder, experimentType, ...
    experimentLabel, fileName, startTime, endTime, startInd, endInd, ...
    bNormalizeData, bNormalizeTime, bVisualizeData);

% select partitioning cuts
strTestSetSelectionId = 'contPartTestSetSelection'; % partitions the data into parsTestSetSelection(2) contiguous sets and selects the parsTestSetSelection(1)th of these sets as test set
parsTestSetSelection = [5 5 0 0]; % %(1)th of %(2) parts with indices from %(3) to %(4)
strTvSetSelectionId = 'contTvSetSelectionFixed'; % partitions the data into parsTvSetSelection(2) contiguous sets and selects the parsTestSetSelection(1)th of these sets as validation set, the rest is used for training
parsTvSetSelection = [4 4]; % %(1)th of %(2) parts is the validation set
numOfSamples = size(inputData, 1);
[trainIndexSet, valIndexSet, tvIndexSet, testIndexSet] = ...
    selectDataPartition(numOfSamples, strTestSetSelectionId, parsTestSetSelection, ...
    strTvSetSelectionId, parsTvSetSelection, bVisualizeData);

% tvIndexSet = (find(inputData(:,1) < 19))';
% testIndexSet = (find(inputData(:,1) >= 19))';

targetTimeSet = (unique(inputData(testIndexSet, 1)))';

[kernelWidth, cellSize, timeScale] = selectMetaParameters(experimentLabel);

if strcmp(gdmType, '2D')
    timeScale = 0.0;
end

% list of targetTimes for which we want to evaluate.
targetTimeList = [max(targetTimeSet)];% sum(targetTimeSet)/size(targetTimeSet,2) min(targetTimeSet)];
result = [];

%for targetTime=targetTimeList
targetTime = 100; %targetTimeList(1,1);
for i =1:10
    mapCellSize = [cellSize, cellSize];
    [numOfMapCells, mapCenter, mapCellSize, mapLowerLeft] = getMapGeometry(mapCenter, mapSize, mapCellSize);
    mapGeometry = [numOfMapCells, mapCenter, mapCellSize, mapLowerLeft];

    maxKernelEvalRadius = maxKernelEvalRadiusFactor * kernelWidth;
    weightVarianceSigma = weightVarianceSigmaFactor * 1 / ( sqrt(2.0 * pi) * kernelWidth );
    weightVarianceSteepness = weightVarianceSigma * weightVarianceSigma;
    
    % create a map for the given meta-parameters and targetTime
    [kGdmvMap, kGdmWeightMin, kGdmWeightMax, kGdmMapMin, kGdmMapMax, totalMean, ...
        kGdmVarMin, kGdmVarMax, sigma2Tot] = createGDM(inputData, tvIndexSet, ...
        mapGeometry, ...
        kernelWidth, maxKernelEvalRadius, weightVarianceSteepness, ...
        timeScale, targetTime, ...
        bVerbose);
    
    % evaluate the gdm model over the test set
    [ avPredictionError, numOfPredictionsMade, ...
        NLPD, numOfPointsForLikelihoodEval] = evaluateGDM(kGdmvMap, ...
        mapGeometry, inputData, testIndexSubSet, bEpsilon, epsilon);
    fprintf('(c = %.2f, sigma = %.2f, beta = %.5f) - targetTime = %i ==> NLPD = %.4f, err = %.4f\n', ...
        cellSize, kernelWidth, timeScale, targetTime, NLPD, avPredictionError);
    
    % store the results in a matrix
    result = [result; targetTime NLPD avPredictionError];
end

resultFilePath = [resultFolder '/' experimentType '/' experimentLabel '/' gdmType '/' strStartTimeId];

if ~exist(resultFilePath, 'dir')
    mkdir(resultFilePath);
end

% set the ending time for logging purpose
endTime = clock;
strEndTimeId = [sprintf('%i',endTime(1)) '_' ...
    sprintf('%02i',endTime(2)) '_' sprintf('%02i',endTime(3)) '-' ...
    sprintf('%02i',endTime(4)) '_' sprintf('%02i',endTime(5)) '_' sprintf('%02.0f',endTime(6))];

if bVisualizeData && bSaveVisualization
    count = 0;
    hfigs = get(0, 'children');
    for m=1:1:length(hfigs)
       if strcmp(hfigs(m).Name, 'Optimization Plot')
           saveas(hfigs(m), [ resultFilePath '/nlpd_plot.fig']);
           print(hfigs(m), '-dpng', '-r96', ...
        [ resultFilePath '/nlpd_plot.png']);
        count = count + 1;
       end
       if strcmp(hfigs(m).Name, 'Data Partitioning')
           saveas(hfigs(m), [ resultFilePath '/paritions_plot.fig']);
           print(hfigs(m), '-dpng', '-r96', ...
        [ resultFilePath '/paritions_plot.png']);
        count = count + 1;
       end
       if strcmp(hfigs(m).Name, 'Visualize Input Data')
           saveas(hfigs(m), [ resultFilePath '/data_plot.fig']);
           print(hfigs(m), '-dpng', '-r96', ...
        [ resultFilePath '/data_plot.png']);
        count = count + 1;
       end
       
       if (count > 2)
           break
       end
    end
end

% save the logs in a file
if bSaveLog
    saveLogs(result, resultFilePath, inputFolder, resultFolder, ...
        strStartTimeId, strEndTimeId, metric, ...
        trainIndexSet, valIndexSet, testIndexSet, ...
        gdmType, experimentType, experimentLabel, gasType, sourceLocation, ...
        mapCenter, mapSize, mapCellSize, ...
        kernelWidth, cellSize, timeScale, ...
        strTestSetSelectionId, parsTestSetSelection, strTvSetSelectionId, parsTvSetSelection, ...
        bNormalizeData, bSaveVisualization, bEpsilon, epsilon);
end

function saveGdwmAsPng(kGdmMap, kGdmWeightMin, kGdmWeightMax, ...
    filename, ...
    maxRange, maxRangeColorMin, maxRangeColorMax, unusedCellsColor)

[mapCellsX, mapCellsY, dummy] = size(kGdmMap);

imgData = zeros(mapCellsY,mapCellsX,3);
%alphaMask = zeros(mapCellsY, mapCellsX,1);

%kGdwmMapMin = min(min(abs(kGdmMap(:,:,3))));
%kGdwmMapMax = max(max(kGdmMap(:,:,3)));

fprintf('Create weight map image in matrix ... ');
for iX = 1:1:mapCellsX
    for iY = 1:1:mapCellsY
        currNormVal = (kGdmMap(iX,iY,3) - kGdmWeightMin) / (kGdmWeightMax - kGdmWeightMin);
        if currNormVal >= 0
            if currNormVal < 1 - maxRange % map to black->white for values outside the peak areas
                imgData(mapCellsY-iY+1,iX,1) = currNormVal;
                imgData(mapCellsY-iY+1,iX,2) = currNormVal;
                imgData(mapCellsY-iY+1,iX,3) = currNormVal;
            else                          % map to black->selectedColor for values outside the peak areas
                imgData(mapCellsY-iY+1,iX,:) = (1 - (1 - currNormVal) / maxRange) * (maxRangeColorMax - maxRangeColorMin) + maxRangeColorMin;
            end
        else                              % special colour for cells for which no prediction is made
            imgData(mapCellsY-iY+1,iX,:) = unusedCellsColor;
        end            
    end
end
fprintf('Done!\n');

fprintf('Save weight map image ... ');
imwrite( imgData, filename, 'PNG' , 'BitDepth', 8);
%imwrite( imgData, '_images/TEST.png', 'PNG' , 'Alpha', alphaMask);
fprintf('Done!\n');

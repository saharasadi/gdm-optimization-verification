function [maxRange, maxRangeColorMin, maxRangeColorMax, unusedCellsColor] = ...
    selectLegacyVizualisationSettings()

maxRange = 0.1;
maxRangeColorMin = [0.0,0.1,0.1];
maxRangeColorMax = [1.0,0.0,0.0];
unusedCellsColor = [0.0,3/8,0.0];

end
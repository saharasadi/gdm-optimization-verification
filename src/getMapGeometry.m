function [ numOfMapCells, mapCenter, mapCellSize, mapLowerLeft ] = ...
    getMapGeometry(mapCenter, mapSize, mapCellSize)

numOfMapCells = mapSize ./ mapCellSize;
numOfMapCells = sign(numOfMapCells) .* ceil(abs(numOfMapCells));
mapLowerLeft = mapCenter - mapSize / 2;

end
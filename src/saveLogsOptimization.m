%% Description: function saveLogsOptimization
%
% Called by "mainOptimize.m"
% This code, saves the logs and the output dataset from the optimization.%
%==========================================================================
function [ ] = saveLogsOptimization( optimizationSet, ...
    logFolder, inputFilePath, resultFolder, ...
    strStartTimeId, strEndTimeId, metric, ...
    trainIndexSet, valIndexSet, testIndexSet, ...
    strTestSetSelectionId, parsTestSetSelection, strTvSetSelectionId, parsTvSetSelection, ...
    gdmType, experimentType, experimentLabel, fileName, gasType, sourceLocation, ...
    mapCenter, mapSize, ...
    optKernelWidth, optCellSize, optTimeScale, ...
    minKernelWidth, minCellSize, minTimeScale, ...
    maxKernelWidth, maxCellSize, maxTimeScale, ...
    stepKernelWidth, stepCellSize, stepTimeScale, ...
    targetTime, ...
    bNormalizeData, bSaveVisualization, scale, itr)

% THIS FUNCTION PROVIDES LOGS ABOUT THE PERFORMED EXPERIMENT
   
% log: time
content = sprintf('\n== TIME ===========================\n');

content = [content 'start time :: ' strStartTimeId sprintf('\n')];
content = [content 'end time :: ' strEndTimeId sprintf('\n')];

% log: configs
content = [content sprintf('\n== CONFIG ===========================\n')];
content = [content 'metric : ' metric sprintf('\n\n')];

% log: input output path
content = [content 'Input Data Path : ' inputFilePath sprintf('\n')];
content = [content 'Result Folder Path : ' resultFolder sprintf('\n\n')];

% log: flags
content = [content sprintf('\n== FLAGS ===========================\n')];
content = [content 'Normalize Data : ' sprintf('%i\n', bNormalizeData)];
content = [content 'Save Visualization : ' sprintf('%i\n', bSaveVisualization)];

% log: experiment type
content = [content sprintf('\n== EXPERIMENT ===========================\n')];
content = [content 'GDM Type : ' gdmType sprintf('\n\n')];
content = [content 'Experiment:' experimentType ' - ' experimentLabel sprintf('\n')];
content = [content 'Data File:' fileName sprintf('\n\n')];

% log : Geometry
content = [content 'map center =  ' sprintf('(%.3f',mapCenter(1,1)) ' x ' sprintf('%.3f',mapCenter(1,2)) sprintf(') m^2\n')];
content = [content 'map size   =  ' sprintf('(%.3f',mapSize(1,1))   ' x ' sprintf('%.3f',mapSize(1,2)) sprintf(') m^2\n')];
content = [content 'source location = ' sprintf('[%.2f, %.2f, %.2f]', sourceLocation(1), sourceLocation(2), sourceLocation(3)) sprintf(') m\n')];
content = [content 'gas type : ' gasType sprintf('\n\n')];

% log: data partitioning
content = [content '== DATA PARTITION ===========================' sprintf('\n')];

content = [content 'parse test set selection: ' strTestSetSelectionId ' - ' ...
    sprintf('[%i %i %i %i]\n', parsTestSetSelection(1,1), parsTestSetSelection(1,2), ...
    parsTestSetSelection(1,2), parsTestSetSelection(1,4))];
content = [content 'parse tv set selection:   ' strTvSetSelectionId ' - ' ...
    sprintf('[%i %i]\n\n', parsTvSetSelection(1,1), parsTvSetSelection(1,2))];

content = [content 'training data index : ' sprintf('[%i:%i]', ...
   trainIndexSet(1,1), trainIndexSet(1,size(trainIndexSet,2))) sprintf('\n')];
content = [content 'validation data index : ' sprintf('[%i:%i]', ...
   valIndexSet(1,1), valIndexSet(1,size(valIndexSet,2))) sprintf('\n')];
content = [content 'test data index : ' ...
   sprintf('[%i:%i]', testIndexSet(1,1), testIndexSet(1, size(testIndexSet,2))) sprintf('\n\n')];

content = [content sprintf('\n== Intervals : GDM META-PARAMETERS =====================\n')];
content = [content sprintf('sigma: [%.2f, %.2f], step : %.2f)', ...
    minKernelWidth, maxKernelWidth, stepKernelWidth) sprintf('\n')]; 
content = [content sprintf('c    : [%.2f, %.2f], step : %.2f)', ...
    minCellSize, maxCellSize, stepCellSize) sprintf('\n')]; 
content = [content sprintf('beta : [%.2f, %.2f], step : %.2f)', ...
    minTimeScale, maxTimeScale, stepTimeScale) sprintf('\n')]; 

content = [content sprintf('\n== Optimized GDM META-PARAMETERS =====================\n')];
content = [content sprintf('Target Time: %i --> (c = %.2f, sigma = %.2f, beta = %.5f)', ...
   targetTime, optCellSize, optKernelWidth, optTimeScale) sprintf('\n')]; 

content = [content sprintf('\n== END =====================================\n')];

%% SAVE IN THE FILE =======================================================
if (scale <2)
    logFile = sprintf('log_optimization_description_target_time_%i', ...
        targetTime);
else     
    logFile = sprintf('log_optimization_description_target_time_%i_scale_%i_itr_%i', ...
        targetTime, scale, itr);
end

save([logFolder '/' logFile '.mat'], 'optimizationSet');
logFileHandler = fopen([logFolder '/' logFile '.txt'],'w');
fprintf(logFileHandler, '%s\n', content);
fclose(logFileHandler);

end

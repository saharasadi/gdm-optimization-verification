function [inputData] = extractInputDataFromSmallNet(dataFilePath, fileName)

numOfSensorsUsed = 10;

% (1) Load input data from file
input = load([dataFilePath '/' fileName '.log']);
startIdx = 33;
endIdx = 42;
inputDataRaw = [input(:,1) input(:,startIdx:endIdx)];

sensors = load([dataFilePath '/' 'stationary_sensor_pos.txt']);

numOfDataPoints = size(inputDataRaw,1);

% (3) Assign columns: raw data structure -> internal data structure
inputData = []; %zeros(numOfDataPoints * (size(inputDataRaw, 2)-1) , 1+3); 
inputTest = zeros(numOfDataPoints, 3+1); 

% (3.1) Add measurement times (assume synchronous measurements)
inputTest(:,1) = inputDataRaw(:,1)/1000; % time stamp

col =2;
for i=2:1:size(inputDataRaw, 2)
   inputTest(:,col) = inputDataRaw(:,i);
   col = col + 1;
   inputTest(:,col) = sensors(i-1,1);
   col = col + 1;
   inputTest(:,col) = sensors(i-1,2);
   A = [inputTest(:,1) inputTest(:,col-2) inputTest(:, col-1) inputTest(:,col)];
   col = col + 1;
   inputData = [inputData; A];
end

inputData = sortrows(inputData);

end

== TIME ===========================
start time :: 2016_01_10-20_04_28
end time :: 2016_01_10-22_18_50

== CONFIG ===========================
Experiment Config File : experiment_config.csv

GDM Config File : gdm_parameter_config.csv

Input Data Path : data
Result Folder Path : result_optimization_without_time_norm


== FLAGS ===========================
Normalize Data : 1
Save Visualization : 1
Epsilon in Evaluation : 0
== EXPERIMENT ===========================
GDM Type : 2DTD

Experiment:simulation_ros - with_obstacle_two_60x20
Data File:release_rate_fast_ethanol_y10_h_25t_1-200

map center =  (30.000 x 10.000) m^2
map size   =  (58.000 x 20.000) m^2
source location = [10.00, 5.00, 0.10]) m
gas type : ethanol

== DATA PARTITION ===========================
parse test set selection: contPartTestSetSelection - [5 5 5 0]
parse tv set selection:   contTvSetSelectionFixed - [4 4]

training data index : [1:292740]
validation data index : [292741:390320]
test data index : [390321:487900]


== Intervals : GDM META-PARAMETERS =====================
sigma: [0.50, 2.00], step : 0.25)
c    : [0.25, 1.50], step : 0.25)
beta : [0.00, 0.30], step : 0.05)

== Optimized GDM META-PARAMETERS =====================
Target Time: 100 --> (c = 0.25, sigma = 1.50, beta = 0.10000)

== END =====================================


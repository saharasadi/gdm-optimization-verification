
== TIME ===========================
start time :: 2016_01_09-09_11_07
end time :: 2016_01_10-08_45_29

== CONFIG ===========================
Experiment Config File : experiment_config.csv

GDM Config File : gdm_parameter_config.csv

Input Data Path : data
Result Folder Path : result_optimization_without_time_norm


== FLAGS ===========================
Normalize Data : 1
Save Visualization : 1
Epsilon in Evaluation : 0
== EXPERIMENT ===========================
GDM Type : 2DTD

Experiment:simulation_ros - with_obstacle_two_60x20

map center =  (8.000 x 3.500) m^2
map size   =  (57.000 x 20.000) m^2
source location = [10.00, 5.00, 0.10]) m
gas type : ethanol

== DATA PARTITION ===========================
parse test set selection: contPartTestSetSelection - [5 5 5 0]
parse tv set selection:   contTvSetSelectionFixed - [4 4]

training data index : [1:273060]
validation data index : [273061:364080]
test data index : [364081:455100]


== Intervals : GDM META-PARAMETERS =====================
sigma: [0.50, 5.00], step : 0.25)
c    : [0.25, 2.00], step : 0.25)
beta : [0.00, 0.70], step : 0.05)

== Optimized GDM META-PARAMETERS =====================
Target Time: 9.050000e+01 --> (c = 0.50, sigma = 0.75, beta = 0.10000)

== END =====================================


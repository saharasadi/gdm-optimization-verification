%% Description: function mainOptimizeTimeVarying
%
% Called by "callOptimizeTimeVarying.m"
%
% This code, reads experiment setup and the input data for the given
% experimentLabel and for each time stamp in the target time (test set),
% computers the metric (NLPD) on the test set. At the end it prints out the
% NLPD values.
%
% Note I: Here the assumption is the model parameters are not learned again 
%   but set using the optimization in callOptimize. if you want to learn
%   meta-parameters for each target time, then please use
%   "callOptimizeTimeVarying.me"
% 
% input: 
%   experimentLabel: name of the input data file
%   experimentType: the type of the experiment and the folder containing the input data file.
%   bNormalizeTime, bEpsilone: are old constants that haven't been
%       refactored. There are set to false.
%   inputFolder: Path to the input data
%   resultFolder: Path to store the output.
%
%% Implementaiton: mainTimeVarying ========================================

function [] = mainOptimizeTimeVarying(experimentLabel, experimentType, ...
    bEpsilon, bNormalizeTime, resultFolder)

% set the start time - this is used to create a folder in the result path
startTime = clock;
strStartTimeId = [sprintf('%i',startTime(1)) '_' ...
    sprintf('%02i',startTime(2)) '_' sprintf('%02i',startTime(3)) '-' ...
    sprintf('%02i',startTime(4)) '_' sprintf('%02i',startTime(5)) '_' sprintf('%02.0f',startTime(6))];

%% Initialization =========================================================
gdmTypeSet = ['2D', '2DTD'];
gdmType = '2DTD';


inputFolder = 'data';

% set optimization function
metric = 'NLPD'; % [NLPD, avErr] optimise for min NLPD

% set kernel constants
weightVarianceSigmaFactor = 1.0;
maxKernelEvalRadiusFactor = 4.0;

% set flags for conditional operations such as plotting, saving logs, etc.
bVisualizeData = false;
bNormalizeData = true;

bVerbose = false;
bSaveLog = true;
bSaveVisualization = false;

% set epsilon
epsilon = 0.0000001;

% select experiment corresponding to experimentLabel
[experimentType, fileName, startInd, endInd, ...
    mapCenter, mapSize, sourceLocation, gasType] = selectExperiment(experimentLabel);

% extract inputData considering the experimentLabel, and the cutting
% indices.
startTime = 1;
if strcmp(experimentType, 'simulation_2d')
    endTime = 20; %25
else
    endTime = 100; %130
end
inputData = extractInputData(inputFolder, experimentType, ...
    experimentLabel, fileName, startTime, endTime, startInd, endInd, ...
    bNormalizeData, bNormalizeTime, bVisualizeData);

time = inputData(:,1);

%% Select partitioning cuts ===============================================
strTestSetSelectionId = 'contPartTestSetSelection'; % partitions the data into parsTestSetSelection(2) contiguous sets and selects the parsTestSetSelection(1)th of these sets as test set
parsTestSetSelection = [5 5 0 0]; % %(1)th of %(2) parts with indices from %(3) to %(4)
strTvSetSelectionId = 'contTvSetSelectionFixed'; % partitions the data into parsTvSetSelection(2) contiguous sets and selects the parsTestSetSelection(1)th of these sets as validation set, the rest is used for training
parsTvSetSelection = [4 4]; % %(1)th of %(2) parts is the validation set
numOfSamples = size(inputData, 1);
[trainIndexSet, valIndexSet, tvIndexSet, testIndexSet] = ...
    selectDataPartition(numOfSamples, strTestSetSelectionId, parsTestSetSelection, ...
    strTvSetSelectionId, parsTvSetSelection, bVisualizeData);

tvTime = 0;
if strcmp(experimentType, 'simulation_2d')
    tvTime = 17;
elseif strcmp(experimentType, 'simulation_ros')
    tvTime = 81;
end

if (tvTime > 0)
    tvIndexSet = (find(inputData(:,1) < tvTime))';
    testIndexSet = (find(inputData(:,1) >= tvTime))';
end
targetTimeSet = (unique(inputData(testIndexSet, 1)))';

%% Set the list of targetTimes for which we want to evaluate. =============
if strcmp(experimentType, 'simulation_2d')
    targetTimeList = [17, 18, 19, 20]; %, 21, 22, 23, 24, 25];
elseif strcmp(experimentType, 'simulation_ros')
    targetTimeList = [81, 82, 83, 84, 85, 90, 95, 100]; %, 105, 110, 115, 120, 125]; %, 21, 22, 23, 24, 25];
else
    targetTimeList = [max(targetTimeSet) sum(targetTimeSet)/size(targetTimeSet,2) min(targetTimeSet)];
end

%% Select the intervals and steps to search the meta-parameters space.=====
[minKernelWidth, maxKernelWidth, minCellSize, maxCellSize, ...
    minTimeScale, maxTimeScale, ...
    stepKernelWidth, stepCellSize, stepTimeScale] = selectMetaParametersIntervals(experimentLabel);

%% Visualization of the Data and Partitions. ==============================
if bVisualizeData && bSaveVisualization
    count = 0;
    hfigs = get(0, 'children');
    for m=1:1:length(hfigs)
       if strcmp(hfigs(m).Name, 'Optimization Plot')
           saveas(hfigs(m), [ resultFilePath '/nlpd_plot.fig']);
           print(hfigs(m), '-dpng', '-r96', ...
        [ resultFilePath '/nlpd_plot.png']);
        count = count + 1;
       end
       if strcmp(hfigs(m).Name, 'Data Partitioning')
           saveas(hfigs(m), [ resultFilePath '/paritions_plot.fig']);
           print(hfigs(m), '-dpng', '-r96', ...
        [ resultFilePath '/paritions_plot.png']);
        count = count + 1;
       end
       if strcmp(hfigs(m).Name, 'Visualize Input Data')
           saveas(hfigs(m), [ resultFilePath '/data_plot.fig']);
           print(hfigs(m), '-dpng', '-r96', ...
        [ resultFilePath '/data_plot.png']);
        count = count + 1;
       end
       
       if (count > 2)
           break
       end
    end
end

close all;

%% Iteration on targetTimes to optimize meta-parameters. ==================
for t=targetTimeList
    evaluationSet = [];
    optNLPD = 10000;
    optKernelWidth = 0; optCellSize = 0; optTimeScale = 0;
    
    index = find(time(:,1) == t)';
    targetTime = inputData(index(1,1), 1);
    
    for kernelWidth = minKernelWidth:stepKernelWidth:maxKernelWidth    
        for cellSize = minCellSize:stepCellSize:maxCellSize
            
            if kernelWidth <= cellSize 
                continue;
            end
            
            for timeScale = minTimeScale:stepTimeScale:maxTimeScale
                mapCellSize = [cellSize, cellSize];
                [numOfMapCells, mapCenter, mapCellSize, mapLowerLeft] = getMapGeometry(mapCenter, mapSize, mapCellSize);
                mapGeometry = [numOfMapCells, mapCenter, mapCellSize, mapLowerLeft];

                maxKernelEvalRadius = maxKernelEvalRadiusFactor * kernelWidth;
                weightVarianceSigma = weightVarianceSigmaFactor * 1 / ( sqrt(2.0 * pi) * kernelWidth );
                weightVarianceSteepness = weightVarianceSigma * weightVarianceSigma;
                
                % Create GDM on training set. 
                [trainGdmMap, kGdmWeightMin, kGdmWeightMax, kGdmMapMin, kGdmMapMax, totalMean, ...
                    kGdmVarMin, kGdmVarMax, sigma2Tot] = ...
                    createGDM(inputData, trainIndexSet, ...
                        mapGeometry, ...
                        kernelWidth, maxKernelEvalRadius, weightVarianceSteepness, ...
                        timeScale, targetTime, ...
                        bVerbose);
                % Evaluate GDM on validation set.
                [ trainAvgErr, numOfPredictionsMade, ...
                    trainNLPD, numOfPointsForLikelihoodEval] = evaluateGDM(trainGdmMap, ...
                    mapGeometry, inputData, valIndexSet, bEpsilon, epsilon);
                
                if trainNLPD < optNLPD
                    optNLPD = trainNLPD;
                    optKernelWidth = kernelWidth;
                    optCellSize = cellSize;
                    optTimeScale = timeScale;
                end
                
                % Create GDM on the training+validation (tv) set.
                [testGdmMap, kGdmWeightMin, kGdmWeightMax, kGdmMapMin, kGdmMapMax, totalMean, ...
                    kGdmVarMin, kGdmVarMax, sigma2Tot] = ...
                    createGDM(inputData, tvIndexSet, ...
                        mapGeometry, ...
                        kernelWidth, maxKernelEvalRadius, weightVarianceSteepness, ...
                        timeScale, targetTime, ...
                        bVerbose);
                    
                % Evaluate on the test set. 
                [ testAvgErr, numOfPredictionsMade, ...
                    testNLPD, numOfPointsForLikelihoodEval] = evaluateGDM(testGdmMap, ...
                    mapGeometry, inputData, testIndexSet, bEpsilon, epsilon);
                
                % Print out the results
                fprintf('(c = %.2f, sigma = %.2f, beta = %.5f) - targetTime = %i ==> (NLPD_train = %.4f, err = %.4f), (NLPD_test = %.4f, err = %.4f)\n', ...
                    cellSize, kernelWidth, timeScale, targetTime, ...
                    trainNLPD, trainAvgErr, testNLPD, testAvgErr);
                
                % Stor it in data tabel
                evaluationSet = [evaluationSet; kernelWidth, cellSize, timeScale, ...
                    trainNLPD, trainAvgErr, testNLPD, testAvgErr];
            end
        end
    end

    % set the ending time for logging purpose
    endTime = clock;
    strEndTimeId = [sprintf('%i',endTime(1)) '_' ...
        sprintf('%02i',endTime(2)) '_' sprintf('%02i',endTime(3)) '-' ...
        sprintf('%02i',endTime(4)) '_' sprintf('%02i',endTime(5)) '_' sprintf('%02.0f',endTime(6))];
    
    % Save the results for the targetTime. 
    if bSaveLog
        scale = 1;
        itr = 1;
        saveLogsOptimization( evaluationSet, ...
            resultFilePath, inputFolder, resultFolder, ...
            strStartTimeId, strEndTimeId, metric, ...
            trainIndexSet, valIndexSet, testIndexSet, ...
            strTestSetSelectionId, parsTestSetSelection, strTvSetSelectionId, parsTvSetSelection, ...
            gdmType, experimentType, experimentLabel, fileName, gasType, sourceLocation, ...
            mapCenter, mapSize, ...
            optKernelWidth, optCellSize, optTimeScale, ...
            minKernelWidth, minCellSize, minTimeScale, ...
            maxKernelWidth, maxCellSize, maxTimeScale, ...
            stepKernelWidth, stepCellSize, stepTimeScale, ...
            targetTime, ...
            bNormalizeData, bSaveVisualization, bEpsilon, epsilon, ...
            scale, itr);
    end
    clear evluationSet;
end

end


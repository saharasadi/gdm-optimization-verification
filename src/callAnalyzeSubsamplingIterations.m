%% Description callAnalyzeSubsamplingIterations
%   written by: Sahar Asadi, 
%   Last day released: 2017-02-27
%   
% For subsampling, when it is random sampling on the whole sampling
% interval to collect X percent, we ran the greed search for optimization
% several times. This code, for each scale and each iteration in that
% scale, collects the optimized values and saves the result of all
% iterations of a scale in a file. To compute confidence interval, we use
% the output and call it in 
get the file where the grid search for subsampling optimization is saved. 
% It reutrns the meta-parameters which optimize GDM for NLPD. 
% If bVisualization is set to true It also plots the search space and the metric values for each set of
% meta-parameters.
% 
% important input: 
%   gdmType: '2D' 
%   experimentLabel: name of the input data file
%   experimentType: the type of the experiment and the folder containing the input data file.
%   scaleSet: the scales for which we want the analysis on the subsampling,
%       We then go through each partition of the data based on the scale
%       (see the for loop).
%   bVisualize: A flag for visualizing the NLPD trends in the
%       meta-parameters search space.
%   plotPath: path to where the output plots have to be saved. 
%
%

%% Initialization =========================================================
close all;
clc;
clear;

% set of scales in subsampling. (2 means 50% and 3 means 33%)
scaleSet = [2,3];

% number of results to search for.
iterations = 10;

% select random from all sample range.
partition = 'full';

% set GDM type. the exisiting data is available only for 2D (Kernel DM+V)
gdmType = '2DTD';

% set input experiment
% [experimentType, experimentLabel] = ['real', 'corridor']
%                                     ['real', 'smallnet'] 
%                                     ['simulation_2d', 'no_obstacle_16x4'] 
%                                     ['simulation_2d', 'with_obstacle_up_16x4'] 
%                                     ['simulation_ros', 'mox_sensor_model']
%                                     ['simulation_ros', 'with_obstacle_two_60x20']
% for simulation ros, you need to add the path manually.
experimentLabel = 'with_obstacle_up_16x4'; 
experimentType = 'simulation_2d'; 

% Select the target time
if strcmp(experimentLabel, 'corridor')
    target_time = '7.450402e+03';
elseif strcmp(experimentLabel, 'smallnet')
    target_time = '1.662001e+03';
elseif strcmp(experimentType, 'simulation_2d')
    target_time = '20';
else
    target_time = '100';
end

% whether of not to show the vislualization of NLPD trends in the
% meta-parameter space.
bVisualize = false;

% a constant value, only used for the file path.
epsilon = 'false';

cellSizeLimit = 0.04;

% Set the input and output path
pathPrefix = ['result_temporal_subsampling' '/' experimentType '/' ...
    experimentLabel '/' '2DTD'];
filePrefix = ['log_optimization_description_target_time_' target_time]; 

plotPath = [pathPrefix '/' 'plots'];
if ~exist(plotPath, 'dir')
    mkdir(plotPath);
end

%% Iterate on scales and number of iterations for each scale
for scale=scaleSet;
    resultSet = [];
    for i=1:iterations
        dataPath = [ pathPrefix '/' 'iterations_scale_' int2str(scale)];
        dataFile = [dataPath '/' filePrefix '_scale_' int2str(scale) ...
            '_itr_' int2str(i) '.mat'];
        dataSource = load(dataFile);
        
        [minTestNLPD, minTrainNLPD, optKernelWidth, optCellSize, optTimeScale] = ...
            mainAnalyzeSubsamplingIterations(experimentLabel, epsilon, gdmType, partition, ...
            int2str(scale),  ...
            dataSource.optimizationSet, plotPath, cellSizeLimit, bVisualize);
        % add the result to a dataset.
        resultSet = [resultSet; minTestNLPD, minTrainNLPD, optKernelWidth, optCellSize, optTimeScale];   
    end
    
    % print out mean and std of the result
    [mean(resultSet(:,1)), std(resultSet(:,1))]
    
    % The result set are saved to calculate confidence interval later. 
    save([pathPrefix '/' 'all_result_scale_' int2str(scale), '_' gdmType '.mat'], 'resultSet');
end

% The mean and std values are saved to calculate the confidence interval. 
save([pathPrefix '/' 'all_result.mat'], 'resultSet');

%% Description
%   written by: Sahar Asadi, 
%   Last day released: 2017-02-27
%   
% This code, get the file where the grid search results is saved. 
% It reutrns the meta-parameters which optimize GDM for the given metric. 
% It also plots the search space and the metric values for each set of
% meta-parameters.
% 
% input: 
%   optimization metric: 'NLPD' or 'RMSE'
%   gdmType: '2D' (for Kernel DM+V) and/or '2DTD' (for TD Kernel DM+V)
%   experimentLabel: name of the input data file
%   experimentType: the type of the experiment and the folder containing the input data file.
%   time: the timestamp of the last sample in the dataset. 
%       for simulation 2D: '20'
%       for simulation 3D: '100'
%       for real experiments: 'max' refering to the timestamp of the last recorded sample. 
%   bConstantCellSize: It can be 'false' to optimize on other meta-parameters as
%       well as cell size. or can be 'true' to optimize only on other
%       meta-parameters and keep cell size constant. 
%   plotPath: path to where the output plots have to be saved. 
%
%
%% Initialization =========================================================
close all;
clc;
clear;

% set input experiment
% [experimentType, experimentLabel] = ['real', 'corridor']
%                                     ['real', 'smallnet'] 
%                                     ['simulation_2d', 'no_obstacle_16x4'] 
%                                     ['simulation_2d', 'with_obstacle_up_16x4'] 
%                                     ['simulation_ros', 'mox_sensor_model']
%                                     ['simulation_ros', 'with_obstacle_two_60x20']
% for simulation ros, you need to add the path manually.
experimentType = 'simulation_2d';
experimentLabel = 'no_obstacle_16x4';

% options for conditionPath: 'constant_rate_y10_z0.1'
%                            'constant_rate_y15_z0.1'
%                            'fast_rate_y10_z0.1'
conditionPath = '';
if strcmp('with_obstacle_two_60x20', experimentLabel)
    conditionPath = 'constant_rate_y15_z0.1' + '_';        
end

% set GDM type
gdmTypeStrSet = ['2D  '; '2DTD'];
gdmTypeCellSet = cellstr(gdmTypeStrSet);

% set optimization parameters
metric = 'NLPD'; %options are : ['NLPD', 'RMSE']
bConstantCellSize = true; %options are false , true
bVisualize = false;

cellConstantSize = 0;
if bConstantCellSize
    if strcmp(experimentType, 'simulation_ros')
        cellConstantSize = 0.25;
    else
        cellConstantSize = 0.05;
    end
end

% set time. it is used in the filename of the input data.
if strcmp(experimentType, 'real')
    time = 'max';
elseif strcmp(experimentType, 'simulation_2d')
    time = '20';
else
    time = '100';
end

% flag that it is constant. used only for the file path
epsilon = 'false';

% set input folder
pathPrefix = 'result_optimization_without_time_norm';
inputDataPath = [ pathPrefix '/' experimentType '/' experimentLabel '/' ...
        '2DTD' '/' conditionPath 'epsilon_' epsilon];
inputDataFile = [inputDataPath '/' 'log_optimization_description_target_time_' ...
        time '.mat'];
    
% set path to save the output plots
plotPath = [pathPrefix '/' 'plots' '/' experimentLabel];
if ~exist(plotPath, 'dir')
    mkdir(plotPath);
end

%% Load input data
dataSource = load(inputDataFile);

%% Run on given GDM types
for i=1:size(gdmTypeCellSet,1);
    gdmType = cell2mat(gdmTypeCellSet(i));
    mainAnalyzeOptimizationMetric(experimentLabel, epsilon, gdmType, time, ...
        dataSource.optimizationSet, plotPath, metric, ...
        bConstantCellSize, cellConstantSize, ...
        bVisualize);
end

%close all;
%=========================================================================

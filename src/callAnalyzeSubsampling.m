%% Description callAnalyzeSubsampling
%   written by: Sahar Asadi, 
%   Last day released: 2017-02-27
%   
% This code, get the file where the grid search for subsampling optimization is saved. 
% It reutrns the meta-parameters which optimize GDM for NLPD. 
% If bVisualization is set to true It also plots the search space and the metric values for each set of
% meta-parameters.
% 
% important input: 
%   gdmType: '2D' 
%   experimentLabel: name of the input data file
%   experimentType: the type of the experiment and the folder containing the input data file.
%   scaleSet: the scales for which we want the analysis on the subsampling,
%       We then go through each partition of the data based on the scale
%       (see the for loop).
%   bVisualize: A flag for visualizing the NLPD trends in the
%       meta-parameters search space.
%   plotPath: path to where the output plots have to be saved. 
%
%

%% Initialization =========================================================
close all;
clc;
clear;

% set input experiment
% [experimentType, experimentLabel] = ['real', 'corridor']
%                                     ['real', 'smallnet'] 
%                                     ['simulation_2d', 'no_obstacle_16x4'] 
%                                     ['simulation_2d', 'with_obstacle_up_16x4'] 
%                                     ['simulation_ros', 'mox_sensor_model']
%                                     ['simulation_ros', 'with_obstacle_two_60x20']
% for simulation ros, you need to add the path manually.
experimentLabel = 'corridor';
experimentType = 'real';

% set GDM type
gdmType = '2D';

% Scales for subsampling that we want to fetch the greed search for. Here 2
% means 50% and 3 means 33% subsampling.
scaleSet = [2,3];

% Set the input and output path
%pathPrefix = 'result_optimization_without_time_norm';
pathPrefix = 'result_temporal_subsampling';

plotPath = [pathPrefix '/' 'plots' '/' experimentLabel];
if ~exist(plotPath, 'dir')
    mkdir(plotPath);
end

% this is constant value only used for path to the input file.
epsilon = 'false';

% flags for visualization of NLPD trend.
bVisualize = false;

% iterate on scales
for scale=scaleSet;
    %iterate on the first,second, ..., (scale)th partition of the data.
    for i=1:scale
        dataPath = [ pathPrefix '/' experimentType '/' experimentLabel '/' ...
            gdmType '/' 'scale_' int2str(scale) '_with-all_epsilon_' epsilon];
        dataFile = [dataPath '/' 'log_optimization_subsampling_scale-' ...
            int2str(scale) '_partition-' int2str(i) '.mat'];
        
        dataSource = load(dataFile);
        mainAnalyzeSubsampling(experimentLabel, epsilon, gdmType, ...
            int2str(i), int2str(scale),  ...
            dataSource.optimizationSet, plotPath, bVisualize);
    end
end

function [kernelWidth, cellSize, timeScale] = selectMetaParameters(experimentLabel)

kernelWidth = 0; 
cellSize = 0;
timeScale = 0;

if strcmp(experimentLabel, 'smallnet')
    kernelWidth = 0.20;
    cellSize = 0.15;
    timeScale = 8 * 10^(-6);
end

if strcmp(experimentLabel, 'corridor')
    kernelWidth = 0.60;
    cellSize = 0.30;
    timeScale = 1.20 * 10^(-6);
end

if strcmp(experimentLabel, 'no_obstacle_16x4')
    kernelWidth = 0.20; %0.40; %0.20;
    cellSize = 0.05; %0.10; %0.05;
    timeScale = 3.60 * 10^(-1); %0.00; %3.60 * 10^(-1);
end

if strcmp(experimentLabel, 'with_obstacle_up_16x4')
    kernelWidth = 0.20;
    cellSize = 0.05; %0.15; %0.05;
    timeScale = 6 * 10^(-2); %0.00; %6 * 10^(-2);
end

if strcmp(experimentLabel, 'with_obstacle_two_60x20')
    kernelWidth = 1.50; %1.50;
    cellSize = 0.25; %0.25;
    timeScale = 0.10; %0.10;
end

if strcmp(experimentLabel, 'mox_sensor_model')
    kernelWidth = 0.125; %0.125; %0.75
    cellSize = 0.25; %0.25; %0.50
    timeScale = 0.00; %0.0; %0.10
end

% 2D,mox_sensor_model,100,false,-1.4208,-1.1728,1.25,0.25,0.000000000
% 2DTD,mox_sensor_model,100,false,-3.1470,-2.5462,0.75,0.50,0.100000000
end
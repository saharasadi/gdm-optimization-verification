function [minKernelWidth, maxKernelWidth, minCellSize, maxCellSize, ...
    minTimeScale, maxTimeScale, ...
    stepKernelWidth, stepCellSize, stepTimeScale] = selectMetaParametersIntervals(experimentLabel)

if strcmp(experimentLabel, 'smallnet')
    minKernelWidth = 0.15;
    maxKernelWidth = 1.0;
    stepKernelWidth = 0.05;
    
    minCellSize = 0.05;
    maxCellSize = 0.5;
    stepCellSize = 0.05;

    minTimeScale = 0.0;
    maxTimeScale = 0.150 * 10^(-4);
    stepTimeScale = 0.005 * 10^(-4);
end

if strcmp(experimentLabel, 'corridor')
    minKernelWidth = 0.15;
    maxKernelWidth = 1.00;
    stepKernelWidth = 0.05;

    minCellSize = 0.05;
    maxCellSize = 0.50;
    stepCellSize = 0.05;

    minTimeScale = 0.00001;
    maxTimeScale = 0.003 ;
    stepTimeScale =0.000005;
end

if strcmp(experimentLabel, 'no_obstacle_16x4')
    minKernelWidth = 0.10;
    maxKernelWidth = 1.0;%0.4;
    stepKernelWidth = 0.05;

    minCellSize = 0.05;
    maxCellSize = 0.50%0.25;
    stepCellSize = 0.05;

    minTimeScale = 0.0;
    maxTimeScale = 1.0; %0.5
    stepTimeScale = 0.02;
end

if strcmp(experimentLabel, 'with_obstacle_up_16x4')
    minKernelWidth = 0.10;
    maxKernelWidth = 1.0;%0.4;
    stepKernelWidth = 0.05;

    minCellSize = 0.05;
    maxCellSize = 0.50; %0.30;
    stepCellSize = 0.05;

    minTimeScale = 0.0;
    maxTimeScale = 1.0; %0.5
    stepTimeScale = 0.02;
end

if strcmp(experimentLabel, 'with_obstacle_two_60x20') || ...
    strcmp(experimentLabel, 'mox_sensor_model')
    
    minKernelWidth = 0.25;
    maxKernelWidth = 2.50;
    stepKernelWidth = 0.25;

    minCellSize = 0.25;
    maxCellSize = 2.0;
    stepCellSize = 0.25;

    minTimeScale = 0.0;
    maxTimeScale = 0.30;
    stepTimeScale = 0.05;
end

end
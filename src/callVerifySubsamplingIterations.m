%% Description callVerifySubsamplingIterations.m
%   written by: Sahar Asadi, 
%   Last day released: 2017-02-27
%   
% This code is developed for the purpose of quick sanity checks and
% verifications. It gets an experiement and set of meta-parameters, it
% creates maps and returns the corresponding NLPD values for the training
% and test. This helps to make sure results from optimization are reproducable.
% 
% Note: You can use "selectMetaParameters.m" or set the meta-parameters 
% manually in this code.
%===========================

close all;
clc;
clear;

%% Initialization =========================================================
% set input experiment
% [experimentType, experimentLabel] = ['real', 'corridor']
%                                     ['real', 'smallnet'] 
%                                     ['simulation_2d', 'no_obstacle_16x4'] 
%                                     ['simulation_2d', 'with_obstacle_up_16x4'] 
%                                     ['simulation_ros', 'mox_sensor_model']
%                                     ['simulation_ros', 'with_obstacle_two_60x20']
% for simulation ros, you need to add the path manually.
experimentType = 'real';
experimentLabel = 'corridor';

% select gdm type
gdmTypeStrSet = ['2D  '; '2DTD'];
gdmTypeCellSet = cellstr(gdmTypeStrSet);
gdmType = '2DTD';

% set optimization function
metric = 'NLPD'; % [NLPD, avErr] optimise for min NLPD

% set kernel constants
weightVarianceSigmaFactor = 1.0;
maxKernelEvalRadiusFactor = 4.0;

% total number of iterations for random sampling
totalItr = 10;

% set flags for conditional operations such as plotting, saving logs, etc.
bVisualizeData = true;
bNormalizeData = true;
bNormalizeTime = false;
bVerbose = false;
bSaveLog = true;
bSaveVisualization = true;

% flag for epsilon in evaluation. This has to stay false (not using epsilon
% in calculations)
bEpsilon = false;
epsilon = 0.0000001;

% setting input and output folder
inputFolder = 'data';
resultFolder = 'result';

%% Select experiment setup to experimentLabel =====================
[experimentType, fileName, startInd, endInd, ...
    mapCenter, mapSize, sourceLocation, gasType] = selectExperiment(experimentLabel);

%% Extract input data =====================================================
% setting the indices to cut samples when extracting input data.
% indices. This is only for the simulation experiments.
startTime = 1;
if strcmp(experimentType, 'simulation_2d')
    endTime = 20;
else
    endTime = 100;
end

% extract inputData
inputData = extractInputData(inputFolder, experimentType, ...
    experimentLabel, fileName, startTime, endTime, startInd, endInd, ...
    bNormalizeData, bNormalizeTime, bVisualizeData);

%% Select partitioning cuts ===============================================
strTestSetSelectionId = 'contPartTestSetSelection'; % partitions the data into parsTestSetSelection(2) contiguous sets and selects the parsTestSetSelection(1)th of these sets as test set
parsTestSetSelection = [5 5 0 0]; % %(1)th of %(2) parts with indices from %(3) to %(4)
strTvSetSelectionId = 'contTvSetSelectionFixed'; % partitions the data into parsTvSetSelection(2) contiguous sets and selects the parsTestSetSelection(1)th of these sets as validation set, the rest is used for training
parsTvSetSelection = [4 4]; % %(1)th of %(2) parts is the validation set
numOfSamples = size(inputData, 1);
[trainIndexSet, valIndexSet, tvIndexSet, testIndexSet] = ...
    selectDataPartition(numOfSamples, strTestSetSelectionId, parsTestSetSelection, ...
    strTvSetSelectionId, parsTvSetSelection, bVisualizeData);

%% Set the meta-parameters ================================================
% You can either read it from "selectMetaParameters" or initialize them
% manually here.
[kernelWidth, cellSize, timeScale] = selectMetaParameters(experimentLabel);
%kernelWidth = 0.90;
%cellSize = 0.35;
%timeScale = 1.35 * 10^(-3);

if strcmp(gdmType, '2D')
    timeScale = 0.0;
endtimeScale = 0.0;
end

%% Set targetTime =========================================================
targetTimeSet = (unique(inputData(testIndexSet, 1)))';
% list of targetTimes for which we want to evaluate.
targetTimeList = [max(targetTimeSet)];% sum(targetTimeSet)/size(targetTimeSet,2) min(targetTimeSet)];
targetTime = max(inputData(testIndexSet,1));

%% Select the subsampling scale ===========================================
% number os scales:  
numberOfScales = 2;  

%s = RandStream('mt19937ar','Seed',0);
subsetSize = round(size(tvIndexSet,2)/numberOfScales);

result = [];
for itr = 1:totalItr  
  for i =1:numberOfScales
    %testSubsetSize = round(size(testIndexSet,2) * 1);
    %testIndexSubSet = testIndexSet; %sort(randperm(s, size(testIndexSet,2), testSubsetSize));
    
    %select subsampling partition for tv set based on the number of scales.
    tvIndexSubSet = ((i-1)*subsetSize + 1):(i*subsetSize); 
    fprintf('%i : [%i , %i]\n', i, tvIndexSubSet(1,1), tvIndexSubSet(1,end));
    part = parsTvSetSelection(1,1);
    partitionSize = round(size(tvIndexSubSet,2)/part);
    trainIndexSubSet = tvIndexSubSet(1, 1:partitionSize * (part -1));
    valIndexSubSet = tvIndexSubSet(1, partitionSize * (part -1)+1:end);

    % optimized meta-parameters for each scale. these values are collected
    % from the results of optimizing subsampling
    if i == 1
        cellSize = 0.10;
        kernelWidth = 0.50;
    elseif i == 2
        cellSize = 0.10;
        kernelWidth = 0.25;
    else
        cellSize = 0.10;
        kernelWidth = 0.20;
    end
    
    %initialize geometry
    mapCellSize = [cellSize, cellSize];
    [numOfMapCells, mapCenter, mapCellSize, mapLowerLeft] = getMapGeometry(mapCenter, mapSize, mapCellSize);
    mapGeometry = [numOfMapCells, mapCenter, mapCellSize, mapLowerLeft];

    % initialize kernel function variables
    maxKernelEvalRadius = maxKernelEvalRadiusFactor * kernelWidth;
    weightVarianceSigma = weightVarianceSigmaFactor * 1 / ( sqrt(2.0 * pi) * kernelWidth );
    weightVarianceSteepness = weightVarianceSigma * weightVarianceSigma;
    
    % create a map for the given meta-parameters and targetTime on the
    % training + validatio set.
    [kGdmvMap, kGdmWeightMin, kGdmWeightMax, kGdmMapMin, kGdmMapMax, totalMean, ...
        kGdmVarMin, kGdmVarMax, sigma2Tot] = createGDM(inputData, tvIndexSet, ...
        mapGeometry, ...
        kernelWidth, maxKernelEvalRadius, weightVarianceSteepness, ...
        timeScale, targetTime, ...
        bVerbose);
    
    % evaluate the gdm model over the test set
    [ avPredictionError, numOfPredictionsMade, ...
        NLPD, numOfPointsForLikelihoodEval] = evaluateGDM(kGdmvMap, ...
        mapGeometry, inputData, testIndexSet, bEpsilon, epsilon);
    fprintf('%s,%s,%s,%.2f,%.2f,%.4f,%.4f\n', ...
        num2str(itr), num2str(numberOfScales), num2str(i), ...
        cellSize, kernelWidth, NLPD, avPredictionError);
    
    % store the results in a matrix
%     result = [result; itr, numberOfScales, i, ...
%         cellSize, kernelWidth, NLPD, avPredictionError];
  end
end



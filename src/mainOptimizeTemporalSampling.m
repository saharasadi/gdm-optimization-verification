function [] = mainOptimizeTemporalSampling(experimentLabel, bEpsilon, ...
    bNormalizeTime, resultFolder, numberOfScales)

% set the start time - this is used to create a folder in the result path
startTime = clock;
strStartTimeId = [sprintf('%i',startTime(1)) '_' ...
    sprintf('%02i',startTime(2)) '_' sprintf('%02i',startTime(3)) '-' ...
    sprintf('%02i',startTime(4)) '_' sprintf('%02i',startTime(5)) '_' sprintf('%02.0f',startTime(6))];

% initialize variables
gdmType = '2D';
inputFolder = 'data';

% set optimization function
metric = 'NLPD'; % [NLPD, avErr] optimise for min NLPD

% set kernel constants
weightVarianceSigmaFactor = 1.0;
maxKernelEvalRadiusFactor = 4.0;

% set flags for conditional operations such as plotting, saving logs, etc.
bVisualizeData = true;
bNormalizeData = true;

bVerbose = false;
bSaveLog = true;
bSaveVisualization = true;

% set epsilon flag and epsilon itself
epsilon = 0.0000001;

% select experiment corresponding to experimentLabel
[experimentType, fileName, startInd, endInd, ...
    mapCenter, mapSize, sourceLocation, gasType] = selectExperiment(experimentLabel);

% extract inputData considering the experimentLabel, and the cutting
% indices.
startTime = 1;
endTime = 100;
inputData = extractInputData(inputFolder, experimentType, ...
    experimentLabel, fileName, startTime, endTime, startInd, endInd, ...
    bNormalizeData, bNormalizeTime, bVisualizeData);

% select partitioning cuts
strTestSetSelectionId = 'contPartTestSetSelection'; % partitions the data into parsTestSetSelection(2) contiguous sets and selects the parsTestSetSelection(1)th of these sets as test set
parsTestSetSelection = [5 5 0 0]; % %(1)th of %(2) parts with indices from %(3) to %(4)
strTvSetSelectionId = 'contTvSetSelectionFixed'; % partitions the data into parsTvSetSelection(2) contiguous sets and selects the parsTestSetSelection(1)th of these sets as validation set, the rest is used for training
parsTvSetSelection = [4 4]; % %(1)th of %(2) parts is the validation set
numOfSamples = size(inputData, 1);
[trainIndexSet, valIndexSet, tvIndexSet, testIndexSet] = ...
    selectDataPartition(numOfSamples, strTestSetSelectionId, parsTestSetSelection, ...
    strTvSetSelectionId, parsTvSetSelection, bVisualizeData);

% tvIndexSet = (find(inputData(:,1) < 19))';
% testIndexSet = (find(inputData(:,1) >= 19))';

targetTimeSet = (unique(inputData(testIndexSet, 1)))';

[minKernelWidth, maxKernelWidth, minCellSize, maxCellSize, ...
    minTimeScale, maxTimeScale, ...
    stepKernelWidth, stepCellSize, stepTimeScale] = selectMetaParametersIntervals(experimentLabel);

resultFilePath = [resultFolder '/' experimentType '/' experimentLabel '/' gdmType '/' strStartTimeId];

if ~exist(resultFilePath, 'dir')
    mkdir(resultFilePath);
end

if bVisualizeData && bSaveVisualization
    count = 0;
    hfigs = get(0, 'children');
    for m=1:1:length(hfigs)
       if strcmp(hfigs(m).Name, 'Optimization Plot')
           saveas(hfigs(m), [ resultFilePath '/nlpd_plot.fig']);
           print(hfigs(m), '-dpng', '-r96', ...
        [ resultFilePath '/nlpd_plot.png']);
        count = count + 1;
       end
       if strcmp(hfigs(m).Name, 'Data Partitioning')
           saveas(hfigs(m), [ resultFilePath '/paritions_plot.fig']);
           print(hfigs(m), '-dpng', '-r96', ...
        [ resultFilePath '/paritions_plot.png']);
        count = count + 1;
       end
       if strcmp(hfigs(m).Name, 'Visualize Input Data')
           saveas(hfigs(m), [ resultFilePath '/data_plot.fig']);
           print(hfigs(m), '-dpng', '-r96', ...
        [ resultFilePath '/data_plot.png']);
        count = count + 1;
       end
       
       if (count > 2)
           break
       end
    end
end

close all;


% list of targetTimes for which we want to evaluate.
targetTime = max(inputData(testIndexSet,1));
subsetSize = round(size(tvIndexSet,2)/numberOfScales);

for i=1:1:numberOfScales
   tvIndexSubSet = [((i-1)*subsetSize + 1):(i*subsetSize)];
%    load([experimentLabel '_' int2str(numberOfScales) '.mat']);
%    indexSet = smallnet_50; 
%    tvIndexSubSet = indexSet';
   fprintf('%i : [%i , %i]\n', i, tvIndexSubSet(1,1), tvIndexSubSet(1,end));
   part = parsTvSetSelection(1,1);
   partitionSize = round(size(tvIndexSubSet,2)/part);
   trainIndexSubSet = tvIndexSubSet(1, 1:partitionSize * (part -1));
   valIndexSubSet = tvIndexSubSet(1, partitionSize * (part -1)+1:end);
   
   optimizationSet = [];
   optNLPD = 10000;
   optKernelWidth = 0; optCellSize = 0; optTimeScale = 0;
   timeScale = 0;
   
   for kernelWidth = minKernelWidth:stepKernelWidth:maxKernelWidth    
       for cellSize = minCellSize:stepCellSize:maxCellSize     
            if kernelWidth <= cellSize 
                continue;
            end

            mapCellSize = [cellSize, cellSize];
            [numOfMapCells, mapCenter, mapCellSize, mapLowerLeft] = getMapGeometry(mapCenter, mapSize, mapCellSize);
            mapGeometry = [numOfMapCells, mapCenter, mapCellSize, mapLowerLeft];

            maxKernelEvalRadius = maxKernelEvalRadiusFactor * kernelWidth;
            weightVarianceSigma = weightVarianceSigmaFactor * 1 / ( sqrt(2.0 * pi) * kernelWidth );
            weightVarianceSteepness = weightVarianceSigma * weightVarianceSigma;

            [trainGdmMap, kGdmWeightMin, kGdmWeightMax, kGdmMapMin, kGdmMapMax, totalMean, ...
                kGdmVarMin, kGdmVarMax, sigma2Tot] = ...
                createGDM(inputData, trainIndexSubSet, ...
                    mapGeometry, ...
                    kernelWidth, maxKernelEvalRadius, weightVarianceSteepness, ...
                    timeScale, targetTime, ...
                    bVerbose);
            [ trainAvgErr, numOfPredictionsMade, ...
                trainNLPD, numOfPointsForLikelihoodEval] = evaluateGDM(trainGdmMap, ...
                mapGeometry, inputData, valIndexSubSet, bEpsilon, epsilon);

            if trainNLPD < optNLPD
                optNLPD = trainNLPD;
                optKernelWidth = kernelWidth;
                optCellSize = cellSize;
                optTimeScale = timeScale;
            end

            % evaluate the gdm model over the test set
            [testGdmMap, kGdmWeightMin, kGdmWeightMax, kGdmMapMin, kGdmMapMax, totalMean, ...
                kGdmVarMin, kGdmVarMax, sigma2Tot] = ...
                createGDM(inputData, tvIndexSubSet, ...
                    mapGeometry, ...
                    kernelWidth, maxKernelEvalRadius, weightVarianceSteepness, ...
                    timeScale, targetTime, ...
                    bVerbose);

            [ testAvgErr, numOfPredictionsMade, ...
                testNLPD, numOfPointsForLikelihoodEval] = evaluateGDM(testGdmMap, ...
                mapGeometry, inputData, testIndexSet, bEpsilon, epsilon);
            fprintf('(c = %.2f, sigma = %.2f, beta = %.5f) - targetTime = %i ==> (NLPD_train = %.4f, err = %.4f), (NLPD_test = %.4f, err = %.4f)\n', ...
                cellSize, kernelWidth, timeScale, targetTime, ...
                trainNLPD, trainAvgErr, testNLPD, testAvgErr);
            optimizationSet = [optimizationSet; kernelWidth, cellSize, timeScale, ...
                trainNLPD, trainAvgErr, testNLPD, testAvgErr];
        end
   end
    
   logFile = sprintf('log_optimization_subsampling_scale-%i_partition-%i', numberOfScales, i);
   save([resultFilePath '/' logFile '.mat'], 'optimizationSet');
   clear optimizationSet;
   
end    

end
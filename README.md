# README #

* Developed by: Sahar Asadi
* Last date modified: 2017-02-01

This project is based on "*A. J. Lilienthal, M. Reggente, M. Trincavelli, J. L. Blanco and J. Gonzalez, A statistical approach to gas distribution modelling with mobile robots - The Kernel DM+V algorithm, 2009 IEEE/RSJ International Conference on Intelligent Robots and Systems, St. Louis, MO, 2009, pp. 570-576. doi: 10.1109/IROS.2009.5354304*". The original code was developed by AASS research center. 

## Content ##
This project extends the two-dimensional vanilla Kernel DM+V to a time-dependent one. 
It includes:
* Temporal subsampling method.
* TD Kernel DM+V
* Kernel DM+V
* Evaluation modules
* Modules to study the impact of varying the target time.
* And visualising results for a given meta parameter set. 

## HOW TO ##
For FAQ on how to use this project, read HOWTO.txt.

## Contribution ##
This work will be presented in the dissertation "Towards Dense Air Quality Monitoring:
Time-Dependent Statistical Gas Distribution Modelling
and Sensor Planning", Orebro University, in print, 2017.

## Contact ##
For further information, please check out [http://aass.oru.se/Research/Learning/index.html](Link URL)
or contact [sahar.asadi@oru.se](Link URL).